
### General

In all messages, except the initial handshake, the first byte of the message indicates the message type and the rest is message-specific.  
The following sections will include the byte for the message type in the header, if applicable.

Note that one shouldn't send commands to the debuggee, unless the debuggee is in a paused state.

#### Communication types

Debugger → debuggee: command  
Debuggee → debugger: message

#### Strings

All strings are sent as follows using the protocol:

* [u16] the length of the string
* [u8]\*{length} bytes for the string
* [u8] null-terminator

#### Initial handshake

Upon startup the emulator will open a socket on the \[::1]:6780 IPv6 loopback address and will wait for a connection.

After establishing the connection the emulator will send a 16-bit value indicating the protocol version in use by the emulator.  
The debugger must ensure that it's able to communicate using the given protocol version value. Otherwise the debugger should simply close the socket.  
If the debug protocol version is acceptable, then the debugger mustn't send anything else until it receives one of the general messages.

#### System startup (0x0)

This indicates that the emulator has loaded the game. The emulator is in a paused state.

* [system_type] type – the system type for which the game is being booted
* [string] name – name of the game

[system_type][u8]:

* 0 – unknown
* 1 – CHIP8
* 2 – GameBoy

#### Error (0x1)

* [u8] error_code – a platform-specific error code
* [string] message

### GameBoy

General errors:

* INVALID_COMMAND (0x0) – when an invalid command is sent

#### Run (0x2)

Runs the emulator.

#### Pause (0x3)

Pauses the emulator after rendering of the current frame is finished.

#### Subscribe (0x4)

Enables subscribing to notifications about various events.

* [event] type
* [bool] subscribe – desired state of the subscription

[event][u8]:

* VBLANK (0) – occurrs after every vertical blank ±17555 cycles

Possible errors:

* INVALID_EVENT (0x1) – when an invalid event type is sent

##### Response message (0x2)

Upon the event occurring the emulator will pause after rendering the current frame and will send this message.

* [event] type

#### Read memory (0x5)

* [u16] address – location from which the read shall begin
* [u16] size – number of bytes to be read

Possible errors:

* OUT_OF_BOUNDS_READ (0x2) – when (address + size) > 0x10'000

##### Response message (0x3)

* [u16] size
* [u8]\*{size} memory – the requested memory area

#### Write memory (0x6)

* [u16] address – address of the byte to be written
* [u8] value – the value to be written

#### Screen buffer (0x7)

(nothing else)

##### Response message (0x4)

* [u8]\*160\*144\*3 buffer – a buffer containing RGB values for the game screen being currently displayed (the screen size is 160x144 pixels)

#### Button (0x8)

Sets the status for the desired button.

* [button] button
* [bool] status – true when pressed, false when not

[button][u8]:

* RIGHT = 0x0
* LEFT = 0x1
* UP = 0x2
* DOWN = 0x3
* A = 0x4
* B = 0x5
* SELECT = 0x6
* START = 0x7

Possible errors:

* INVALID_BUTTON (0x3) – a request to press an invalid button was received