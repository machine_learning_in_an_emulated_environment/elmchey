# Elmchey

Elmchey is a CHIP8 and GameBoy emulator with a remote socket-based debugging API for GameBoy.

Development of this project has stopped. For lessons learned refer to [the postmortem section](#postmortem).

![picture of main window, Dr. Mario running, the about window and the licences for third-party libraries](https://i.vgy.me/IdSlAo.png)

## Getting started

Instructions on the [wiki](https://gitlab.com/machine_learning_in_an_emulated_environment/elmchey/wikis/getting-started) for development setup.

## Game detection

Games must be situated in a folder named according to the platform name within the directory of the emulator executable.

Platform name and corresponding file extension:

* GameBoy: .gb
* CHIP8: .ch8

## Configuration

Options for the program are contained within the "Elmchey.yaml", which is created in the same folder as the emulator executable on the first startup of the emulator.  
The configuration file is in the YAML format.

Each currently supported platform has the renderer option with the following possible values:

* 0 – Vulkan (recommended for better performance)
* 1 – [DirectX 12] not functional, do not use
* 2 – OpenGL 4.5
* 3 – OpenGL 1.1

Platforms supporting debugging also offer a boolean option "headless", which can be used to turn off rendering and allow the emulator run at maximum speed.

## Debugging

Debugging can be enabled using the boolean "enable_debugging" option located in the "developer" section of the configuration file.  
This option causes the emulator to initially startup in headless mode.

The debug protocol is described in [debug.md](debug.md).

# Postmortem

LOC excluding third-party libraries:

|   Language   | files | blank | comment | code  |
|:------------:|:-----:|-------|---------|-------|
| C++          | 26    | 1511  | 358     | 6958  |
| C/C++ Header | 37    | 463   | 233     | 2885  |
| CMake        | 9     | 79    | 47      | 228   |
| Markdown     | 2     | 9     | 0       | 29    |
| YAML         | 2     | 1     | 0       | 13    |
|    **SUM**   | 76    | 2063  | 638     | 10113 |

### wxWidgets

wxWidgets was used as the UI library.

The library is clunky to use with modern C++. This is mostly due to it still supporting ancient platforms (e.g. Win9x) and compilers (e.g. MSVC6 and 7). The build process is also very complicated, easy to break and tedious.  
[I reported many of the problems](https://trac.wxwidgets.org/query?status=accepted&status=closed&status=confirmed&status=infoneeded&status=infoneeded_new&status=new&status=portneeded&status=reopened&reporter=tambre&col=id&col=summary&col=component&col=status&col=owner&col=type) and [implemented a major quality of life improvement](https://trac.wxwidgets.org/ticket/17623) to the event binding mechanism for C++11 and newer.

The lack of low-level access to primitives used by the library resulted in hacks for properly rendering to a canvas (see [renderer_frame.h](external/common/gui/renderer_frame.h#L34)).

I also created [a resize clamping mechanism](external/common/gui/renderer_frame.cpp#L104) used by the pixel renderers for even scaling ([video](https://i.imgur.com/KbOU05r.gifv)). This had hacks of its own and had to support the earlier canvas rendering hacks.

With relaxed time constraints, it's worth considering using the platform-specific UI APIs directly to avoid wasting time fighting the library.

### Logging

[A small custom logger](external/common/logger/logger.h) was used for this project. Its completely insufficient for anything more advanced (e.g. per-module logging, colour logging).

It's a good idea to use a ready-made advanced logger like *spdlog* instead.

### Configuration system

[A configuration system](emulator_common/configuration/configuration.h) was created to make it easy to access and add new options in code.  
It automatically populated the settings interface ([picture](https://i.vgy.me/3ACKST.png)).

However, the implementation ended up requiring quite a bit of boilerplate and resulted in significant compile-time dependencies, requiring an almost full rebuild whenever any modifications were made to code pertaining to configuration.  
Since configuration items were declared in the midst of a single header this reduced their visibility and required the definitions of enums and such to be made available in that header.

Automatically populating the interface was a bad idea due to the resulting unfriendly interface.  
Rest of the system would require a big rewrite to be worth using (and there probably are better existing libraries for this).

### Pixel renderers

I developed a common pixel renderer interface for rendering pixels to a canvas with multiple backends to go along with it.

This abstraction worked well and I'm proud of the extremely fast [Vulkan backend](emulator_common/pixel_renderer/vulkan_pixel_renderer.cpp).

### GameBoy

This emulator backend was implemented in a hurry and was only tested to be capable of running Dr. Mario and Tetris.  
The code quality is terrible and in need of a big refactoring.

### Debugging

The debugger interface is flexible, enabling each emulator backend to implement custom commands fit for debugging the given system.

The socket-based binary API is fit for purpose and fast.

## Conclusion

I gained a lot of valuable experience through having made these mistakes. I'm working on a successor to this project which avoids the pitfalls described here.