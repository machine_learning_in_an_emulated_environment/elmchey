#pragma once

#include "debug/debug.h"
#include "system.h"

#include "misc/byte_buffer.h"

// Structure that represents an executable program for any given system
struct Executable
{
	System system;
	std::string name;
	fs::path path;
};

// Base class for different cores, so they could all be initialized and booted nicely and easily
class Emulator
{
public:
	Emulator(Executable& executable) : executable(executable)
	{
	}

	// Needs to be virtual to ensure that the deriving class's destructor gets called
	virtual ~Emulator();

	// This is called on the GUI thread.
	virtual bool initialize() = 0;

	// Creates the emulation thread, which calls either run() or run_debugger()
	void start();

#ifdef DEBUGGER
	// Called when a message is received from the debugger
	virtual void handle_message(u8& message, ByteBuffer& buffer) = 0;
#endif

protected:
	// The excutable being executed by the current emulator instance
	Executable executable;

	// Must be called when the emulator instance exits
	void exit();
	void exit(wxCloseEvent& event);

private:
	std::thread emulation_thread;

	// Called on the emulation thread.
	virtual void run() = 0;

#ifdef DEBUGGER
	// Called when the executable is being launched with a debugger attached.
	virtual void run_debugger() = 0;
#endif
};

// The emulator instance
extern std::unique_ptr<Emulator> emu;