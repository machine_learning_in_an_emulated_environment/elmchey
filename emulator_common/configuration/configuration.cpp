#include "stdafx_emulator.h"
#include "configuration.h"

// For parsing YAML
#include "yaml-cpp/yaml.h"

namespace cfg
{
	// Configuration structure definitions
	Root root;
	General general;
	CHIP8 chip8;
	GameBoy game_boy;
	Developer developer;

	// Loads the configuration for the current configuration.
	bool load();

	// The executable that we are using a custom configuration for.
	// Is a nullptr if no custom configuration is in use.
	Executable* executable;

	// Holds pointers to pushed executables.
	std::stack<Executable*> pushed_executables;

	// Returns the path for the currently in-use configuration file.
	fs::path current_config_path();

#ifdef DEBUG
	// For debugging the configuration tree.
	void print_cfg_hierarchy(EntryBase& node);
#endif
}

fs::path cfg::current_config_path()
{
	if (executable)
	{
		return fs::current_path() / "data" / system_to_string(executable->system) / (executable->name + ".yaml");
	}
	else
	{
		return fs::current_path() / "Elmchey.yaml";
	}
}

bool cfg::load()
{
	// Open and parse the YAML configuration file
	std::fstream config_file(current_config_path(), std::fstream::in);

	if (!config_file)
	{
		// When the executable isn't a nullptr and opening the fail fails, then that means that the custom
		// configuration for the given executable doesn't exist yet and we avoid logging it as an error.
		if (!executable)
		{
			error("Failed to open configuration file for loading. %s", strerror(errno));
		}

		return false;
	}

	YAML::Node config_node = YAML::Load(config_file);

	// Parse the configuration into our own structures
	auto parse_node = [](EntryBase& node, YAML::Node& config_node, auto& parse_node) -> bool
	{
		// Parse the node
		switch (node.type)
		{
			case Type::NODE:
			{
				if (!config_node.IsMap())
				{
					error("Config node %s should be a map, but is something else. (%d)", node.name.c_str(), config_node.Type());
					return false;
				}

				break;
			}

			case Type::BOOLEAN:
			{
				if (!config_node.IsScalar())
				{
					error("Boolean config node %s should be a scalar, but is something else. (%d)", node.name.c_str(), config_node.Type());
					return false;
				}

				reinterpret_cast<Boolean&>(node) = config_node.as<bool>();

				break;
			}

			case Type::STRING:
			{
				if (!config_node.IsScalar())
				{
					error("String config node %s should be a scalar, but is something else. (%d)", node.name.c_str(), config_node.Type());
					return false;
				}

				reinterpret_cast<String&>(node) = config_node.as<std::string>();

				break;
			}

			case Type::ENUM:
			{
				if (!config_node.IsScalar())
				{
					error("Enum config node %s should be a scalar, but is something else. (%d)", node.name.c_str(), config_node.Type());
					return false;
				}

				reinterpret_cast<Enum<enum Dummy>&>(node) = static_cast<Dummy>(config_node.as<u32>());

				break;
			}
		}

		// Also parse all children of the node.
		for (EntryBase* child : node.children)
		{
			YAML::Node child_config_node = config_node[child->name];

			// Check if the child config node actually exists, before trying to parse it.
			if (child_config_node)
			{
				if (!parse_node(*child, child_config_node, parse_node))
				{
					return false;
				}
			}
		}

		return true;
	};

	// Parse all the nodes starting from the root node.
	if (!parse_node(root, config_node, parse_node))
	{
		return false;
	}

	return true;
}

bool cfg::load_global()
{
	// Disable the custom configuration.
	executable = nullptr;

	// If the global configuration file doesn't exist yet, then we create it with the default settings.
	if (!fs::exists(current_config_path()))
	{
		warning("The configuration file doesn't exist. One will be created with the default settings.");
		save();
		return true;
	}

	return load();
}

bool cfg::load_custom(Executable& executable_in)
{
	// Set the pointer to enable the custom configuration.
	executable = &executable_in;

	// If the loading fails, we'll still be using the values from the global configuration (hopefully).
	if (!load())
	{
		executable = nullptr;
		return false;
	}

	return true;
}

void cfg::push()
{
	pushed_executables.push(executable);
}

bool cfg::pop()
{
	executable = pushed_executables.top();
	pushed_executables.pop();
	return load();
}

bool cfg::save()
{
	fs::path config_path = current_config_path();
	fs::path config_folder = config_path.parent_path();

	// Make sure that the folder for the configuration file exists
	if (!fs::exists(config_folder) && !fs::create_directories(config_folder))
	{
		error("Failed to create the directories necessary for storing the configuration file.");
	}

	// Open the configuration file
	std::fstream config_file(config_path, std::fstream::out | std::fstream::trunc);

	if (!config_file)
	{
		error("Failed to open configuration file for saving. %s", strerror(errno));
		return false;
	}

	// The emitter
	YAML::Emitter config_emitter;
	config_emitter.SetIndent(4);

	// We need a map to wrap all the sub-nodes of the root.
	config_emitter << YAML::BeginMap;

	// Emit the config
	auto emit_node = [](EntryBase& node, YAML::Emitter& emitter, auto& emit_node) -> bool
	{
		// Emit the given node's beginning
		switch (node.type)
		{
			case Type::NODE:
			{
				emitter << YAML::Key << node.name;
				emitter << YAML::BeginMap;
				break;
			}

			case Type::BOOLEAN:
			{
				Boolean& boolean = static_cast<Boolean&>(node);
				emitter << YAML::Key << boolean.name;
				emitter << YAML::Value << boolean.value;
				break;
			}

			case Type::STRING:
			{
				String& string = static_cast<String&>(node);
				emitter << YAML::Key << string.name;
				emitter << YAML::Value << string.value;
				break;
			}

			case Type::ENUM:
			{
				Enum<enum Dummy>& enumerator = static_cast<Enum<Dummy>&>(node);
				emitter << YAML::Key << enumerator.name;
				emitter << YAML::Value << enumerator.value;
				break;
			}
		}

		// Also emit all the children of the given node.
		for (EntryBase* child : node.children)
		{
			if (!emit_node(*child, emitter, emit_node))
			{
				return false;
			}
		}

		// If the node is a node, we need to also end it
		if (node.type == Type::NODE)
		{
			emitter << YAML::EndMap;
		}

		return true;
	};

	// For a custom config we only emit the node relevant for the system.
	// For a global config we emit all the nodes (excluding the root node).
	if (executable)
	{
		// The node that we need to emit for the custom config.
		EntryBase* node;

		switch (executable->system)
		{
			case System::CHIP8:
			{
				node = &cfg::chip8;
				break;
			}

			case System::GAME_BOY:
			{
				node = &cfg::game_boy;
				break;
			}
		}

		if (!emit_node(*node, config_emitter, emit_node))
		{
			error("Failed to emit the custom config node for %s (%s)", executable->name.c_str(), system_to_string(executable->system));
			return false;
		}
	}
	else
	{
		// We don't emit the root node, as that's only used for internal grouping.
		for (EntryBase* child : root.children)
		{
			if (!emit_node(*child, config_emitter, emit_node))
			{
				error("Failed to output configuration node.");
				return false;
			}
		}
	}

	// End the wrapper map.
	config_emitter << YAML::EndMap;

	// Check if emitting succeded
	if (!config_emitter.good())
	{
		error("Failed to emit configuration file. Error: %s", config_emitter.GetLastError().c_str());
		return false;
	}

	// Output config to file
	config_file << config_emitter.c_str();

	return true;
}

#ifdef DEBUG
// A function that prints the config hierarchy, including all the children and their respective values.
void cfg::print_cfg_hierarchy(EntryBase& node = root)
{
	auto print_node = [](EntryBase& node, u8 indent, auto& print_node) -> void
	{
		// Obtain the value of the node
		std::string value;

		switch (node.type)
		{
			case Type::NODE:
			{
				break;
			}

			case Type::BOOLEAN:
			{
				value = format("%s", reinterpret_cast<Boolean&>(node) ? "true" : "false");
				break;
			}

			case Type::STRING:
			{
				value = format("%s", reinterpret_cast<String&>(node).value.c_str());
				break;
			}

			case Type::ENUM:
			{
				value = format("%d", reinterpret_cast<Enum<enum Dummy>&>(node).value);
				break;
			}
		}

		// Add the correct amount of tabs for indentation.
		std::string indents = "";

		for (u8 i = 0; i < indent; ++i)
		{
			indents += "\t";
		}

		// Log the message with the indentation, node name and the value of the node.
		log("%s%s: %s\n", indents.c_str(), node.name.c_str(), value.c_str());

		// Print all the sub nodes of the current node.
		for (EntryBase* child : node.children)
		{
			print_node(*child, indent + 1, print_node);
		}
	};

	for (auto& child : node.children)
	{
		print_node(*child, 0, print_node);
	}

	log("\n");
};
#endif