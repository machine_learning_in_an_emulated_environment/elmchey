#pragma once

// Forward declarations
struct Executable; // for load_custom()

// Enum definitions
enum class PixelRendererType : u8
{
	VULKAN   = 0,
	DX12     = 1,
	OPENGL45 = 2,
	OPENGL11 = 3,
};

namespace cfg
{
	// Different configuration entry types
	enum class Type : u8
	{
		NODE,
		BOOLEAN,
		STRING,
		ENUM,
	};

	// Extern definition of the root structure
	extern struct Root root;

	// Base class for all configuration entry types
	class EntryBase
	{
	public:
		Type type;
		std::string name;
		std::vector<EntryBase*> children;

	protected:
		EntryBase(Type type, EntryBase* parent, std::string name) : type(type), name(name)
		{
			// We add the current entry to the parent's list of children, so the hierarchy can be easily parsed
			if (parent)
			{
				parent->children.push_back(this);
			}
		}
	};

	class Node : public EntryBase
	{
	public:
		Node(Node* parent_node, std::string name) : EntryBase(Type::NODE, parent_node, name) {}
		Node(std::string name) : EntryBase(Type::NODE, reinterpret_cast<EntryBase*>(&root), name) {}
	};

	class Boolean : public EntryBase
	{
	public:
		Boolean(Node* parent_node, std::string name, bool default_value) : EntryBase(Type::BOOLEAN, parent_node, name), value(default_value) {}

		operator bool() const
		{
			return value;
		}

		bool& operator=(bool value)
		{
			this->value = value;
			return this->value;
		}

		bool value;
	};

	class String : public EntryBase
	{
	public:
		String(Node* parent_node, std::string name, std::string default_value) : EntryBase(Type::STRING, parent_node, name), value(default_value) {}

		operator std::string&()
		{
			return value;
		}

		std::string& operator=(std::string value)
		{
			this->value = value;
			return this->value;
		}

		std::string value;
	};

	template<typename Enumerator>
	class Enum : public EntryBase
	{
	public:
		Enum(Node* parent_node, std::string name, Enumerator default_value) : EntryBase(Type::ENUM, parent_node, name), value(default_value) {}

		operator Enumerator() const
		{
			return value;
		}

		Enumerator& operator=(Enumerator value)
		{
			this->value = value;
			return this->value;
		}

		Enumerator value;
	};

	// Special root structure, which holds all other configuration nodes.
	struct Root : public Node
	{
		Root() : Node(nullptr, "root") {}
	};

	// --- Configuration system structures ---
	// Each configuration node is a structure and all of them are in a hierarchical order.
	// Configuration nodes except those deriving from the root node need to pass a pointer of a parent node to the constructor as the first argument.
	// The second argument for all configuration entries is the name of the entry and the third should be the default value of the entry.

	// All configuration nodes should be declared here along with an extern definition, which ensures there will only ever be one instance of the node.
	// Definitions for nodes need to be added at the top of configuration.cpp.
	struct General : public Node
	{
		General() : Node("general") {}
	} extern general;

	struct CHIP8 : public Node
	{
		CHIP8() : Node("chip8") {}

		Enum<PixelRendererType> renderer{ this, "renderer", PixelRendererType::VULKAN };
	} extern chip8;

	struct GameBoy : public Node
	{
		GameBoy() : Node("game_boy") {}

		Enum<PixelRendererType> renderer{ this, "renderer", PixelRendererType::VULKAN };
		Boolean headless{ this, "headless", false };
	} extern game_boy;

	struct Developer : public Node
	{
		Developer() : Node("developer") {}

		Boolean enable_debugging{ this, "enable_debugging", false };
	} extern developer;

	// Loads the global configuration and sets the global configuration as the current configuration.
	bool load_global();

	// Tries to load the custom configuration file for the given executable and sets the custom configuration as the current configuration.
	// To set the global configuration as the current configuration, the global configuration must be reloaded.
	bool load_custom(Executable& executable);

	// Saves the current configuration, so it can be re-loaded later on.
	void push();

	// Pops and loads the last configuration that was pushed.
	bool pop();

	// Saves the current configuration. Works for both the global and custom configurations.
	bool save();
}