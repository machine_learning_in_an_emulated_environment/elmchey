#pragma once

#include "pixel_renderer.h"
#include "gui/opengl_frame.h"
#include "opengl/opengl_helper.h"

static std::string vertex_shader_source =
{
	"#version 450\n"
	"\n"
	"layout(location=1) out vec2 tex_coord;\n"
	"\n"
	"const vec2 quad_vertices[] = vec2[](vec2(-1.0, -1.0),\n"
	"                                    vec2(-1.0,  1.0),\n"
	"                                    vec2( 1.0, -1.0),\n"
	"                                    vec2( 1.0,  1.0));\n"
	"\n"
	"void main()\n"
	"{\n"
	"	gl_Position = vec4(quad_vertices[gl_VertexID], 0.0, 1.0);\n"
	"	gl_Position.y = -gl_Position.y;\n"
	"	tex_coord = quad_vertices[gl_VertexID];\n"
	"}\n"
};

static std::string fragment_shader_source =
{
	"#version 450\n"
	"\n"
	"layout(binding=0) uniform sampler2D pixel_texture;\n"
	"layout(location=0) out vec4 out_color;\n"
	"layout(location=1) in vec2 tex_coord;\n"
	"\n"
	"void main()\n"
	"{\n"
	"	out_color = texture(pixel_texture, ((tex_coord + 1.0) / 2.0).xy);\n"
	"}\n"
};

class OGLPixelRenderer45 : public PixelRenderer
{
public:
	OGLPixelRenderer45(std::string& title, u32& width, u32& height, u8& width_scale, u8& height_scale);
	bool initialize();
	bool draw();

private:
	void on_resize();

	// Dummy
	GLuint dummy_vertex_array;

	// For shaders
	GLuint vertex_shader;
	GLuint fragment_shader;
	GLuint shader_program;

	// For the texture
	GLuint pixel_texture;

	// For the PBO
	GLuint pixel_buffer;
	GLsizei pixel_buffer_size;
	void* pixel_buffer_data;
};