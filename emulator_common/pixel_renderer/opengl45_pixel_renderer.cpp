#include "stdafx_emulator.h"
#include "opengl45_pixel_renderer.h"

OGLPixelRenderer45::OGLPixelRenderer45(std::string& title, u32& width, u32& height, u8& width_scale, u8& height_scale) : PixelRenderer(width, height, width_scale, height_scale)
{
	wxGLAttributes attributes;
	attributes.RGBA();
	attributes.DoubleBuffer();
	attributes.Depth(0);
	attributes.SampleBuffers(1);
	attributes.Samplers(1);
	attributes.EndList();

	wxGLContextAttrs context_attributes;
	context_attributes.CoreProfile();
	context_attributes.ForwardCompatible();
	context_attributes.OGLVersion(4, 5);
	context_attributes.EndList();

	frame.reset(new OGLFrame(title, width * width_scale, height * height_scale, attributes, &context_attributes));
}

bool OGLPixelRenderer45::initialize()
{
	PixelRenderer::initialize();

	if (!frame->initialize(nullptr, nullptr))
	{
		error("Failed to initialize OpenGL frame for OpenGL 4.5 pixel renderer.");
		return false;
	}

	// Set the clear color
	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);

	// Create a VAO, because the driver will complain if no VAO is bound and we use glDrawArrays
	glCreateVertexArrays(1, &dummy_vertex_array);
	glBindVertexArray(dummy_vertex_array);

	// Create a texture to which we'll render to
	glCreateTextures(GL_TEXTURE_2D, 1, &pixel_texture);
	glBindTexture(GL_TEXTURE_2D, pixel_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Create the shaders
	if (!create_shader(&vertex_shader, GL_VERTEX_SHADER, vertex_shader_source.c_str()))
	{
		error("Failed to compile the vertex shader.");
		return false;
	}

	if (!create_shader(&fragment_shader, GL_FRAGMENT_SHADER, fragment_shader_source.c_str()))
	{
		error("Failed to compile the fragment shader.");
		return false;
	}

	// Create the shader program, attach the shaders, link the program and use it
	if (!create_shader_program(&shader_program, { vertex_shader, fragment_shader }))
	{
		error("Failed to create the shader program.");
		return false;
	}

	glUseProgram(shader_program);

	// Object names for debugging
	object_name(GL_VERTEX_ARRAY, dummy_vertex_array, "Fake vertex array");
	object_name(GL_TEXTURE, pixel_texture, "Pixel texture");
	object_name(GL_SHADER, vertex_shader, "Vertex shader");
	object_name(GL_SHADER, fragment_shader, "Fragment shader");
	object_name(GL_PROGRAM, shader_program, "Shader program");

	return true;
}

void OGLPixelRenderer45::on_resize()
{
	PixelRenderer::on_resize();

	// Resize the viewport
	glViewport(0, 0, surface_width, surface_height);

	// Calculate the pixel buffer size
	pixel_buffer_size = width * height * 4;

	// Create a pixel buffer for storing pixel data
	glCreateBuffers(1, &pixel_buffer);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pixel_buffer);
	glBufferStorage(GL_PIXEL_UNPACK_BUFFER, pixel_buffer_size, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);

	// Object names for debugging
	object_name(GL_BUFFER, pixel_buffer, "Pixel buffer");
}

bool OGLPixelRenderer45::draw()
{
	// Check if the size of the surface has changed
	u32 current_surface_width;
	u32 current_surface_height;

	frame->get_size(&current_surface_width, &current_surface_height);

	if (surface_width != current_surface_width || surface_height != current_surface_height)
	{
		surface_width = current_surface_width;
		surface_height = current_surface_height;

		on_resize();
	}

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Map and bind the PBO so we can write to it
	pixel_buffer_data = glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0, pixel_buffer_size, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	
	// Make sure the PBO is clean before we draw anything to it
	memset(pixel_buffer_data, 0x0, pixel_buffer_size);

	// Draw the image to the PBO
	for (u32 y = 0; y < height; ++y)
	{
		for (u32 x = 0; x < width; ++x)
		{
			if (pixels[(y * width) + x])
			{
				u32* pixel = static_cast<u32*>(pixel_buffer_data) + (y * width) + x;
				*pixel = 0xFFFFFFFF;
			}
		}
	}

	// Unmap the PBO
	glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);

	// Copy the data from PBO to the texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

	// Draw a fullscreen quad (vertex data for the quad is as a constant in the shader)
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (!frame->flip())
	{
		error("Failed to flip the frame.");
		return false;
	}

	return true;
}