#pragma once

#include "pixel_renderer.h"
#include "gui/vulkan_frame.h"

class VulkanPixelRenderer : public PixelRenderer
{
public:
	VulkanPixelRenderer(std::string& title, u32 width, u32 height, u8& width_scale, u8& height_scale, bool colour = false);
	~VulkanPixelRenderer();
	bool initialize();
	bool draw();

private:
	// General handles for Vulkan
	std::unique_ptr<VulkanHandles> handles;

	// The memory properties of the physical device we are using
	VkPhysicalDeviceMemoryProperties memory_properties;

	// The family index of the queue we are using
	u32 queue_family_index;

	// The handle for our logical device
	VkDevice device;

	// Our graphical rendering queue
	VkQueue queue;

	// For displaying stuff on the screen
	VkSwapchainKHR swapchain;
	VkPresentModeKHR present_mode;
	VkFormat surface_format;
	VkImage swapchain_images[2];

	// For recording commands
	VkCommandPool command_pool;
	VkCommandBuffer command_buffers[2];

	// We use the semaphore to synchronize the acquisition of images from the swapchain
	VkSemaphore swapchain_semaphore;

	// We use this to signal the completion of rendering. This ensures that the pixel buffer is not written to until its no longer used.
	VkFence submission_fence;

	// For the buffer to hold pixels
	VkBuffer pixel_buffer;
	VkDeviceMemory pixel_buffer_memory;
	VkDeviceSize pixel_buffer_size;
	void* pixel_buffer_data;

	// For the image that holds the rendered result
	VkImage pixel_image;
	VkDeviceMemory pixel_image_memory;

	// Function that recreates the swapchain whenever the window is resized.
	bool on_resize();

	// Initializes the frame, creating a Vulkan instance and the general Vulkan handles
	bool initialize_frame();

	// Creates the logical device that we'll use
	bool create_device();

	// Obtains an appropriate surface format, presentation mode and ensures that surface properties are appropriate for our use
	bool query_surface_properties();

	// Creates a command pool and allocates a command buffer
	bool allocate_command_buffer();

	// Creates the synchronization primitives we use
	bool create_synchronization_primitives();

	// Creates a pixel buffer, which we'll render the result to
	bool create_pixel_buffer();

	// Creates a pixel image, which we'll copy our buffers contents to and blit to the screen
	bool create_pixel_image();

	// Records the rendering commands to the command buffers
	bool record_command_buffers();
};