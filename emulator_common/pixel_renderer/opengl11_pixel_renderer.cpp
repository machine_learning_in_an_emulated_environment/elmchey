#include "stdafx_emulator.h"
#include "opengl11_pixel_renderer.h"

OGLPixelRenderer11::OGLPixelRenderer11(std::string& title, u32& width, u32& height, u8& width_scale, u8& height_scale) : PixelRenderer(width, height, width_scale, height_scale)
{
	wxGLAttributes attributes;
	attributes.RGBA();
	attributes.DoubleBuffer();
	attributes.Depth(0);
	attributes.SampleBuffers(0);
	attributes.Samplers(0);
	attributes.EndList();

	wxGLContextAttrs context_attributes;
	context_attributes.CompatibilityProfile();
	context_attributes.OGLVersion(1, 1);
	context_attributes.EndList();

	frame.reset(new OGLFrame(title, width * width_scale, height * height_scale, attributes, &context_attributes));
}

bool OGLPixelRenderer11::initialize()
{
	PixelRenderer::initialize();

	if (!frame->initialize(nullptr, nullptr))
	{
		error("Failed to initialize OpenGL frame for OpenGL 1.1 pixel renderer.");
		return false;
	}

	return true;
}

void OGLPixelRenderer11::on_resize()
{
	PixelRenderer::on_resize();

	// It's hard to support uneven scaling, so we just render according to the smallest scaling factor
	scaling = std::min(width_scale, height_scale);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, surface_width, surface_height);
	glOrtho(0, surface_width, surface_height, 0, 1, -1);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(scaling);

	scaling_offset = scaling / 2;
}

bool OGLPixelRenderer11::draw()
{
	u32 current_surface_width;
	u32 current_surface_height;

	frame->get_size(&current_surface_width, &current_surface_height);

	if (surface_width != current_surface_width || surface_height != current_surface_height)
	{
		surface_width = current_surface_width;
		surface_height = current_surface_height;

		// We need to recreate some objects if the window is resized
		on_resize();
	}

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT);

	// Draw the pixels
	glBegin(GL_POINTS);

	for (u32 y = 0; y < height; ++y)
	{
		for (u32 x = 0; x < width; ++x)
		{
			if (pixels[(y * width) + x])
			{
				glVertex2i((x * scaling) + scaling_offset, (y * scaling) + scaling_offset);
			}
		}
	}

	glEnd();

	if (!frame->flip())
	{
		error("Failed to flip the frame.");
		return false;
	}

	return true;
}