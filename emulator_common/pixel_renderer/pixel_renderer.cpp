#include "stdafx_emulator.h"
#include "pixel_renderer.h"

#include "vulkan_pixel_renderer.h"
#include "opengl45_pixel_renderer.h"
#include "opengl11_pixel_renderer.h"

PixelRenderer::PixelRenderer(u32& width, u32& height, u8& width_scale, u8& height_scale, bool colour)
{
	pixels = nullptr;
	surface_width = 0;
	surface_height = 0;

	this->colour = colour;
	this->width = width;
	this->width_scale = width_scale;
	this->height = height;
	this->height_scale = height_scale;
}

PixelRenderer* PixelRenderer::create(PixelRendererType renderer_type, std::string title, u32& width, u32& height, u8& width_scale, u8& height_scale, bool colour)
{
	switch (renderer_type)
	{
	case PixelRendererType::VULKAN:
		return new VulkanPixelRenderer(title, width, height, width_scale, height_scale, colour);

	case PixelRendererType::DX12:
		todo("DX12 pixel render");
		return nullptr;

	case PixelRendererType::OPENGL45:
		return new OGLPixelRenderer45(title, width, height, width_scale, height_scale);

	case PixelRendererType::OPENGL11:
		return new OGLPixelRenderer11(title, width, height, width_scale, height_scale);

	default:
		error("Unknown pixel renderer type: %d", renderer_type);
		return nullptr;
	}
}