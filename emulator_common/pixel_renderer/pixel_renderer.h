#pragma once

#include "gui/renderer_frame.h"
#include "configuration/configuration.h"

class PixelRenderer
{
public:
	PixelRenderer(u32& width, u32& height, u8& width_scale, u8& height_scale, bool colour = false);

	// Creates a PixelRenderer instance for the specified renderer
	static PixelRenderer* create(PixelRendererType renderer_type, std::string title, u32& width, u32& height, u8& width_scale, u8& height_scale, bool colour = false);

	// To ensure that the child's destructor gets called
	virtual ~PixelRenderer() {}

	// Initializes the PixelRenderer
	virtual bool initialize()
	{
		set_clamp();
		return true;
	};

	// Draws a new frame
	virtual bool draw() = 0;

	// Sets the size of the rendering surface and updates the clamping
	inline void set_size(u32& width, u32& height)
	{
		this->width = width;
		this->height = height;
		set_clamp();
	}

	// Set the array/pointer from which to obtain the pixels from
	inline void set_pixels(u8* pixels)
	{
		this->pixels = pixels;
	}
	
	// We expose it so the classes using the renderer can bind callbacks and perform other various operations
	std::unique_ptr<RendererFrame, RendererFrameDestructor> frame;

protected:
	// The intended native (non-upscaled) rendering resolution
	u32 width;
	u32 height;

	// Whether the pixels array should be treated as 24-bit colour buffer.
	bool colour;

	// Pointer to an array of pixels for rendering.
	// Array size without colour width * height, with colour width * height * 3.
	u8* pixels;

	// How many times the result should be upscaled in the given directions
	u8 width_scale;
	u8 height_scale;

	// For storing the client size of the rendering surface
	u32 surface_width;
	u32 surface_height;

	// Set the clamp for the frame resizing
	inline void set_clamp()
	{
		frame->set_resize_clamp(width, height);
	}

	// This function handles changes that need to be done when the surface area changes
	inline void on_resize()
	{
		width_scale = surface_width / width;
		height_scale = surface_height / height;
	}
};