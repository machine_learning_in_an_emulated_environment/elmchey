#include "stdafx_emulator.h"
#include "vulkan_pixel_renderer.h"
#include "vulkan/vulkan_helper.h"

VulkanPixelRenderer::VulkanPixelRenderer(std::string& title, u32 width, u32 height, u8& width_scale, u8& height_scale, bool colour) : PixelRenderer(width, height, width_scale, height_scale, colour)
{
	device = nullptr;
	swapchain = nullptr;
	pixel_buffer = nullptr;
	pixel_buffer_memory = nullptr;

	frame.reset(new VulkanFrame(title, width * width_scale, height * height_scale));
}

VulkanPixelRenderer::~VulkanPixelRenderer()
{
	// If the initialization failed completely we bail.
	if (!device)
	{
		return;
	}

	VkResult result;

	// We need to wait for the device to become idle, as the things we created might still be in use
	while ((result = vkDeviceWaitIdle(device)) != VK_SUCCESS)
	{
		error("Failed to wait for the device to become idle. (%d)", result);
		std::this_thread::sleep_for(1ms);
	}

	if (pixel_image_memory)
	{
		vkFreeMemory(device, pixel_image_memory, nullptr);
	}

	if (pixel_image)
	{
		vkDestroyImage(device, pixel_image, nullptr);
	}

	if (pixel_buffer_memory)
	{
		vkFreeMemory(device, pixel_buffer_memory, nullptr);
	}

	if (pixel_buffer)
	{
		vkDestroyBuffer(device, pixel_buffer, nullptr);
	}

	if (submission_fence)
	{
		vkDestroyFence(device, submission_fence, nullptr);
	}

	if (swapchain_semaphore)
	{
		vkDestroySemaphore(device, swapchain_semaphore, nullptr);
	}

	if (command_pool)
	{
		vkDestroyCommandPool(device, command_pool, nullptr);
	}

	if (swapchain)
	{
		vkDestroySwapchainKHR(device, swapchain, nullptr);
	}

	if (device)
	{
		vkDestroyDevice(device, nullptr);
	}
}

bool VulkanPixelRenderer::initialize()
{
	PixelRenderer::initialize();

	if (!initialize_frame())
	{
		return false;
	}

	if (!create_device())
	{
		return false;
	}

	if (!query_surface_properties())
	{
		return false;
	}

	if (!allocate_command_buffer())
	{
		return false;
	}

	if (!create_synchronization_primitives())
	{
		return false;
	}

	if (!create_pixel_buffer())
	{
		return false;
	}

	return create_pixel_image();
}

bool VulkanPixelRenderer::initialize_frame()
{
	// Initialize the frame for rendering
	VulkanOptions options;
#ifdef DEBUG
	options.enable_validation = true;
#endif
	options.device_number = 0; // TODO: Support changing this
	options.engine = "PixelRenderer";
	options.engine_version = 1;

	// Pointer to the Vulkan handles that were created by the frame
	VulkanHandles* handles_pointer;

	if (!frame->initialize(&options, &handles_pointer))
	{
		error("Failed to initialize Vulkan frame for Vulkan pixel renderer.");
		return false;
	}

	// Create the std::unique_ptr from the handles' pointer
	handles = std::unique_ptr<VulkanHandles>(handles_pointer);

	return true;
}

bool VulkanPixelRenderer::create_device()
{
	VkResult result;

	// Obtain the family index for the queue that we're going to use
	if (!get_family_index(handles->physical_device, handles->surface, &queue_family_index))
	{
		error("Failed to obtain the queue family index.");
		return false;
	}

	// Our graphics queue info
	float queue_priority = 1;

	VkDeviceQueueCreateInfo graphics_queue_info;
	graphics_queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	graphics_queue_info.pNext = nullptr;
	graphics_queue_info.flags = 0;
	graphics_queue_info.queueFamilyIndex = queue_family_index;
	graphics_queue_info.queueCount = 1;
	graphics_queue_info.pQueuePriorities = &queue_priority;

	// Create a Vulkan device
	std::vector<const char*> device_extensions =
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	VkDeviceCreateInfo device_info;
	device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_info.pNext = nullptr;
	device_info.flags = 0;
	device_info.queueCreateInfoCount = 1;
	device_info.pQueueCreateInfos = &graphics_queue_info;
	device_info.enabledLayerCount = static_cast<u32>(handles->validation_layers.size());
	device_info.ppEnabledLayerNames = handles->validation_layers.data();
	device_info.enabledExtensionCount = static_cast<u32>(device_extensions.size());
	device_info.ppEnabledExtensionNames = device_extensions.data();
	device_info.pEnabledFeatures = nullptr;

	if ((result = vkCreateDevice(handles->physical_device, &device_info, nullptr, &device)) != VK_SUCCESS)
	{
		error("Failed to create a Vulkan device handle. (%d)", result);
		return false;
	}

	// Get the graphics queue we created
	vkGetDeviceQueue(device, queue_family_index, 0, &queue);

	return true;
}

bool VulkanPixelRenderer::query_surface_properties()
{
	VkResult result;

	// Obtain the physical device memory properties. We need those later for memory allocation.
	vkGetPhysicalDeviceMemoryProperties(handles->physical_device, &memory_properties);

	// Obtain the available surface formats and presentation modes
	u32 surface_format_count;

	if ((result = vkGetPhysicalDeviceSurfaceFormatsKHR(handles->physical_device, handles->surface, &surface_format_count, nullptr)) != VK_SUCCESS)
	{
		error("Failed to obtain the number of device surface formats. (%d)", result);
		return false;
	}

	std::vector<VkSurfaceFormatKHR> surface_formats(surface_format_count);

	if ((result = vkGetPhysicalDeviceSurfaceFormatsKHR(handles->physical_device, handles->surface, &surface_format_count, surface_formats.data())) != VK_SUCCESS)
	{
		error("Failed to obtain the available surface formats. (%d)", result);
		return false;
	}

	// Doesn't really matter what surface format we choose, as our image is converted to the appropriate automatically by the driver
	surface_format = surface_formats[0].format;

	// VK_PRESENT_MODE_FIFO_KHR is guaranteed to always be available
	present_mode = VK_PRESENT_MODE_FIFO_KHR;

	return true;
}

bool VulkanPixelRenderer::allocate_command_buffer()
{
	VkResult result;

	// Create a command pool
	VkCommandPoolCreateInfo command_pool_info;
	command_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	command_pool_info.pNext = nullptr;
	command_pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	command_pool_info.queueFamilyIndex = queue_family_index;

	if ((result = vkCreateCommandPool(device, &command_pool_info, nullptr, &command_pool)) != VK_SUCCESS)
	{
		error("Failed to create a command pool. (%d)", result);
		return false;
	}

	// Allocate a command buffer
	VkCommandBufferAllocateInfo command_buffer_allocate_info;
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.pNext = nullptr;
	command_buffer_allocate_info.commandPool = command_pool;
	command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	command_buffer_allocate_info.commandBufferCount = 2;

	if ((result = vkAllocateCommandBuffers(device, &command_buffer_allocate_info, command_buffers)) != VK_SUCCESS)
	{
		error("Failed to allocate a command buffer. (%d)", result);
		return false;
	}

	return true;
}

bool VulkanPixelRenderer::create_synchronization_primitives()
{
	VkResult result;

	// Create the semaphore
	VkSemaphoreCreateInfo semaphore_info;
	semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphore_info.pNext = nullptr;
	semaphore_info.flags = 0;

	if ((result = vkCreateSemaphore(device, &semaphore_info, nullptr, &swapchain_semaphore)) != VK_SUCCESS)
	{
		error("Failed to create a semaphore for swapchain image acquisition synchronization. (%d)", result);
		return false;
	}

	// Create a fence to synchronize the completion of rendering
	// The fence is created as signaled, as we wait right at the beginning
	VkFenceCreateInfo fence_info;
	fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fence_info.pNext = nullptr;
	fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	if ((result = vkCreateFence(device, &fence_info, nullptr, &submission_fence)) != VK_SUCCESS)
	{
		error("Failed to create a fence for rendering synchronization. (%d)", result);
		return false;
	}

	return true;
}

bool VulkanPixelRenderer::create_pixel_buffer()
{
	VkResult result;

	// Calculate the size of the pixel buffer
	pixel_buffer_size = width * height * 4;

	// Create a pixel buffer for storing the rendering result
	VkBufferCreateInfo buffer_info;
	buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	buffer_info.pNext = nullptr;
	buffer_info.flags = 0;
	buffer_info.size = pixel_buffer_size;
	buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	buffer_info.queueFamilyIndexCount = 0;
	buffer_info.pQueueFamilyIndices = nullptr;

	if ((result = vkCreateBuffer(device, &buffer_info, nullptr, &pixel_buffer)) != VK_SUCCESS)
	{
		error("Failed to create a pixel buffer. (%d)", result);
		return false;
	}

	// Get the memory requirements for the buffer
	VkMemoryRequirements memory_requirements;
	vkGetBufferMemoryRequirements(device, pixel_buffer, &memory_requirements);

	// Memory allocation info
	VkMemoryAllocateInfo allocation_info;
	allocation_info.pNext = nullptr;
	allocation_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocation_info.allocationSize = memory_requirements.size;

	// Get the appropriate memory allocation type
	if (!get_memory_type(memory_properties, memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &allocation_info.memoryTypeIndex))
	{
		error("Failed to obtain the appropriate memory type for the pixel buffer.");
		return false;
	}

	// Allocate the memory
	if ((result = vkAllocateMemory(device, &allocation_info, nullptr, &pixel_buffer_memory)) != VK_SUCCESS)
	{
		error("Failed to allocate memory for the pixel buffer. (%d)", result);
		return false;
	}

	// Bind memory to the buffer
	if ((result = vkBindBufferMemory(device, pixel_buffer, pixel_buffer_memory, 0)) != VK_SUCCESS)
	{
		error("Failed to bind memory to the pixel buffer. (%d)", result);
		return false;
	}

	// Map the buffer memory, so we can access it
	if ((result = vkMapMemory(device, pixel_buffer_memory, 0, memory_requirements.size, 0, &pixel_buffer_data)) != VK_SUCCESS)
	{
		error("Failed to map buffer memory. (%d)", result);
		return false;
	}

	return true;
}

bool VulkanPixelRenderer::create_pixel_image()
{
	VkResult result;

	// Create an image which we'll display to the screen
	VkImageCreateInfo image_info;
	image_info.pNext = nullptr;
	image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_info.flags = 0;
	image_info.imageType = VK_IMAGE_TYPE_2D;
	image_info.format = VK_FORMAT_B8G8R8A8_UNORM;
	image_info.extent = { width, height, 1 };
	image_info.mipLevels = 1;
	image_info.arrayLayers = 1;
	image_info.samples = VK_SAMPLE_COUNT_1_BIT;
	image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
	image_info.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	image_info.queueFamilyIndexCount = 0;
	image_info.pQueueFamilyIndices = nullptr;
	image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	if ((result = vkCreateImage(device, &image_info, nullptr, &pixel_image)) != VK_SUCCESS)
	{
		error("Failed to create a pixel image. (%d)", result);
		return false;
	}

	// Get the memory requirements for the image
	VkMemoryRequirements memory_requirements;
	vkGetImageMemoryRequirements(device, pixel_image, &memory_requirements);

	// Memory allocation info
	VkMemoryAllocateInfo allocation_info;
	allocation_info.pNext = nullptr;
	allocation_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocation_info.allocationSize = memory_requirements.size;

	// Get the appropriate memory allocation type
	if (!get_memory_type(memory_properties, memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &allocation_info.memoryTypeIndex))
	{
		error("Failed to obtain the appropriate memory type for the pixel image.");
		return false;
	}

	// Allocate the memory
	if ((result = vkAllocateMemory(device, &allocation_info, nullptr, &pixel_image_memory)) != VK_SUCCESS)
	{
		error("Failed to allocate memory for the pixel image. (%d)", result);
		return false;
	}

	// Bind memory to the image
	if ((result = vkBindImageMemory(device, pixel_image, pixel_image_memory, 0)) != VK_SUCCESS)
	{
		error("Failed to bind memory to the pixel buffer. (%d)", result);
		return false;
	}

	return true;
}

bool VulkanPixelRenderer::record_command_buffers()
{
	VkResult result;

	for (u8 i = 0; i < 2; ++i)
	{
		// Start recording to the command buffer
		VkCommandBufferBeginInfo begin_info;
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.pNext = nullptr;
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		begin_info.pInheritanceInfo = nullptr;

		if ((result = vkBeginCommandBuffer(command_buffers[i], &begin_info)) != VK_SUCCESS)
		{
			error("Failed to start recording to the command buffer %d. (%d)", i, result);
			return false;
		}

		// The common subresource thingies (they must match!)
		VkImageSubresourceRange subresource_range;
		subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource_range.baseMipLevel = 0;
		subresource_range.levelCount = 1;
		subresource_range.baseArrayLayer = 0;
		subresource_range.layerCount = 1;

		VkImageSubresourceLayers subresource;
		subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource.mipLevel = 0;
		subresource.baseArrayLayer = 0;
		subresource.layerCount = 1;

		// Make sure that the buffer is no longer writable and is optimal for being read from
		VkBufferMemoryBarrier buffer_barrier;
		buffer_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
		buffer_barrier.pNext = nullptr;
		buffer_barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		buffer_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		buffer_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		buffer_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		buffer_barrier.buffer = pixel_buffer;
		buffer_barrier.offset = 0;
		buffer_barrier.size = pixel_buffer_size;

		// Transfer the swapchain image to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, so we can blit to it
		VkImageMemoryBarrier clear_barrier;
		clear_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		clear_barrier.pNext = nullptr;
		clear_barrier.srcAccessMask = 0;
		clear_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		clear_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		clear_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		clear_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		clear_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		clear_barrier.image = swapchain_images[i];
		clear_barrier.subresourceRange = subresource_range;

		// Transfer the pixel image to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, so we can copy our buffer to it
		VkImageMemoryBarrier copy_barrier;
		copy_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		copy_barrier.pNext = nullptr;
		copy_barrier.srcAccessMask = 0;
		copy_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		copy_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		copy_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		copy_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier.image = pixel_image;
		copy_barrier.subresourceRange = subresource_range;

		pipeline_barrier(command_buffers[i], VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, {}, { buffer_barrier }, { clear_barrier, copy_barrier });

		// Copy the contents of the buffer to the image
		VkBufferImageCopy image_copy;
		image_copy.bufferOffset = 0;
		image_copy.bufferRowLength = 0;
		image_copy.bufferImageHeight = 0;
		image_copy.imageSubresource = subresource;
		image_copy.imageOffset = {};
		image_copy.imageExtent = { width, height, 1 };

		vkCmdCopyBufferToImage(command_buffers[i], pixel_buffer, pixel_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);

		// Transfer the pixel image to VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, so we can blit from it to the swapchain
		VkImageMemoryBarrier blit_barrier;
		blit_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		blit_barrier.pNext = nullptr;
		blit_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		blit_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		blit_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		blit_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		blit_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		blit_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		blit_barrier.image = pixel_image;
		blit_barrier.subresourceRange = subresource_range;

		pipeline_barrier(command_buffers[i], VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, {}, {}, { blit_barrier });

		// Blit the image to the swapchain image
		VkImageBlit image_blit;
		image_blit.srcSubresource = subresource;
		image_blit.srcOffsets[0] = {};
		image_blit.srcOffsets[1] = { static_cast<s32>(width), static_cast<s32>(height), 1 };
		image_blit.dstSubresource = subresource;
		image_blit.dstOffsets[0] = {};
		image_blit.dstOffsets[1] = { static_cast<s32>(surface_width), static_cast<s32>(surface_height), 1 };

		vkCmdBlitImage(command_buffers[i], pixel_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, swapchain_images[i], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_blit, VK_FILTER_NEAREST);

		// Transfer to the presentation layout
		VkImageMemoryBarrier present_barrier;
		present_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		present_barrier.pNext = nullptr;
		present_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		present_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		present_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		present_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		present_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		present_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		present_barrier.image = swapchain_images[i];
		present_barrier.subresourceRange = subresource_range;

		pipeline_barrier(command_buffers[i], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT | VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, {}, {}, { present_barrier });

		// Stop recording to the command buffer
		if ((result = vkEndCommandBuffer(command_buffers[i])) != VK_SUCCESS)
		{
			error("Failed to end recording to the command buffer %d. (%d)", i, result);
			return false;
		}
	}

	return true;
}

bool VulkanPixelRenderer::on_resize()
{
	PixelRenderer::on_resize();

	VkResult result;

	// Get the surface size for the new swapchain
	VkSurfaceCapabilitiesKHR surface_capabilities;

	// NOTE: Race condition, receives resize event before initialise_frame is called!
	if ((result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(handles->physical_device, handles->surface, &surface_capabilities)) != VK_SUCCESS)
	{
		error("Failed to obtain surface capbilities. (%d)", result);
		return false;
	}

	// Recreate the swapchain
	VkSwapchainCreateInfoKHR swapchain_info;
	swapchain_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchain_info.pNext = nullptr;
	swapchain_info.flags = 0;
	swapchain_info.surface = handles->surface;
	swapchain_info.minImageCount = 2;
	swapchain_info.imageFormat = surface_format;
	swapchain_info.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	swapchain_info.imageExtent = surface_capabilities.currentExtent;
	swapchain_info.imageArrayLayers = 1;
	swapchain_info.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	swapchain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchain_info.queueFamilyIndexCount = 0;
	swapchain_info.pQueueFamilyIndices = nullptr;
	swapchain_info.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchain_info.presentMode = present_mode;
	swapchain_info.clipped = true;
	swapchain_info.oldSwapchain = swapchain;

	if (!create_swapchain(device, swapchain, swapchain_info, swapchain_images))
	{
		error("Failed to create a new swapchain.");
		return false;
	}

	// We need to record the commands to the command buffers, because we have new swapchain image handles
	if (!record_command_buffers())
	{
		error("Failed to record to the command buffers.");
		return false;
	}
	
	return true;
}

bool VulkanPixelRenderer::draw()
{
	VkResult result;

	// Check if the size of the surface has changed
	u32 current_surface_width;
	u32 current_surface_height;

	frame->get_size(&current_surface_width, &current_surface_height);

	if (surface_width != current_surface_width || surface_height != current_surface_height)
	{
		surface_width = current_surface_width;
		surface_height = current_surface_height;

		if (!on_resize())
		{
			return false;
		}
	}

	// Wait for the command buffer to finish execution, before recording to the command buffer again.
	if ((result = vkWaitForFences(device, 1, &submission_fence, true, UINT64_MAX)) != VK_SUCCESS)
	{
		error("Failed to wait on the submission fence. (%d)", result);
		return false;
	}

	// And reset the fences
	if ((result = vkResetFences(device, 1, &submission_fence)) != VK_SUCCESS)
	{
		error("reset_fences: Failed to reset fences. (%d)", result);
		return false;
	}

	// Make sure that the buffer is clear before drawing
	memset(pixel_buffer_data, 0, pixel_buffer_size);

	// Draw to the buffer
	if (colour)
	{
		for (u32 y = 0; y < height; ++y)
		{
			for (u32 x = 0; x < width; ++x)
			{
				u32 offset = (y * width * 3) + (x * 3);
				u32* pixel = static_cast<u32*>(pixel_buffer_data) + (y * width) + x;
				*pixel |= pixels[offset] << 16;
				*pixel |= pixels[offset + 1] << 8;
				*pixel |= pixels[offset + 2];
			}
		}
	}
	else
	{
		for (u32 y = 0; y < height; ++y)
		{
			for (u32 x = 0; x < width; ++x)
			{
				if (pixels[(y * width) + x])
				{
					u32* pixel = static_cast<u32*>(pixel_buffer_data) + (y * width) + x;
					*pixel = 0xFFFFFFFF;
				}
			}
		}
	}

	// Acquire the index for the next image. Note that this is asynchronous and we have a semaphore that we wait on before the actual use of the index.
	u32 image_index;

	if ((result = vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, swapchain_semaphore, nullptr, &image_index)) != VK_SUCCESS)
	{
		error("Failed to obtain index of the next swapchain image. (%d)", result);
		return false;
	}

	// Submit the work to the queue along with the semaphore, that indicates if the next image from the swapchain is ready for rendering
	VkPipelineStageFlags stage_mask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

	VkSubmitInfo submit_info;
	submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submit_info.pNext = nullptr;
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = &swapchain_semaphore;
	submit_info.pWaitDstStageMask = &stage_mask;
	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &command_buffers[image_index];
	submit_info.signalSemaphoreCount = 0;
	submit_info.pSignalSemaphores = nullptr;

	if ((result = vkQueueSubmit(queue, 1, &submit_info, submission_fence)) != VK_SUCCESS)
	{
		error("Failed to submit work to the queue. (%d)", result);
		return false;
	}

	// Present the queue result to the swapchain.
	VkPresentInfoKHR present_info;
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.pNext = nullptr;
	present_info.waitSemaphoreCount = 0;
	present_info.pWaitSemaphores = nullptr;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &swapchain;
	present_info.pImageIndices = &image_index;
	present_info.pResults = nullptr;

	if ((result = vkQueuePresentKHR(queue, &present_info)) != VK_SUCCESS)
	{
		error("Failed to present the queue to the swapchain. (%d)", result);
		return false;
	}

	if (!frame->flip())
	{
		error("Failed to flip the frame.");
		return false;
	}

	return true;
}