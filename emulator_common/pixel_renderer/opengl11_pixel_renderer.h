#pragma once

#include "pixel_renderer.h"
#include "gui/opengl_frame.h"

class OGLPixelRenderer11 : public PixelRenderer
{
public:
	OGLPixelRenderer11(std::string& title, u32& width, u32& height, u8& width_scale, u8& height_scale);
	bool initialize();
	bool draw();

private:
	void on_resize();

	u8 scaling;
	u32 scaling_offset;
};