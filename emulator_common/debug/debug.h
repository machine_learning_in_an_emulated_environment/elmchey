#pragma once

class ByteBuffer;
struct Executable;

namespace dbg
{
	// Initializes the debugging interface and waits for a connection from the debugger.
	bool initialize();

	// Notifies the debugger that we started an executable along with information about it.
	void notify_system_startup(Executable& executable);

	// Allows the emulator to send debug messages to the debugger.
	void send(ByteBuffer& buffer);

	// A wrapper for sending an error message to the debugger.
	void send_error(u8 error_code, std::string error_message);
}