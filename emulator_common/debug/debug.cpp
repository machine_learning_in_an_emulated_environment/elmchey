#include "stdafx_emulator.h"
#include "debug.h"

#include "debugger.h"

#include "misc/byte_buffer.h"
#include "networking/networking.h"

#ifdef WINDOWS
	#include <iphlpapi.h>
	#include <MSWSock.h>
#endif

namespace dbg
{
	net::socket socket;
	std::thread thread;

	// The debug thread main function, which communicates with the debugger.
	void communicate();
}

bool dbg::initialize()
{
	net::socket listen_socket;

	if (!net::initialize())
	{
		error("Failed to initialize networking for debugging.");
		return false;
	}

	if (!listen_socket.create(net::socket::family::IP6, net::socket::type::STREAM, net::socket::protocol::TCP))
	{
		error("Failed to create debug server socket.");
		return false;
	}

	listen_socket.enable_loopback_optimization();

#ifdef WINDOWS
	bool result;

	// TODO: Support selecting adapters, once we implement this. For now only support loopback.
	if constexpr(true)
	{
		sockaddr_in6 address;
		address.sin6_family = AF_INET6;
		address.sin6_port = htons(6780);
		address.sin6_flowinfo = 0;
		address.sin6_scope_id = 0;

		// Clear out the address memory and set the address as the loopback address
		std::memset(&address.sin6_addr.u, 0, 15);
		address.sin6_addr.u.Byte[15] = 0x1;

		result = listen_socket.bind(address);
	}
	else
	{
		// First get the required buffer size
		ULONG addresses_size = 0;
		ULONG result;

		if (result = GetAdaptersAddresses(AF_INET6, 0, nullptr, nullptr, &addresses_size); result != ERROR_BUFFER_OVERFLOW)
		{
			error("Failed to get the size for adapter addresses buffer. (%d)", result);
			return false;
		}

		IP_ADAPTER_ADDRESSES* addresses = reinterpret_cast<IP_ADAPTER_ADDRESSES*>(std::malloc(addresses_size));

		if (result = GetAdaptersAddresses(AF_INET6, 0, nullptr, addresses, &addresses_size); result != ERROR_SUCCESS)
		{
			error("Failed to get adapter addresses. (%d)", result);
			return false;
		}

		sockaddr_in6& address = *reinterpret_cast<sockaddr_in6*>(addresses->FirstUnicastAddress->Address.lpSockaddr);
		result = listen_socket.bind(address);

		std::free(addresses);
	}
#endif

	if (!result)
	{
		error("Failed to bind the debug server socket.");
		return false;
	}

	if (!listen_socket.listen(1))
	{
		error("Failed to start listening for a debugger connection.");
		return false;
	}

	// TODO: Use AcceptEx() for asynchronity
	if (!listen_socket.accept(socket))
	{
		error("Failed to accept debugger connection");
		return false;
	}

	if (!listen_socket.close())
	{
		error("Failed to close the listen socket.");
		return false;
	}

	// Send the protocol version used by the server
	ByteBuffer buffer;
	buffer.push16(dbg::protocol_version);

	if (!socket.send(buffer))
	{
		error("Failed to send protocol information.");
		return false;
	}

	// Create a thread for communicating with the debugger
	thread = std::thread(communicate);
	set_thread_name(thread, "Debugger thread");

	return true;
}

void dbg::notify_system_startup(Executable& executable)
{
	ByteBuffer buffer;
	buffer.push8(dbg::SYSTEM_STARTUP);
	buffer.push8(static_cast<u8>(executable.system));
	buffer.push_string(executable.name);

	if (!socket.send(buffer))
	{
		error("Failed to send system startup notification.");
	}
}

void dbg::send(ByteBuffer& buffer)
{
	socket.send(buffer);
}

void dbg::send_error(u8 error_code, std::string error_message)
{
	ByteBuffer error_buffer;
	error_buffer.push8(dbg::ERROR_MESSAGE);
	error_buffer.push8(error_code);
	error_buffer.push_string(error_message);

	if (!socket.send(error_buffer))
	{
		error("Failed to send error message to the debugger. (error %d – %s)", error_code, error_message.c_str());
	}
}

void dbg::communicate()
{
	HANDLE network_event = WSACreateEvent();

	if (WSAEventSelect(socket, network_event, FD_READ | FD_CLOSE) != 0)
	{
		error("Failed to select socket events for monitoring. (%d)", WSAGetLastError());
		return;
	}

	// Wait for network events to occur
	while (true)
	{
		if (WaitForSingleObject(network_event, INFINITE) != WSA_WAIT_EVENT_0)
		{
			error("Failed to wait for network events. (%d)", WSAGetLastError());
			break;
		}

		WSANETWORKEVENTS events;

		if (WSAEnumNetworkEvents(socket, network_event, &events) != 0)
		{
			error("Failed to enumerate network events. (%d)", WSAGetLastError());
			break;
		}

		// Work around a Windows bug.
		// Windows signals that an event we're subscribed to occurred.
		// Yet then when we ask for which events occurred, it tells us none occurred.
		if (events.lNetworkEvents == 0)
		{
			continue;
		}

		// Check if the socket has been closed
		if (events.lNetworkEvents & FD_CLOSE)
		{
			info("The debugger has closed the socket.");
			break;
		}

		// If we didn't receive a close event, then it must be a read event.
		ByteBuffer buffer;

		if (!socket.receive_available(buffer))
		{
			error("Failed to receive data from the debugger.");
			break;
		}

		while (buffer.data_available())
		{
			u8& message = buffer.get8();
			emu->handle_message(message, buffer);
		}
	}

	WSACloseEvent(network_event);

	// Make sure to close the socket
	if (!socket.close())
	{
		error("Failed to close the debug socket.");
	}

	// If the emulator exited by itself, then its instance will have already been destroyed.
	// But if the debugger closes the connection, then we need to destroy the instance ourselves here.
	if (emu)
	{
		emu.reset();
	}
}