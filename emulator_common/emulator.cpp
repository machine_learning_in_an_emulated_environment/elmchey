#include "stdafx_emulator.h"
#include "emulator.h"

#include "debug/debug.h"

void Emulator::start()
{
#ifdef DEBUGGER
	if (cfg::developer.enable_debugging)
	{
		dbg::notify_system_startup(executable);
		emulation_thread = std::thread(&Emulator::run_debugger, this);
	}
	else
#endif
	{
		emulation_thread = std::thread(&Emulator::run, this);
	}

	set_thread_name(emulation_thread, "Emulation thread");
}

void Emulator::exit(wxCloseEvent& event)
{
	exit();
}