#include "stdafx.h"
#include "chip8.h"
#include "chip8/cpu.h"

CHIP8::CHIP8(Executable& executable) : Emulator(executable), instructions(0x40)
{
	instructions[chip8::Instruction::CLEAR]          = &CHIP8::clear;
	instructions[chip8::Instruction::RETURN]         = &CHIP8::ret;
	instructions[chip8::Instruction::JUMP]           = &CHIP8::jump;
	instructions[chip8::Instruction::CALL]           = &CHIP8::call;
	instructions[chip8::Instruction::SKIP_EQUAL_NN]  = &CHIP8::skip_equal_nn;
	instructions[chip8::Instruction::SKIP_NEQUAL_NN] = &CHIP8::skip_nequal_nn;
	instructions[chip8::Instruction::SET_NN]         = &CHIP8::set_NN;
	instructions[chip8::Instruction::ADD_NN]         = &CHIP8::add_NN;
	instructions[chip8::Instruction::SET_VX_VY]      = &CHIP8::set_VX_VY;
	instructions[chip8::Instruction::ADD]            = &CHIP8::add;
	instructions[chip8::Instruction::SUBTRACT]       = &CHIP8::subtract;
	instructions[chip8::Instruction::SHIFT_RIGHT]    = &CHIP8::shift_right;
	instructions[chip8::Instruction::SHIFT_LEFT]     = &CHIP8::shift_left;
	instructions[chip8::Instruction::SKIP_NEQUAL]    = &CHIP8::skip_nequal;
	instructions[chip8::Instruction::SET_I]          = &CHIP8::set_I;
	instructions[chip8::Instruction::RANDOM]         = &CHIP8::random;
	instructions[chip8::Instruction::DRAW]           = &CHIP8::draw;
	instructions[chip8::Instruction::SKIP_PRESSED]   = &CHIP8::skip_pressed;
	instructions[chip8::Instruction::SKIP_NPRESSED]  = &CHIP8::skip_npressed;
	instructions[chip8::Instruction::GET_DELAY]      = &CHIP8::get_delay;
	instructions[chip8::Instruction::WAIT_KEY]       = &CHIP8::wait_key;
	instructions[chip8::Instruction::SET_DELAY]      = &CHIP8::set_delay;
	instructions[chip8::Instruction::SET_SOUND]      = &CHIP8::set_sound;
	instructions[chip8::Instruction::ADD_I]          = &CHIP8::add_I;
	instructions[chip8::Instruction::SPRITE]         = &CHIP8::sprite;
	instructions[chip8::Instruction::STORE_DECIMAL]  = &CHIP8::store_decimal;
	instructions[chip8::Instruction::STORE_I_VX]     = &CHIP8::store_I_VX;
	instructions[chip8::Instruction::STORE_VX_I]     = &CHIP8::store_VX_I;
	instructions[chip8::Instruction::UNKNOWN]        = &CHIP8::unknown;
}

CHIP8::~CHIP8()
{
	running = false;

	// If we're waiting for a keypress we need to notify condition variable so the thread can exit
	key_pressed.notify_all();
	
	if (timer_thread.joinable())
	{
		timer_thread.join();
	}
}

bool CHIP8::load_rom()
{
	// Open the ROM file
	std::ifstream rom(executable.path.c_str(), std::ios::binary);

	if (!rom)
	{
		error("Failed to open ROM file: %s", std::strerror(errno));
		return false;
	}

	// Get the size of the file
	u64 length = fs::file_size(executable.path);

	// The ROM must not be bigger than (0x1000 - 0x200) = 0xE00 bytes.
	if (length > 0xE00)
	{
		error("The ROM is too big! (0x%X)", length);
		return false;
	}

	// Read the data into memory at address 0x200
	rom.read(reinterpret_cast<char*>(memory + 0x200), length);

	return true;
}

bool CHIP8::reset()
{
	running = true;
	emulating = true;
	redraw = false;

	// Reset sizes
	width = 64;
	height = 32;
	scaling = 3;
	pixels.resize(width * height);

	// Timers and keypad
	delay_timer = 0;
	sound_timer = 0;
	last_key = 0;

	// Reset registers
	PC = 0x200;
	I = 0;
	SP = 0;
	std::memset(V, 0, 16);
	std::memset(memory, 0, 4096);
	std::memset(stack, 0, 16 * 2);

	// Copy 5-byte character sprites (16 total) to the start of the memory. Takes up a total of 0x50 bytes.
	std::array<u8, 0x50> fontset =
	{
		0xF0, 0x90, 0x90, 0x90, 0xF0,
		0x20, 0x60, 0x20, 0x20, 0x70,
		0xF0, 0x10, 0xF0, 0x80, 0xF0,
		0xF0, 0x10, 0xF0, 0x10, 0xF0,
		0x90, 0x90, 0xF0, 0x10, 0x10,
		0xF0, 0x80, 0xF0, 0x10, 0xF0,
		0xF0, 0x80, 0xF0, 0x90, 0xF0,
		0xF0, 0x10, 0x20, 0x40, 0x40,
		0xF0, 0x90, 0xF0, 0x90, 0xF0,
		0xF0, 0x90, 0xF0, 0x10, 0xF0,
		0xF0, 0x90, 0xF0, 0x90, 0x90,
		0xE0, 0x90, 0xE0, 0x90, 0xE0,
		0xF0, 0x80, 0x80, 0x80, 0xF0,
		0xE0, 0x90, 0x90, 0x90, 0xE0,
		0xF0, 0x80, 0xF0, 0x80, 0xF0,
		0xF0, 0x80, 0xF0, 0x80, 0x80,
	};

	std::memcpy(memory, fontset.data(), fontset.size());

	// Load the ROM
	if (!load_rom())
	{
		error("Failed to load the ROM.");
		return false;
	}

	return true;
}

bool CHIP8::initialize()
{
	// Reset the emulator core
	if (!reset())
	{
		error("Failed to reset the CHIP8 emulator.");
		return false;
	}

	// Create the renderer
	renderer.reset(PixelRenderer::create(cfg::chip8.renderer, executable.name, width, height, scaling, scaling));

	if (!renderer)
	{
		error("Failed to create a pixel renderer instance.");
		return false;
	}

	// Set the pointer to the array of pixels
	renderer->set_pixels(pixels.data());

	// Bind key callbacks so we can detect key presses
	renderer->frame->bind_key_down(&CHIP8::key_down, this);
	renderer->frame->bind_key_up(&CHIP8::key_up, this);

	// Bind the close callback
	renderer->frame->bind_close(&CHIP8::exit, reinterpret_cast<Emulator*>(this));

	// Setup the mt19937 random number generator
	rng = std::mt19937(seed());
	gen = std::uniform_int_distribution<>(0, 0xFF);

	// Create a timer thread that counts decrements the timers at ~60Hz.
	timer_thread = std::thread([&]()
	{
		Timer loop_timer;
		u8 frequency = 60; // TODO: It would be cool to allow changing this!

		while (running)
		{
			loop_timer.start();

			if (delay_timer > 0)
			{
				--delay_timer;
			}

			if (sound_timer > 0)
			{
				// FIXME: This is always true?
				if (sound_timer)
				{
#ifdef WINDOWS
					// Needs to be at least 22ms to be actually heard
					// TODO: Replace with an asynchronous method
					Beep(0x980, 22);
#else
					// TODO: Cross-platform
					#warning "Beeping isn't supported :("
#endif
				}

				--sound_timer;
			}

			std::this_thread::sleep_for((1000ms / frequency) - loop_timer.elapsed_time_milliseconds());
		}
	});

	set_thread_name(timer_thread, "Timer thread");

	return true;
}

void CHIP8::run()
{
	// We initialize the renderer here, as some renderers need the initialization to be done on the emulation thread.
	if (!renderer->initialize())
	{
		error("Failed to initialize the pixel renderer for CHIP8.");
		return;
	}

	Timer execution_timer;
	target_ips = 120;

	while (running)
	{
		execution_timer.start();

		if (emulating)
		{
			// Fetch the instruction
			instruction = (memory[PC] << 8) | memory[PC + 1];
			PC += 2;

			// Decode
			chip8::Instruction instruction = chip8::decode(this->instruction);
			//debug("0x%04X: 0x%04X (%d), %s", PC - 2, this->instruction, instruction, instruction_string(instruction).c_str());

			// Execute
			(this->*(instructions[static_cast<u8>(instruction)]))();
		}

		if (redraw || !emulating)
		{
			if (!renderer->draw())
			{
				error("Pixel renderer failed to draw. Stopping execution.");
				running = false;
			}

			redraw = false;
		}

		std::this_thread::sleep_for((1000ms / target_ips) - execution_timer.elapsed_time_milliseconds());
	}
}

void CHIP8::run_debugger()
{
	todo("Debugging not supported for CHIP8.");
	exit();
}

void CHIP8::handle_message(u8& message, ByteBuffer& buffer)
{
}

void CHIP8::key_down(wxKeyEvent& event)
{
	debug("key_down: %d", event.GetUnicodeKey());
	std::lock_guard<std::mutex> lock(key_press_mutex);
	key_pressed.notify_all();
}

void CHIP8::key_up(wxKeyEvent& event)
{
	debug("key_up: %d", event.GetUnicodeKey());
}

void CHIP8::unknown()
{
	error("Unknown instruction: 0x%04X", instruction);
}

void CHIP8::clear()
{
	memset(pixels.data(), 0, pixels.size());
	redraw = true;
}

void CHIP8::ret()
{
	PC = stack[SP--];
}

void CHIP8::jump()
{
	u16 destination = instruction & 0xFFF;

	// Check if the jump is to the same location as we are right now.
	// If it is, we exit the emulation loop and simply redraw the screen once a second (to ensure that resizing the window works).
	// This prevents pointless CPU time waste.
	if (destination == (PC - 2))
	{
		emulating = false;
		target_ips = 1;
	}

	PC = destination;
}

void CHIP8::call()
{
	stack[++SP] = PC;
	PC = instruction & 0xFFF;
}

void CHIP8::skip_equal_nn()
{
	if (V[(instruction >> 8) & 0xF] == (instruction & 0xFF))
	{
		PC += 2;
	}
}

void CHIP8::skip_nequal_nn()
{
	if (V[(instruction >> 8) & 0xF] != (instruction & 0xFF))
	{
		PC += 2;
	}
}

void CHIP8::set_NN()
{
	V[(instruction >> 8) & 0xF] = instruction & 0xFF;
}

void CHIP8::add_NN()
{
	V[(instruction >> 8) & 0xF] += instruction & 0xFF;
}

void CHIP8::set_VX_VY()
{
	V[(instruction >> 8) & 0xF] = V[(instruction >> 4) & 0xF];
}

void CHIP8::add()
{
	u8 X = (instruction >> 8) & 0xF;
	u8 Y = (instruction >> 4) & 0xF;

	if (Y > (0xFF - X))
	{
		V[0xF] = 1;
	}
	else
	{
		V[0xF] = 0;
	}

	V[X] += V[Y];
}

void CHIP8::subtract()
{
	u8 X = (instruction >> 8) & 0xF;
	u8 Y = (instruction >> 4) & 0xF;

	if (V[Y] > V[X])
	{
		V[0xF] = 0;
	}
	else
	{
		V[0xF] = 1;
	}

	V[X] -= V[Y];
}

void CHIP8::shift_right()
{
	u8 X = (instruction >> 8) & 0xF;

	V[0xF] = V[X] & 1;
	V[X] >>= 1;
}

void CHIP8::shift_left()
{
	u8 X = (instruction >> 8) & 0xF;

	V[0xF] = (V[X] >> 7) & 0xF;
	V[X] <<= 1;
}

void CHIP8::skip_nequal()
{
	if (V[(instruction >> 8) & 0xF] != V[(instruction >> 4) & 0xF])
	{
		PC += 2;
	}
}

void CHIP8::set_I()
{
	I = instruction & 0xFFF;
}

void CHIP8::random()
{
	V[(instruction >> 8) & 0xF] = gen(rng) & (instruction & 0xFF);
}

void CHIP8::draw()
{
	u8 x = V[(instruction >> 8) & 0xF];
	u8 y = V[(instruction >> 4) & 0xF];
	u8 height = instruction & 0xF;

	// Set the collision flag off
	V[0xF] = 0;

	// In multiples of 64 and 32 for x and y respectively the sprites wrap around to the other side.
	x %= 64;
	y %= 32;

	// When the sprite's height is higher than the screen's height, then we calculate the new height
	// for the amount of pixels that can be displayed before the cut-off.
	if (y > 24 && height > (32 % y))
	{
		height = 32 % y;
	}

	for (u8 sprite_y = 0; sprite_y < height; ++sprite_y)
	{
		u8 data = memory[I + sprite_y];
		u8 width = 8;

		// Cut-off detection for the x-axis.
		if (x > 56)
		{
			width = 64 % x;
		}

		for (u8 sprite_x = 0; sprite_x < width; ++sprite_x)
		{
			if (data & (0x80 >> sprite_x))
			{
				u16 pixel = ((y + sprite_y) * 64) + (x + sprite_x);

				if (pixels[pixel])
				{
					V[0xF] = 1;
				}

				pixels[pixel] = !pixels[pixel];
			}
		}
	}

	redraw = true;
}

void CHIP8::skip_pressed()
{
	debug("SKIP PRESSED");
}

void CHIP8::skip_npressed()
{
	debug("SKIP NOT PRESSED (%X)", V[(instruction >> 8) & 0xF]);
}

void CHIP8::get_delay()
{
	V[(instruction >> 8) & 0xF] = delay_timer.load();
}

void CHIP8::wait_key()
{
	std::unique_lock<std::mutex> key_press_lock(key_press_mutex);
	key_pressed.wait(key_press_lock);
	V[(instruction >> 8) & 0xF] = last_key;
}

void CHIP8::set_delay()
{
	delay_timer.store(V[(instruction >> 8) & 0xF]);
}

void CHIP8::set_sound()
{
	sound_timer.store(V[(instruction >> 8) & 0xF]);
}

void CHIP8::add_I()
{
	I += V[(instruction >> 8) & 0xF];
}

void CHIP8::sprite()
{
	I = V[(instruction >> 8) & 0xF] * 0x5;
}

void CHIP8::store_decimal()
{
	u8 X = (instruction >> 8) & 0xF;
	memory[I] = V[X] / 100;
	memory[I + 1] = (V[X] / 10) % 10;
	memory[I + 2] = (V[X] % 100) % 10;
}

void CHIP8::store_I_VX()
{
	for (u8 i = 0; i <= ((instruction >> 8) & 0xF); ++i)
	{
		memory[I + i] = V[i];
	}
}

void CHIP8::store_VX_I()
{
	for (u8 i = 0; i <= ((instruction >> 8) & 0xF); ++i)
	{
		V[i] = memory[I + i];
	}

	// TODO: This hidden behaviour is used by some games. Implement this through the settings menu somehow
	//I += X + 1;
}