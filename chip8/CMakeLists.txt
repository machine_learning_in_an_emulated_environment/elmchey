file(GLOB CHIP8_SRC "*.h" "*.cpp")

source_group("Source" FILES ${CHIP8_SRC})

add_library(chip8 STATIC ${CHIP8_SRC})

target_include_directories(chip8 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(chip8 PRIVATE emulator_common)

common_target_options(chip8)

set_property(TARGET chip8 PROPERTY COTIRE_CXX_PREFIX_HEADER_INIT "stdafx.h")
cotire(chip8)