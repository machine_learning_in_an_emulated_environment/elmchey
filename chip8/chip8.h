#pragma once

#include "pixel_renderer/pixel_renderer.h"
#include "emulator.h"

class CHIP8 : public Emulator
{
public:
	CHIP8(Executable& executable);
	~CHIP8();
	bool initialize();
	void run();
	void run_debugger();
	void handle_message(u8& message, ByteBuffer& buffer);

protected:
	bool load_rom();
	bool reset();

	// General
	std::unique_ptr<PixelRenderer> renderer;
	bool running;
	bool emulating;

	// For emulation
	u16 instruction;
	u16 PC;
	u8 V[16];
	u16 I;
	u8 memory[4096];
	u16 stack[16];
	u8 SP;
	bool redraw;
	u16 target_ips;

	// For video output
	u32 width;
	u32 height;
	u8 scaling;
	std::vector<u8> pixels;

	// For the timers
	std::thread timer_thread;
	std::atomic<u8> delay_timer;
	std::atomic<u8> sound_timer;

	// For random number generation
	std::random_device seed;
	std::mt19937 rng;
	std::uniform_int_distribution<> gen;

	// For receiving keypad input
	void key_down(wxKeyEvent& event);
	void key_up(wxKeyEvent& event);
	std::condition_variable key_pressed;
	std::mutex key_press_mutex;
	std::atomic<u8> last_key;
	//std::atomic<bool> keypad[0xF]{};

	// Instructions
	std::vector<void(CHIP8::*)()> instructions;
	void unknown();
	void clear();
	void ret();
	void jump();
	void call();
	void skip_equal_nn();
	void skip_nequal_nn();
	void set_NN();
	void add_NN();
	void set_VX_VY();
	void add();
	void subtract();
	void shift_right();
	void shift_left();
	void skip_nequal();
	void set_I();
	void random();
	void draw();
	void skip_pressed();
	void skip_npressed();
	void get_delay();
	void wait_key();
	void set_delay();
	void set_sound();
	void add_I();
	void sprite();
	void store_decimal();
	void store_I_VX();
	void store_VX_I();
};