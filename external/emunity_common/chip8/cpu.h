#pragma once

namespace chip8
{
	namespace InstructionEnum
	{
		enum Instruction : u8
		{
			CALL_RCA,
			CLEAR,
			RETURN,
			JUMP,
			CALL,
			SKIP_EQUAL_NN,
			SKIP_NEQUAL_NN,
			SKIP_EQUAL,
			SET_NN,
			ADD_NN,
			SET_VX_VY,
			ADD,
			SUBTRACT,
			SHIFT_RIGHT,
			SHIFT_LEFT,
			SKIP_NEQUAL,
			SET_I,
			RANDOM,
			DRAW,
			SKIP_PRESSED,
			SKIP_NPRESSED,
			GET_DELAY,
			WAIT_KEY,
			SET_DELAY,
			SET_SOUND,
			ADD_I,
			SPRITE,
			STORE_DECIMAL,
			STORE_I_VX,
			STORE_VX_I,
			UNKNOWN = 0x3F,
		};
	};

	using Instruction = InstructionEnum::Instruction;

	inline Instruction decode(u16& instruction)
	{
		// Different byte values of the instruction
		u8 b1 = instruction >> 12;
		u8 b2 = (instruction >> 8) & 0xF;
		u8 b3 = (instruction >> 4) & 0xF;
		u8 b4 = instruction & 0xF;

		switch (b1)
		{
			case 0x0:
			{
				switch (b4)
				{
					case 0x0:
					{
						return Instruction::CLEAR;
					}

					case 0xE:
					{
						return Instruction::RETURN;
					}

					default:
					{
						error("Unknown 0x00E byte 4: 0x%01X", b4);
						break;
					}
				}
			}

			case 0x1:
			{
				return Instruction::JUMP;
			}

			case 0x2:
			{
				return Instruction::CALL;
			}

			case 0x3:
			{
				return Instruction::SKIP_EQUAL_NN;
			}

			case 0x4:
			{
				return Instruction::SKIP_NEQUAL_NN;
			}

			case 0x5:
			{
				return Instruction::SKIP_EQUAL;
			}

			case 0x6:
			{
				return Instruction::SET_NN;
			}

			case 0x7:
			{
				return Instruction::ADD_NN;
			}

			case 0x8:
			{
				switch (b4)
				{
					case 0x0:
					{
						return Instruction::SET_VX_VY;
					}

					case 0x4:
					{
						return Instruction::ADD;
					}

					case 0x5:
					{
						return Instruction::SUBTRACT;
					}

					case 0x6:
					{
						return Instruction::SHIFT_RIGHT;
					}

					case 0xE:
					{
						return Instruction::SHIFT_LEFT;
					}

					default:
					{
						error("Unknown 0x8** byte 4 (0x%01X)", b4);
						break;
					}
				}
			}

			case 0x9:
			{
				return Instruction::SKIP_NEQUAL;
			}

			case 0xA:
			{
				return Instruction::SET_I;
			}

			case 0xC:
			{
				return Instruction::RANDOM;
			}

			case 0xD:
			{
				return Instruction::DRAW;
			}

			case 0xE:
			{
				switch (b3)
				{
					case 0x9:
					{
						return Instruction::SKIP_PRESSED;
					}

					case 0xA:
					{
						return Instruction::SKIP_NPRESSED;
					}

					default:
					{
						error("Unknown 0xE* byte 3: 0x%01X", b3);
						break;
					}
				}
			}

			case 0xF:
			{
				switch (b3)
				{
					case 0x0:
					{
						switch (b4)
						{
							case 0x7:
							{
								return Instruction::GET_DELAY;
							}

							case 0xA:
							{
								return Instruction::WAIT_KEY;
							}

							default:
							{
								error("Unknown 0xF*0 byte 4: 0x%01X", b4);
								break;
							}
						}
					}

					case 0x1:
					{
						switch (b4)
						{
							case 0x5:
							{
								return Instruction::SET_DELAY;
							}

							case 0x8:
							{
								return Instruction::SET_SOUND;
							}

							case 0xE:
							{
								return Instruction::ADD_I;
							}

							default:
							{
								error("Unknown 0xF*1 byte 4: 0x%01X", b4);
								break;
							}
						}
					}

					case 0x2:
					{
						return Instruction::SPRITE;
					}

					case 0x3:
					{
						return Instruction::STORE_DECIMAL;
					}

					case 0x5:
					{
						return Instruction::STORE_I_VX;
					}

					case 0x6:
					{
						return Instruction::STORE_VX_I;
					}

					default:
					{
						error("Unknown 0xF* byte 3: 0x%01X", b3);
						break;
					}
				}
			}

			default:
			{
				error("Unknown byte 1: 0x%01X", b1);
			}
		}

		return Instruction::UNKNOWN;
	}

	static const std::string instruction_string(Instruction& opcode)
	{
		switch (opcode)
		{
			case Instruction::CALL_RCA:       return "CALL_RCA NNN";
			case Instruction::CLEAR:          return "CLEAR";
			case Instruction::RETURN:         return "RET";
			case Instruction::JUMP:           return "JUMP NNN";
			case Instruction::CALL:           return "CALL NNN";
			case Instruction::SKIP_EQUAL_NN:  return "SKIP X==NN";
			case Instruction::SKIP_NEQUAL_NN: return "SKIP X!=NN";
			case Instruction::SKIP_EQUAL:     return "SKIP X==Y";
			case Instruction::SET_NN:         return "SET X=NN";
			case Instruction::ADD_NN:         return "ADD X+=NN";
			case Instruction::SET_VX_VY:      return "SET X=Y";
			case Instruction::ADD:            return "ADD X+=Y";
			case Instruction::SUBTRACT:       return "SUB X-Y";
			case Instruction::SHIFT_RIGHT:    return "SHIFT X>>1";
			case Instruction::SHIFT_LEFT:     return "SHIFT X<<1";
			case Instruction::SKIP_NEQUAL:    return "SKIP X!=Y";
			case Instruction::SET_I:          return "SET I=NNN";
			case Instruction::RANDOM:         return "RANDOM NN";
			case Instruction::DRAW:           return "DRAW";
			case Instruction::SKIP_PRESSED:   return "SKIP PRESSED[VX]";
			case Instruction::SKIP_NPRESSED:  return "SKIP !PRESSED[VX]";
			case Instruction::GET_DELAY:      return "GET VX=DELAY";
			case Instruction::WAIT_KEY:       return "WAIT VX";
			case Instruction::SET_DELAY:      return "SET DELAY=VX";
			case Instruction::SET_SOUND:      return "SET SOUND=VX";
			case Instruction::ADD_I:          return "ADD I+=X";
			case Instruction::SPRITE:         return "SRITE X";
			case Instruction::STORE_DECIMAL:  return "STORE_DECIMAL VX";
			case Instruction::STORE_I_VX:     return "STORE [I], VX";
			case Instruction::STORE_VX_I:     return "STORE VX, [I]";
			case Instruction::UNKNOWN:        return "UNKNOWN";
		}

		return "Invalid Instruction! Something went very wrong.";
	}
}