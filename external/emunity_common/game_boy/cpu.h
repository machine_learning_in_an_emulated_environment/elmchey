#pragma once

namespace gb
{
	struct Registers
	{
		union
		{
			struct
			{
				struct
				{
					u8     : 4;
					bool C : 1;
					bool H : 1;
					bool N : 1;
					bool Z : 1;
				} F;

				u8 A;
			};
	
			u16 AF;
		};

		union
		{
			struct
			{
				u8 C;
				u8 B;
			};
	
			u16 BC;
		};
	
		union
		{
			struct
			{
				u8 E;
				u8 D;
			};
	
			u16 DE;
		};
	
		union
		{
			struct
			{
				u8 L;
				u8 H;
			};
	
			u16 HL;
		};

		u16 SP;
		u16 PC;
	};

	namespace InstructionEnum
	{
		enum Instruction : u8
		{
			NOP,
			LD_BC_I16,
			LD_ABC_A,
			INC_BC,
			INC_B,
			DEC_B,
			LD_B_I8,
			RLCA,
			LD_A16_SP,
			ADD_HL_BC,
			LD_A_ABC,
			DEC_BC,
			INC_C,
			DEC_C,
			LD_C_I8,
			RRCA,
			STOP,
			LD_DE_I16,
			LD_ADE_A,
			INC_DE,
			INC_D,
			DEC_D,
			LD_D_I8,
			RLA,
			JR_S8,
			ADD_HL_DE,
			LD_A_ADE,
			DEC_DE,
			INC_E,
			DEC_E,
			LD_E_I8,
			RRA,
			JR_NZ_S8,
			LD_HL_I16,
			LD_AHLI_A,
			INC_HL,
			INC_H,
			DEC_H,
			LD_H_I8,
			DAA,
			JR_Z_S8,
			ADD_HL_HL,
			LD_A_AHLI,
			DEC_HL,
			INC_L,
			DEC_L,
			LD_L_I8,
			CPL,
			JR_NC_S8,
			LD_SP_I16,
			LD_AHLD_A,
			INC_SP,
			INC_AHL,
			DEC_AHL,
			LD_AHL_I8,
			SCF,
			JR_C_S8,
			ADD_HL_SP,
			LD_A_AHLD,
			DEC_SP,
			INC_A,
			DEC_A,
			LD_A_I8,
			CCF,
			LD_B_B,
			LD_B_C,
			LD_B_D,
			LD_B_E,
			LD_B_H,
			LD_B_L,
			LD_B_AHL,
			LD_B_A,
			LD_C_B,
			LD_C_C,
			LD_C_D,
			LD_C_E,
			LD_C_H,
			LD_C_L,
			LD_C_AHL,
			LD_C_A,
			LD_D_B,
			LD_D_C,
			LD_D_D,
			LD_D_E,
			LD_D_H,
			LD_D_L,
			LD_D_AHL,
			LD_D_A,
			LD_E_B,
			LD_E_C,
			LD_E_D,
			LD_E_E,
			LD_E_H,
			LD_E_L,
			LD_E_AHL,
			LD_E_A,
			LD_H_B,
			LD_H_C,
			LD_H_D,
			LD_H_E,
			LD_H_H,
			LD_H_L,
			LD_H_AHL,
			LD_H_A,
			LD_L_B,
			LD_L_C,
			LD_L_D,
			LD_L_E,
			LD_L_H,
			LD_L_L,
			LD_L_AHL,
			LD_L_A,
			LD_AHL_B,
			LD_AHL_C,
			LD_AHL_D,
			LD_AHL_E,
			LD_AHL_H,
			LD_AHL_L,
			HALT,
			LD_AHL_A,
			LD_A_B,
			LD_A_C,
			LD_A_D,
			LD_A_E,
			LD_A_H,
			LD_A_L,
			LD_A_AHL,
			LD_A_A,
			ADD_B,
			ADD_C,
			ADD_D,
			ADD_E,
			ADD_H,
			ADD_L,
			ADD_AHL,
			ADD_A,
			ADC_B,
			ADC_C,
			ADC_D,
			ADC_E,
			ADC_H,
			ADC_L,
			ADC_AHL,
			ADC_A,
			SUB_B,
			SUB_C,
			SUB_D,
			SUB_E,
			SUB_H,
			SUB_L,
			SUB_AHL,
			SUB_A,
			SBC_B,
			SBC_C,
			SBC_D,
			SBC_E,
			SBC_H,
			SBC_L,
			SBC_AHL,
			SBC_A,
			AND_B,
			AND_C,
			AND_D,
			AND_E,
			AND_H,
			AND_L,
			AND_AHL,
			AND_A,
			XOR_B,
			XOR_C,
			XOR_D,
			XOR_E,
			XOR_H,
			XOR_L,
			XOR_AHL,
			XOR_A,
			OR_B,
			OR_C,
			OR_D,
			OR_E,
			OR_H,
			OR_L,
			OR_AHL,
			OR_A,
			CP_B,
			CP_C,
			CP_D,
			CP_E,
			CP_H,
			CP_L,
			CP_AHL,
			CP_A,
			RET_NZ,
			POP_BC,
			JP_NZ_A16,
			JP_A16,
			CALL_NZ_A16,
			PUSH_BC,
			ADD_I8,
			RST_00,
			RET_Z,
			RET,
			JP_Z_A16,
			PREFIX_CB,
			CALL_Z_A16,
			CALL_A16,
			ADC_I8,
			RST_08,
			RET_NC,
			POP_DE,
			JP_NC_A16,
			INVALID,
			CALL_NC_A16,
			PUSH_DE,
			SUB_I8,
			RST_10,
			RET_C,
			RETI,
			JP_C_A16,

			CALL_C_A16 = 0xDC,

			SBC_I8 = 0xDE,
			RST_18,
			LDH_A8_A,
			POP_HL,
			LD_AC_A,

			PUSH_HL = 0xE5,
			AND_I8,
			RST_20,
			ADD_SP_S8,
			JP_HL,
			LD_A16_A,

			XOR_I8 = 0xEE,
			RST_28,
			LDH_A_A8,
			POP_AF,
			LD_A_AC,
			DI,

			PUSH_AF = 0xF5,
			OR_I8,
			RST_30,
			LD_HL_SP_S8,
			LD_SP_HL,
			LD_A_A16,
			EI,

			CP_I8 = 0xFE,
			RST_38,
		};
	};

	using Instruction = InstructionEnum::Instruction;

	inline Instruction decode(u8& instruction)
	{
		switch (instruction)
		{
			case 0xDB:
			case 0xDD:
			case 0xE3:
			case 0xE4:
			case 0xEB:
			case 0xEC:
			case 0xED:
			case 0xF4:
			case 0xFC:
			case 0xFD:
			{
				return Instruction::INVALID;
			}

			default:
			{
				return static_cast<Instruction>(instruction);
			}
		}
	}

	namespace CBInstructionEnum
	{
		enum CBInstruction
		{
			RLC_B,
			RLC_C,
			RLC_D,
			RLC_E,
			RLC_H,
			RLC_L,
			RLC_AHL,
			RLC_A,
			RRC_B,
			RRC_C,
			RRC_D,
			RRC_E,
			RRC_H,
			RRC_L,
			RRC_AHL,
			RRC_A,
			RL_B,
			RL_C,
			RL_D,
			RL_E,
			RL_H,
			RL_L,
			RL_AHL,
			RL_A,
			RR_B,
			RR_C,
			RR_D,
			RR_E,
			RR_H,
			RR_L,
			RR_AHL,
			RR_A,
			SLA_B,
			SLA_C,
			SLA_D,
			SLA_E,
			SLA_H,
			SLA_L,
			SLA_AHL,
			SLA_A,
			SRA_B,
			SRA_C,
			SRA_D,
			SRA_E,
			SRA_H,
			SRA_L,
			SRA_AHL,
			SRA_A,
			SWAP_B,
			SWAP_C,
			SWAP_D,
			SWAP_E,
			SWAP_H,
			SWAP_L,
			SWAP_AHL,
			SWAP_A,
			SRL_B,
			SRL_C,
			SRL_D,
			SRL_E,
			SRL_H,
			SRL_L,
			SRL_AHL,
			SRL_A,
			BIT_0_B,
			BIT_0_C,
			BIT_0_D,
			BIT_0_E,
			BIT_0_H,
			BIT_0_L,
			BIT_0_AHL,
			BIT_0_A,
			BIT_1_B,
			BIT_1_C,
			BIT_1_D,
			BIT_1_E,
			BIT_1_H,
			BIT_1_L,
			BIT_1_AHL,
			BIT_1_A,
			BIT_2_B,
			BIT_2_C,
			BIT_2_D,
			BIT_2_E,
			BIT_2_H,
			BIT_2_L,
			BIT_2_AHL,
			BIT_2_A,
			BIT_3_B,
			BIT_3_C,
			BIT_3_D,
			BIT_3_E,
			BIT_3_H,
			BIT_3_L,
			BIT_3_AHL,
			BIT_3_A,
			BIT_4_B,
			BIT_4_C,
			BIT_4_D,
			BIT_4_E,
			BIT_4_H,
			BIT_4_L,
			BIT_4_AHL,
			BIT_4_A,
			BIT_5_B,
			BIT_5_C,
			BIT_5_D,
			BIT_5_E,
			BIT_5_H,
			BIT_5_L,
			BIT_5_AHL,
			BIT_5_A,
			BIT_6_B,
			BIT_6_C,
			BIT_6_D,
			BIT_6_E,
			BIT_6_H,
			BIT_6_L,
			BIT_6_AHL,
			BIT_6_A,
			BIT_7_B,
			BIT_7_C,
			BIT_7_D,
			BIT_7_E,
			BIT_7_H,
			BIT_7_L,
			BIT_7_AHL,
			BIT_7_A,
			RES_0_B,
			RES_0_C,
			RES_0_D,
			RES_0_E,
			RES_0_H,
			RES_0_L,
			RES_0_AHL,
			RES_0_A,
			RES_1_B,
			RES_1_C,
			RES_1_D,
			RES_1_E,
			RES_1_H,
			RES_1_L,
			RES_1_AHL,
			RES_1_A,
			RES_2_B,
			RES_2_C,
			RES_2_D,
			RES_2_E,
			RES_2_H,
			RES_2_L,
			RES_2_AHL,
			RES_2_A,
			RES_3_B,
			RES_3_C,
			RES_3_D,
			RES_3_E,
			RES_3_H,
			RES_3_L,
			RES_3_AHL,
			RES_3_A,
			RES_4_B,
			RES_4_C,
			RES_4_D,
			RES_4_E,
			RES_4_H,
			RES_4_L,
			RES_4_AHL,
			RES_4_A,
			RES_5_B,
			RES_5_C,
			RES_5_D,
			RES_5_E,
			RES_5_H,
			RES_5_L,
			RES_5_AHL,
			RES_5_A,
			RES_6_B,
			RES_6_C,
			RES_6_D,
			RES_6_E,
			RES_6_H,
			RES_6_L,
			RES_6_AHL,
			RES_6_A,
			RES_7_B,
			RES_7_C,
			RES_7_D,
			RES_7_E,
			RES_7_H,
			RES_7_L,
			RES_7_AHL,
			RES_7_A,
			SET_0_B,
			SET_0_C,
			SET_0_D,
			SET_0_E,
			SET_0_H,
			SET_0_L,
			SET_0_AHL,
			SET_0_A,
			SET_1_B,
			SET_1_C,
			SET_1_D,
			SET_1_E,
			SET_1_H,
			SET_1_L,
			SET_1_AHL,
			SET_1_A,
			SET_2_B,
			SET_2_C,
			SET_2_D,
			SET_2_E,
			SET_2_H,
			SET_2_L,
			SET_2_AHL,
			SET_2_A,
			SET_3_B,
			SET_3_C,
			SET_3_D,
			SET_3_E,
			SET_3_H,
			SET_3_L,
			SET_3_AHL,
			SET_3_A,
			SET_4_B,
			SET_4_C,
			SET_4_D,
			SET_4_E,
			SET_4_H,
			SET_4_L,
			SET_4_AHL,
			SET_4_A,
			SET_5_B,
			SET_5_C,
			SET_5_D,
			SET_5_E,
			SET_5_H,
			SET_5_L,
			SET_5_AHL,
			SET_5_A,
			SET_6_B,
			SET_6_C,
			SET_6_D,
			SET_6_E,
			SET_6_H,
			SET_6_L,
			SET_6_AHL,
			SET_6_A,
			SET_7_B,
			SET_7_C,
			SET_7_D,
			SET_7_E,
			SET_7_H,
			SET_7_L,
			SET_7_AHL,
			SET_7_A,
		};
	}

	using CBInstruction = CBInstructionEnum::CBInstruction;

	inline CBInstruction cb_decode(u8& cb_instruction)
	{
		return static_cast<CBInstruction>(cb_instruction);
	}

	// I/O registers
	enum IORegisters
	{
		ROM_MBC = 0x147,
		ROM_SIZE = 0x148,
		RAM_SIZE = 0x149,
		JOYPAD = 0xFF00,
		SB = 0xFF01,
		SC = 0xFF02,
		DIV = 0xFF04,
		TIMA = 0xFF05,
		TMA = 0xFF06,
		TAC = 0xFF07,
		IF = 0xFF0F,
		NR10 = 0xFF10,
		NR11 = 0xFF11,
		NR12 = 0xFF12,
		NR13 = 0xFF13,
		NR14 = 0xFF14,
		NR21 = 0xFF16,
		NR22 = 0xFF17,
		NR23 = 0xFF18,
		NR24 = 0xFF19,
		NR30 = 0xFF1A,
		NR31 = 0xFF1B,
		NR32 = 0xFF1C,
		NR33 = 0xFF1D,
		NR34 = 0xFF1E,
		NR41 = 0xFF20,
		NR42 = 0xFF21,
		NR43 = 0xFF22,
		NR44 = 0xFF23,
		NR50 = 0xFF24,
		NR51 = 0xFF25,
		NR52 = 0xFF26,
		LCDC = 0xFF40,
		STAT = 0xFF41,
		SCY = 0xFF42,
		SCX = 0xFF43,
		LY = 0xFF44,
		LYC = 0xFF45,
		DMA = 0xFF46,
		BGP = 0xFF47,
		OBP0 = 0xFF48,
		OBP1 = 0xFF49,
		WY = 0xFF4A,
		WX = 0xFF4B,
		KEY1 = 0xFF4D,
		VBK = 0xFF4F,
		DEACTIVATE_BOOT_ROM = 0xFF50,
		HDMA1 = 0xFF51,
		HDMA2 = 0xFF52,
		HDMA3 = 0xFF53,
		HDMA4 = 0xFF54,
		HDMA5 = 0xFF55,
		RP = 0xFF56,
		BCPS = 0xFF68,
		BCPD = 0xFF69,
		OCPS = 0xFF6A,
		OCPD = 0xFF6B,
		SVBK = 0xFF70,
		IE = 0xFFFF,
	};

	union INTRegister
	{
		struct
		{
			bool vblank    : 1;
			bool lcd_stat  : 1;
			bool timer     : 1;
			bool serial_io : 1;
			bool joypad    : 1;
			u8             : 3;
		};

		u8 value;
	};

	union STATRegister
	{
		struct
		{
			u8 mode              : 2;
			bool ly_match        : 1;
			bool mode_00_select  : 1;
			bool mode_01_select  : 1;
			bool mode_10_select  : 1;
			bool ly_match_select : 1;
		};

		u8 value;
	};

	union LCDCRegister
	{
		struct
		{
			bool bg_enable     : 1;
			bool obj_enable    : 1;
			bool obj_size      : 1;
			bool bg_code_area  : 1;
			bool bg_char_area  : 1;
			bool window_enable : 1;
			bool window_area   : 1;
			bool lcd_enable    : 1;
		};

		u8 value;
	};

	union P14
	{
		P14() : value(0xEF) {}

		struct
		{
			bool right    : 1;
			bool left     : 1;
			bool up       : 1;
			bool down     : 1;
			u8 output     : 2;
			u8 always_one : 2;
		};

		u8 value;
	};

	union P15
	{
		P15() : value(0xDF) {}

		struct
		{
			bool a        : 1;
			bool b        : 1;
			bool select   : 1;
			bool start    : 1;
			u8 output     : 2;
			u8 always_one : 2;
		};

		u8 value;
	};

	union TACRegister
	{
		struct
		{
			u8 clock      : 2;
			bool enable   : 1;
			u8 always_one : 5;
		};

		u8 value;
	};

	union SpriteFlags
	{
		struct
		{
			u8 unused     : 4;
			bool palette  : 1;
			bool x_flip   : 1;
			bool y_flip   : 1;
			bool priority : 1;
		};

		u8 value;
	};

	union Palette
	{
		struct
		{
			u8 dot_00 : 2;
			u8 dot_01 : 2;
			u8 dot_10 : 2;
			u8 dot_11 : 2;
		};

		u8 operator[](u8 dot_value)
		{
			switch (dot_value)
			{
				case 0b00:
				{
					return dot_00;
				}

				case 0b01:
				{
					return dot_01;
				}

				case 0b10:
				{
					return dot_10;
				}

				case 0b11:
				{
					return dot_11;
				}
			}

			std::abort();
		}

		u8 value;
	};
}