#pragma once

namespace gb
{
	namespace dbg
	{
		enum Error : u8
		{
			INVALID_COMMAND,
			INVALID_EVENT,
			OUT_OF_BOUNDS_READ,
			INVALID_BUTTON,
		};

		// Commands to the debbugger by the debuggee.
		enum Command : u8
		{
			RUN = 2,
			PAUSE,
			SUBSCRIBE,
			READ_MEMORY,
			WRITE_MEMORY,
			SEND_SCREEN_BUFFER,
			BUTTON,
		};

		// Messages sent by the debuggee to the debugger.
		enum Message : u8
		{
			EVENT = 2,
			MEMORY,
			SCREEN_BUFFER,
		};

		enum Event : u8
		{
			VBLANK,
		};

		struct SubscribeCommand
		{
			Event event;
			bool subscribe;
		};

		struct ReadMemoryCommand
		{
			u16 address;
			u16 size;
		};

		struct WriteMemoryCommand
		{
			u16 address;
			u8 value;
		};

		struct EventMessage
		{
			Event event;
		};

		struct MemoryMessage
		{
			u16 size;
			// The rest contains <size> bytes of memory for the last issued memory read request.
		};
	}
}