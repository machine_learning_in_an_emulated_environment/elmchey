#pragma once

enum class System : u8
{
	UNKNOWN = 0,
	CHIP8,
	GAME_BOY,
};

static const char* system_to_string(System& system)
{
	switch (system)
	{
		case System::UNKNOWN:  return "Unknown";
		case System::CHIP8:    return "CHIP8";
		case System::GAME_BOY: return "Game Boy";
	}

	return "Unknown System value!";
}