#pragma once

namespace dbg
{
	// The current protocol version. Must be incremented, when the protocol changes.
	constexpr u16 protocol_version = 1;

	enum General
	{
		SYSTEM_STARTUP = 0,
		ERROR_MESSAGE  = 1,
	};
}