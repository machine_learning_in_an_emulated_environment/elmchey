#include "stdafx_common.h"
#include "networking.h"

#include "misc/byte_buffer.h"

#ifdef WINDOWS
	#include <WinSock2.h>
	#include <mstcpip.h>
#endif

#include <string_view>

bool net::initialize()
{
#ifdef WINDOWS
	WSADATA wsa_data;

	if (int result = WSAStartup(MAKEWORD(2, 2), &wsa_data); result != 0)
	{
		error("Failed to initialize WSA. (%d)", result);
		return false;
	}
#endif

	return true;
}

bool net::cleanup()
{
#ifdef WINDOWS
	if (WSACleanup() != 0)
	{
		error("Failed to cleanup WSA. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::parse_ip_address(ip_address& address, address_string address_string)
{
	if (address_string.empty())
	{
		return false;
	}

	u64 closing_bracket = std::string::npos;

	// Windows isn't able to parse the ports or square brackets. So we validate them separately, if they exist.
	if (address_string[0] == L'[')
	{
		// Make sure the closing bracket exists
		closing_bracket = address_string.find_first_of(L']');

		if (closing_bracket == std::string::npos)
		{
			return false;
		}

		// Check if there is anything after the closing bracket, ie. a colon for a port.
		if ((address_string.length() - 1) > closing_bracket)
		{
			// Make sure the next character's actually a colon
			u64 port_colon = closing_bracket + 1;

			if (address_string[port_colon] != L':')
			{
				return false;
			}

			// Make sure the port number is not over 5 characters
			std::wstring_view port_string(&address_string[port_colon + 1], address_string.size() - (port_colon + 1));

			if (port_string.length() > 5 || port_string.length() == 0)
			{
				return false;
			}

			// Parse the port number as 32-bit, as it's input as a maximum of 5 characters, so it may overflow.
			u32 port;

			try
			{
				port = std::stoi(port_string.data());
			}
			catch (std::invalid_argument&)
			{
				return false;
			}

			// Make sure the port is not too big
			if (port == 0 || port > 65535)
			{
				return false;
			}

#ifdef WINDOWS
			address.sin6_port = htons(static_cast<u16>(port));
#endif
		}
	}

#ifdef WINDOWS
	// Zero out things from the address structure, which we don't write ourselves and don't expect to be filled by InetPton().
	address.sin6_flowinfo = 0;
	address.sin6_scope_id = 0;

	s32 result;

	if (closing_bracket != std::string::npos)
	{
		// If there address includes a port, we avoid the cost of creating a new string by creating a string view of the actual address part.
		std::wstring_view actual_address = std::wstring_view(&address_string[1], closing_bracket);

		// Set the closing bracket to a NUL
		address_string[closing_bracket] = '\0';

		result = InetPtonW(AF_INET6, actual_address.data(), &address.sin6_addr);
	}
	else
	{
		result = InetPtonW(AF_INET6, address_string.c_str(), &address.sin6_addr);
	}

	if (result != 1)
	{
		return false;
	}

	address.sin6_family = AF_INET6;
#endif

	return true;
}

bool net::is_valid_ip_address(address_string& address_string)
{
	ip_address address;
	return parse_ip_address(address, address_string);
}

bool net::socket::create(family family, type type, protocol protocol)
{
#ifdef WINDOWS
	handle = WSASocketW(static_cast<s32>(family), static_cast<s32>(type), static_cast<s32>(protocol), nullptr, 0, WSA_FLAG_OVERLAPPED);

	if (handle == INVALID_SOCKET)
	{
		error("Failed to create a socket. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::close()
{
#ifdef WINDOWS
	if (closesocket(handle) != 0)
	{
		error("Failed to close a socket. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::connect(ip_address& address)
{
#ifdef WINDOWS
	SOCKET_ADDRESS_LIST address_list;
	address_list.iAddressCount = 1;
	address_list.Address[0].iSockaddrLength = sizeof(ip_address);
	address_list.Address[0].lpSockaddr = reinterpret_cast<sockaddr*>(&address);

	if (!WSAConnectByList(handle, &address_list, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr))
	{
		error("Failed to connect a socket. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::bind(ip_address& address)
{
#ifdef WINDOWS
	if (::bind(handle, reinterpret_cast<sockaddr*>(&address), sizeof(ip_address)) != 0)
	{
		error("Failed to bind a socket. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::listen(s32 backlog)
{
#ifdef WINDOWS
	if (::listen(handle, backlog) != 0)
	{
		error("Failed to listen on a socket. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::accept(socket& accepted_socket)
{
#ifdef WINDOWS
	SOCKET new_socket;

	if (new_socket = ::accept(handle, nullptr, nullptr); new_socket == INVALID_SOCKET)
	{
		error("Failed to accept a connection on a socket. (%d)", WSAGetLastError());
		return false;
	}
	else
	{
		accepted_socket = new_socket;
	}
#endif

	return true;
}

void net::socket::enable_loopback_optimization()
{
#ifdef WINDOWS
	s32 option_value = 1;
	DWORD bytes_returned;

	if (s32 result = WSAIoctl(handle, SIO_LOOPBACK_FAST_PATH, &option_value, sizeof(option_value), nullptr, 0, &bytes_returned, nullptr, nullptr); result != 0)
	{
		// We ignore errors besides logging them, as failing to enable the loopback optimization isn't an important problem.
		error("Failed to enable loopback optimization. (%d)", WSAGetLastError());
	}
#endif
}

bool net::socket::send(ByteBuffer& buffer)
{
#ifdef WINDOWS
	if (::send(handle, reinterpret_cast<char*>(buffer.data()), static_cast<s32>(buffer.size()), 0) != buffer.size())
	{
		error("Failed to send data. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::receive(ByteBuffer& buffer, u16 bytes)
{
#ifdef WINDOWS
	buffer.resize(bytes);

	if (::recv(handle, reinterpret_cast<char*>(buffer.data()), bytes, 0) != bytes)
	{
		error("Failed to receive data. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}

bool net::socket::receive_available(ByteBuffer& buffer)
{
#ifdef WINDOWS
	u_long bytes_available;

	if (ioctlsocket(handle, FIONREAD, &bytes_available) != 0)
	{
		error("Failed to determine how many bytes are available for receiving. (%d)", WSAGetLastError());
		return false;
	}

	buffer.resize(bytes_available);

	if (::recv(handle, reinterpret_cast<char*>(buffer.data()), bytes_available, 0) != bytes_available)
	{
		error("Failed to receive available data. (%d)", WSAGetLastError());
		return false;
	}
#endif

	return true;
}