#pragma once

#ifdef WINDOWS
	// For the definitions of various structures/handles (ie. SOCKET, sockaddr_in6)
	#include <ws2tcpip.h>
#endif

class ByteBuffer;

namespace net
{
	// The platform-specific string container type for the IP address string.
#ifdef WINDOWS
	using address_string = std::wstring;
#else
	using address_string = std::string;
#endif

	// The platform-specific address structure
#ifdef WINDOWS
	using ip_address = sockaddr_in6;
#endif

	// The maximum length of a valid IPv6 address.
	// Longest known: [0000:0000:0000:0000:0000:ffff:123.123.123.123]:32768
	constexpr u8 maximum_address_length = 53;

	// Initializes networking. Subsequent calls to any other networking functions fail if this is not called beforehand.
	bool initialize();

	// Cleans up networking. Fails if initialize() wasn't called before.
	bool cleanup();

	// Parses a IPv6 address.
	bool parse_ip_address(ip_address& address, address_string address_string);

	// Returns true for a valid IPv6 address.
	bool is_valid_ip_address(address_string& address_string);

	class socket
	{
	public:
		// Socket families
		enum class family : s32
		{
			IP6 = AF_INET6,
		};

		// Socket types
		enum class type : s32
		{
			STREAM = SOCK_STREAM,
			DATAGRAM = SOCK_DGRAM,
			RAW = SOCK_RAW,
		};

		// Socket protocols
		enum class protocol : s32
		{
			TCP = IPPROTO_TCP,
			UDP = IPPROTO_UDP,
		};

		// Creates a socket.
		bool create(family family, type type, protocol protocol);

		// Closes a socket.
		bool close();

		// Connects the socket.
		bool connect(ip_address& address);

		// Binds the socket.
		bool bind(ip_address& address);

		// Starts listening for connection attempts on the socket.
		bool listen(s32 backlog);

		// Accept a connection on the specified socket. If successful, accepted socket handle is stored in accepted_socket.
		bool accept(socket& accepted_socket);

		// Enables TCP loopback optimization on the given socket. Has an effect only on Windows.
		// Both sides of the connection need to enable this on their respective sockets.
		// Must be enabled on the client socket before attempting to connect.
		// Must be enabled on the server socket before binding.
		void enable_loopback_optimization();

		// Sends the given byte buffer.
		bool send(ByteBuffer& buffer);

		// Receives the specified amount of bytes into the byte buffer.
		bool receive(ByteBuffer& buffer, u16 bytes);

		// Receives all available data from the socket in a byte buffer.
		bool receive_available(ByteBuffer& buffer);

		inline operator SOCKET&()
		{
			return handle;
		}

		socket& operator=(SOCKET& socket)
		{
			this->handle = socket;
			return *this;
		}

	private:
		SOCKET handle;
	};
}