#pragma once

// Formats the given string.
std::string format(const std::string format, ...);

// Obtains a command-line option. Returns an empty string if not found.
std::string get_option(s32 argc, char** begin, const std::string& option);

// Checks if a command-line switch exists
bool check_switch(s32 argc, char** begin, const std::string& option);

// Sets the thread's name.
void set_thread_name(std::thread& thread, std::string name);