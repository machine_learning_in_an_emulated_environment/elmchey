#pragma once

#include "stdafx_common.h"

class Timer
{
public:
	Timer() : stopped(true)
	{
	}

	inline void start()
	{
		stopped = false;
		beginning = std::chrono::high_resolution_clock::now();
	}

	inline void stop()
	{
		stopped = true;
		end = std::chrono::high_resolution_clock::now();
	}

	inline std::chrono::seconds elapsed_time_seconds()
	{
		return std::chrono::duration_cast<std::chrono::seconds>(elapsed_time_milliseconds());
	}

	inline std::chrono::milliseconds elapsed_time_milliseconds()
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_time_microseconds());
	}

	inline std::chrono::microseconds elapsed_time_microseconds()
	{
		return std::chrono::duration_cast<std::chrono::microseconds>(elapsed_time_nanoseconds());
	}

	inline std::chrono::nanoseconds elapsed_time_nanoseconds()
	{
		std::chrono::high_resolution_clock::time_point now = stopped ? end : std::chrono::high_resolution_clock::now();
		return std::chrono::duration_cast<std::chrono::nanoseconds>(now - beginning);
	}

private:
	std::chrono::high_resolution_clock::time_point beginning;
	std::chrono::high_resolution_clock::time_point end;
	bool stopped;
};