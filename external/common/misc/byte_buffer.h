#pragma once

#include <vector>

class ByteBuffer : public std::vector<u8>
{
public:
	inline void push8(u8 byte)
	{
		emplace_back(byte);
	}

	inline void push16(u16 value)
	{
		push8(value & 0xFF);
		push8(value >> 8);
	}

	inline void push32(u32 value)
	{
		push16(value & 0xFFFF);
		push16(value >> 16);
	}

	inline void push64(u64 value)
	{
		push32(value & 0xFFFFFFFF);
		push32(value >> 32);
	}

	// 1. Pushes 2 bytes indicating the length of the string.
	// 2. Pushes all the string bytes.
	// 3. Pushes a null-terminator.
	inline void push_string(std::string string)
	{
		push16(static_cast<u16>(string.length()));

		// Push the executable name
		for (char& character : string)
		{
			push8(character);
		}

		// Add a null-terminator
		push8(0);
	}

	inline u8& get8()
	{
		return operator[](pos++);
	}

	inline u16& get16()
	{
		u16& value = *reinterpret_cast<u16*>(data() + pos);
		consume(sizeof(u16));
		return value;
	}

	inline u32& get32()
	{
		u32& value = *reinterpret_cast<u32*>(data() + pos);
		consume(sizeof(u32));
		return value;
	}

	inline u64& get64()
	{
		u64& value = *reinterpret_cast<u64*>(data() + pos);
		consume(sizeof(u64));
		return value;
	}

	// Returns a reference to a structure in the buffer.
	// Use the return type resolver (RTR) idiom to make template argument deduction work.
	class get_structure
	{
	public:
		get_structure(ByteBuffer& buffer) : buffer(buffer) {}

		template<typename S>
		operator S&()
		{
			S& structure = *reinterpret_cast<S*>(buffer.data() + buffer.pos);
			buffer.consume(sizeof(S));
			return structure;
		}

	private:
		ByteBuffer& buffer;
	};

	inline bool data_available()
	{
		return pos != size();
	}

	template<typename T>
	inline void consume(T amount)
	{
		pos += amount;
	}

	std::size_t pos = 0;
};