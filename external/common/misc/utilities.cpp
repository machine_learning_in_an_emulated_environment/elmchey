#include "stdafx_common.h"
#include "utilities.h"

#ifdef WINDOWS
	#include <windows.h>
#endif

std::string format(const std::string format, ...)
{
	va_list args;
	va_start(args, format);

	std::vector<char> buffer(vsnprintf(nullptr, 0, format.c_str(), args) + 1);
	vsnprintf(buffer.data(), buffer.size(), format.c_str(), args);

	va_end(args);
	return buffer.data();
}

std::string get_option(s32 argc, char** begin, const std::string& option)
{
	char** itr = std::find(begin, begin + argc, option);

	if (itr != (begin + argc) && ++itr != (begin + argc))
	{
		return *itr;
	}

	return "";
}

bool check_switch(s32 argc, char** begin, const std::string& option)
{
	return std::find(begin, begin + argc, option) != (begin + argc);
}

void set_thread_name(std::thread& thread, std::string name)
{
#ifdef DEBUG
	#ifdef WINDOWS
		// First we need to convert to UTF16.
		s32 characters_needed = MultiByteToWideChar(CP_UTF8, 0, name.data(), static_cast<s32>(name.size()), nullptr, 0);

		if (characters_needed == 0)
		{
			error("Failed to determine the amount of characters needed for UTF8 to UTF16 conversion.");
			return;
		}

		std::vector<wchar_t> buffer(characters_needed);
		s32 characters_converted = MultiByteToWideChar(CP_UTF8, 0, name.data(), static_cast<s32>(name.size()), buffer.data(), static_cast<s32>(buffer.size()));

		if (characters_converted == 0)
		{
			error("Failed to convert UTF8 thread name to UTF16.");
			return;
		}

		// Add a null-terminator
		buffer.push_back(0);

		HRESULT result = SetThreadDescription(thread.native_handle(), buffer.data());

		if (result < 0)
		{
			error("Failed to set thread name.");
			return;
		}
	#else
		#warning Thread naming missing
	#endif
#endif
}