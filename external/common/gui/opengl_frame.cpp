#include "stdafx_common.h"
#include "opengl_frame.h"

#ifdef DEBUG
void GLAPIENTRY OGLFrame::debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	switch (type)
	{
		case GL_DEBUG_TYPE_ERROR:
		{
			error("%s", message);
			break;
		}

		default:
		{
			// GL_DEBUG_TYPE_OTHER on Nvidia has useless debug messages. Oh I just bound a buffer and you allocated fast memory for it? I don't care, it's fast!
			if (type != GL_DEBUG_TYPE_OTHER)
			{
				warning("%s (0x%X)", message, type);
			}
		}
	}
}
#endif

OGLFrame::OGLFrame(std::string& title, u32 width, u32 height, wxGLAttributes& attributes, wxGLContextAttrs* context_attributes) : RendererFrame(title, width, height)
{
#ifdef DEBUG
	context_attributes->DebugCtx();
#endif

	canvas = std::make_unique<wxGLCanvas>(static_cast<wxFrame*>(this), attributes, wxID_ANY, wxDefaultPosition, wxSize(width, height));
	context = std::make_unique<wxGLContext>(canvas.get(), nullptr, context_attributes);

	end_constructor();
}

bool OGLFrame::initialize(void* options_in, void* out)
{
	RendererFrame::initialize(options_in, out);

	// Set the OpenGL context we created in the constructor as the canvas's current context
	if (!canvas->SetCurrent(*context.get()))
	{
		error("Failed to set the OpenGL canvas context.");
		return false;
	}

	// Initialize glew so we can use OpenGL functions
	GLenum ret = glewInit();

	if (ret != GLEW_OK)
	{
		error("Failed to initialize GLEW: %s", glewGetErrorString(ret));
		return false;
	}

#ifdef DEBUG
	// Set the driver debug output callback
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(static_cast<GLDEBUGPROC>(debug_callback), nullptr);
#endif

	return true;
}

bool OGLFrame::flip()
{
	// We need to call the base flip method, as it measures the FPS.
	RendererFrame::flip();

	return canvas->SwapBuffers();
}