#include "stdafx_common.h"
#include "vulkan_frame.h"

VulkanFrame::VulkanFrame(std::string& title, u32 width, u32 height) : RendererFrame(title, width, height)
{
	// Create a panel, which we'll use as a rendering surface
	surface_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(static_cast<s32>(width), static_cast<s32>(height)), wxWANTS_CHARS);

	end_constructor();
}

bool VulkanFrame::initialize(void* options_in, void* out)
{
	RendererFrame::initialize(options_in, out);

	// The Vulkan options
	VulkanOptions& options = *static_cast<VulkanOptions*>(options_in);

	// Allocate the VulkanHandles structure and set the output pointer it
	*static_cast<VulkanHandles**>(out) = new VulkanHandles;
	VulkanHandles& handles = **static_cast<VulkanHandles**>(out);

	// The general instance extensions
	std::vector<const char*> instance_extensions =
	{
		VK_KHR_SURFACE_EXTENSION_NAME,
	};

#ifdef WINDOWS
	instance_extensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#elif defined(LINUX)
	instance_extensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#endif

#ifdef DEBUG
	instance_extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif

	std::vector<const char*>& validation_layers = handles.validation_layers;

	// Enable validation layers, if enabled
#ifdef DEBUG
	if (options.enable_validation)
	{
		validation_layers.push_back("VK_LAYER_LUNARG_standard_validation");
		validation_layers.push_back("VK_LAYER_GOOGLE_unique_objects");
	}
#endif

	// Create an instance
	VkApplicationInfo application_info;
	application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	application_info.pNext = nullptr;
	application_info.pApplicationName = "Elmchey";
	application_info.applicationVersion = 1; // TODO: Use the actual Elmchey version
	application_info.pEngineName = options.engine;
	application_info.engineVersion = options.engine_version;
	application_info.apiVersion = VK_MAKE_VERSION(1, 0, VK_HEADER_VERSION);

	VkInstanceCreateInfo instance_info;
	instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_info.pNext = nullptr;
	instance_info.flags = 0;
	instance_info.pApplicationInfo = &application_info;
	instance_info.enabledLayerCount = static_cast<u32>(validation_layers.size());
	instance_info.ppEnabledLayerNames = validation_layers.data();
	instance_info.enabledExtensionCount = static_cast<u32>(instance_extensions.size());
	instance_info.ppEnabledExtensionNames = instance_extensions.data();

	VkResult result;

	if ((result = vkCreateInstance(&instance_info, nullptr, &handles.instance)) != VK_SUCCESS)
	{
#ifdef DEBUG
		if (result == VK_ERROR_LAYER_NOT_PRESENT)
		{
			error("Vulkan SDK missing or its installations is corrupted. (VK_ERROR_LAYER_NOT_PRESENT)");
			return false;
		}
#endif

		error("Failed to create Vulkan instance. (%d)", result);
		return false;
	}

	// Set up the debug callback
#ifdef DEBUG
	handles.create_debug_report = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(handles.instance, "vkCreateDebugReportCallbackEXT"));
	handles.destroy_debug_report = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(handles.instance, "vkDestroyDebugReportCallbackEXT"));

	if (!handles.create_debug_report || !handles.destroy_debug_report)
	{
		error("Failed to obtain debug callback function addresses. (%d)", result);
		return false;
	}

	VkDebugReportFlagsEXT flags = {};
	flags |= VK_DEBUG_REPORT_ERROR_BIT_EXT;
	flags |= VK_DEBUG_REPORT_WARNING_BIT_EXT;
	flags |= VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;

	VkDebugReportCallbackCreateInfoEXT debug_callback_info;
	debug_callback_info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
	debug_callback_info.pNext = nullptr;
	debug_callback_info.flags = flags;
	debug_callback_info.pfnCallback = reinterpret_cast<PFN_vkDebugReportCallbackEXT>(handles.debug_callback);
	debug_callback_info.pUserData = nullptr;

	if ((result = handles.create_debug_report(handles.instance, &debug_callback_info, nullptr, &handles.debug_callback_function)) != VK_SUCCESS)
	{
		error("Failed to create debug report callback. (%d)", result);
		return false;
	}
#endif

	// Create a surface appropriate for the platform
#ifdef WINDOWS
	VkWin32SurfaceCreateInfoKHR surface_info;
	surface_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surface_info.pNext = nullptr;
	surface_info.flags = 0;
	surface_info.hinstance = nullptr;
	surface_info.hwnd = surface_panel->GetHandle();

	if ((result = vkCreateWin32SurfaceKHR(handles.instance, &surface_info, nullptr, &handles.surface)) != VK_SUCCESS)
	{
		error("Failed to create window surface. (%d)", result);
		return false;
	}
#elif defined(LINUX)
	debug("handle: 0x%p", surface_panel->GetHandle());

	VkXcbSurfaceCreateInfoKHR surface_info;
	surface_info.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	surface_info.pNext = nullptr;
	surface_info.flags = 0;

	return false;
#endif

	// Find an appropriate device
	u32 device_count;

	if ((result = vkEnumeratePhysicalDevices(handles.instance, &device_count, nullptr)) != VK_SUCCESS)
	{
		error("Failed to obtain the number of devices. (%d)", result);
		return false;
	}

	// TODO: This should probably try to use some other device instead and reset the device to default.
	if (device_count < options.device_number)
	{
		error("Requested graphics adapter doesn't exist. (%d/%d)", device_count, options.device_number);
		return false;
	}

	std::vector<VkPhysicalDevice> adapters(device_count);

	if ((result = vkEnumeratePhysicalDevices(handles.instance, &device_count, adapters.data())) != VK_SUCCESS)
	{
		error("Failed to obtain list of physical devices. (%d)", result);
		return false;
	}

	handles.physical_device = adapters[options.device_number];

	return true;
}