#pragma once

#include "misc/timer.h"

class RendererFrame : protected wxFrame
{
public:
	explicit RendererFrame(std::string& title, u32& width, u32& height);

	// Initializes the frame so it can be renderer to.
	// options_in is a pointer to a structure of arguments for the rendering frame.
	// out is a pointer to a structure of outputs.
	// Note that both options_in and out structures are specific to the rendering frame you're initializing.
	// If these aren't required by the rendering surface type being initialized, then pass nullptr.
	virtual bool initialize(void* options_in, void* out);

	// Call this function to flip a new frame.
	virtual bool flip();

	// Destroys the window.
	bool destroy();

	// Gets the client size of the window.
	void get_size(u32* width, u32* height);

	// Sets the width and height to which clamp the resizing.
	// The window is only resized when the resized width and height are both multiples of the passed values.
	// The size values are rounded down to the nearest equal multiple. This can be changed by allowing uneven clamping.
	void set_resize_clamp(u32& width, u32& height, bool force_even_clamping = true);

	// Sets whether the window can be resized or not.
	void set_resizable(bool resizable = true);

	// Binds the function to receive an event when the key is pressed.
	// You will receive multiple events while the key is held down.
	// This function must be called from the GUI thread.
	// handler must be a pointer to the class instance of the function.
	template<class Class>
	inline void bind_key_down(void(Class::*key_down_callback)(wxKeyEvent& event), Class* handler)
	{
		if (sub_object)
		{
			// HACK:
			// When any child object (for example wxGLCanvas or wxPanel) exists, it can steal the focus and thus the events, so the frame never receives them.
			// To solve this we also bind the event handler to the sub_object to ensure we receive the events.
			// Note that we can't make this virtual, because the functions needs to be templated for member class functions.
			sub_object->Bind(wxEVT_KEY_DOWN, key_down_callback, handler);
		}
		
		// We still bind to the frame itself, even if we bind to the the sub_object, because the sub_object may be out-of-focus in certain cases.
		Bind(wxEVT_KEY_DOWN, key_down_callback, handler);
	}

	// Binds the function to receive an event when a key is released.
	// This function must be called from the GUI thread.
	// handler must be a pointer to the class instance of the function.
	template<class Class>
	inline void bind_key_up(void(Class::*key_up_callback)(wxKeyEvent& event), Class* handler)
	{
		if (sub_object)
		{
			// HACK:
			// Same hack as described in bind_key_down().
			sub_object->Bind(wxEVT_KEY_UP, key_up_callback, handler);
		}
		
		Bind(wxEVT_KEY_UP, key_up_callback, handler);
	}

	// Bind the function to receive an event when the window is closed.
	// This function must be called from the GUI thread.
	// handler must be a pointer to the class instance of the function.
	template<class Class>
	inline void bind_close(void(Class::*close_callback)(wxCloseEvent& event), Class* handler)
	{
		Bind(wxEVT_CLOSE_WINDOW, close_callback, handler);
	}

protected:
	// This should be called by sub-classes at the end of the constructor. It sets the sub_object allowing keybinding and various other operations.
	void end_constructor();

private:
	// These are used for the fps counter.
	std::string title;
	Timer fps_timer;
	u64 frame_count;

	// Things needed for resize clamping.
	bool force_even_clamping;
	u32 width_clamp;
	u32 height_clamp;

	void sizing(wxSizeEvent& event);

	// The timer is used to track when the resizing ends, so the clamp that's set during resizing is released, allowing the window to be resized again.
	std::unique_ptr<wxTimer> resize_timer;

	void resize_timer_timeout(wxTimerEvent& event);

	// This is a pointer to an object that fills the client area. It may be a nullptr.
	wxWindow* sub_object;
};

// For use with unique_ptrs
struct RendererFrameDestructor
{
	void operator()(RendererFrame* frame)
	{
		frame->destroy();
	}
};