#pragma once

#include "renderer_frame.h"
#include "vulkan/vulkan_helper.h"

struct VulkanOptions
{
	bool enable_validation; // Whether to enable validation layers for debugging or not
	u8 device_number;       // Device number to use
	const char* engine;     // The current engine name, ie. "PixelRenderer", "Xbox"
	u32 engine_version;     // The current engine version.
};

class VulkanFrame : public RendererFrame
{
public:
	VulkanFrame(std::string& title, u32 width, u32 height);

	// Initializes basic Vulkan functionality.
	// options_in should be a pointer to a VulkanOptions structure.
	// out should be a pointer pointer of a VulkanHandles structure.
	bool initialize(void* options_in, void* out);

private:
	void resized(wxSizeEvent& event);

	// For creating an appropriately size surface
	wxPanel* surface_panel;
};