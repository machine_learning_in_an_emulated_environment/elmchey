#include "stdafx_common.h"
#include "renderer_frame.h"

RendererFrame::RendererFrame(std::string& title, u32& width, u32& height) : wxFrame(nullptr, wxID_ANY, title), frame_count(0), title(title), width_clamp(0),
                                                                            height_clamp(0), force_even_clamping(false)
{
	// Resize the actual surface area to be the intended width and height
	SetClientSize(wxSize(static_cast<s32>(width), static_cast<s32>(height)));
	Centre();
	Show();

	// Disable maximizing
	EnableMaximizeButton(false);

	// Event binding
	Bind(wxEVT_SIZING, &RendererFrame::sizing, this);

	// Create a timer that we use to detect when resizing has ended and bind it to our timeout method
	resize_timer = std::make_unique<wxTimer>();
	resize_timer->Bind(wxEVT_TIMER, &RendererFrame::resize_timer_timeout, this);
}

void RendererFrame::end_constructor()
{
	// Obtain and set the sub_object, that we use for a couple workarounds
	if (!GetChildren().IsEmpty())
	{
		sub_object = static_cast<wxWindow*>(GetChildren().GetFirst()->GetData());
	}
	else
	{
		sub_object = nullptr;
	}
}

bool RendererFrame::initialize(void* options_in, void* out)
{
	// Make sure to start the FPS timer so we can begin counting the FPS.
	fps_timer.start();

	return true;
}

bool RendererFrame::flip()
{
	++frame_count;

	// We update the FPS counter every 0.5 seconds to keep it decently responsive
	if (fps_timer.elapsed_time_seconds() >= 0.5s)
	{
		std::string fps_title = format("FPS: %.2f | %s", frame_count / static_cast<double>(fps_timer.elapsed_time_seconds().count()), title.c_str());
		SetTitle(fps_title);

		// Reset the frame count and (re)start the timer to begin counting again.
		frame_count = 0;
		fps_timer.start();
	}

	return true;
}

void RendererFrame::get_size(u32* width, u32* height)
{
	// If the sub_object exists, we use that to obtain the client size, as on some implementations the window's minimum client size is restricted.
	// sub_object's client size is always the size of the intended rendering surface, so that fixes the problem.
	wxWindow* client = sub_object ? sub_object : this;

	client->GetClientSize(reinterpret_cast<s32*>(width), reinterpret_cast<s32*>(height));
}

void RendererFrame::set_resize_clamp(u32& width, u32& height, bool force_even_clamping)
{
	width_clamp = width;
	height_clamp = height;
	this->force_even_clamping = force_even_clamping;
}

void RendererFrame::set_resizable(bool resizable)
{
	// Obtain the current style, so we can modify it.
	long style = GetWindowStyle();

	// Change the resizable style bit depending on the resizable flag.
	style ^= (static_cast<long>(resizable) ^ style) & wxRESIZE_BORDER;

	SetWindowStyle(style);
}

void RendererFrame::resize_timer_timeout(wxTimerEvent& event)
{
	// Check if resizing has stopped (left mouse button up)
	if (!wxGetMouseState().LeftIsDown())
	{
		// Unrestrict the client size, so resizing events can be received once again.
		wxSize unclamped_size(-1, -1);
		SetMaxClientSize(unclamped_size);
		SetMinClientSize(unclamped_size);

		// Stop the timer, as the timer only will be needed once the resizing starts again.
		resize_timer->Stop();
	}
}

void RendererFrame::sizing(wxSizeEvent& event)
{
	// Make sure that this event is passed onto other components, so they can react to being resized.
	event.Skip();

	// Check if the clamping is set, otherwise we won't handle the resizing logic.
	if (!width_clamp && !height_clamp)
	{
		return;
	}

	// Start the timer to ensure that the resizing completion check gets executed.
	resize_timer->Start(100);

	// Rounds the number down to the nearest multiple.
	auto round_down_to_nearest_multiple = [](auto number, auto multiple)
	{
		return number - (number % multiple);
	};

	u32 clamped_width = round_down_to_nearest_multiple(static_cast<u32>(event.GetSize().GetWidth()), width_clamp);
	u32 clamped_height = round_down_to_nearest_multiple(static_cast<u32>(event.GetSize().GetHeight()), height_clamp);
	u32 width_clamp_amount = clamped_width / width_clamp;
	u32 height_clamp_amount = clamped_height / height_clamp;
	
	// This restricts the clamp to the original clamp aspect ratio.
	if (width_clamp_amount != height_clamp_amount && force_even_clamping)
	{
		u32 smallest_clamp_amount = std::min(width_clamp_amount, height_clamp_amount);
		clamped_width = smallest_clamp_amount * width_clamp;
		clamped_height = smallest_clamp_amount * height_clamp;
	}

	// Unrestrict the size so the size can be set to the clamped size.
	// Otherwise the maximum client size may be smaller than the minimum one or the minimum client size may be bigger than the maximum one causing a crash.
	// Note that we need to only unrestrict the max client size, as we set the min client size first and thus it eliminates the above problem.
	wxSize unclamped_size(-1, -1);
	SetMaxClientSize(unclamped_size);

	// Actually set the current clamped client size.
	wxSize clamped_size(clamped_width, clamped_height);
	SetMinClientSize(clamped_size);
	SetMaxClientSize(clamped_size);

	// If a sub_object exists, make sure to set the client size of the sub_object, as in some cases the sizing event is not handled by the sub_object
	if (sub_object)
	{
		sub_object->SetClientSize(clamped_size);
	}
}

bool RendererFrame::destroy()
{
	return Destroy();
}