#pragma once

#include "renderer_frame.h"
#include "wx/glcanvas.h"

class OGLFrame : public RendererFrame
{
public:
	OGLFrame(std::string& title, u32 width, u32 height, wxGLAttributes& attributes, wxGLContextAttrs* context_attributes);
	bool initialize(void* options_in, void* out);
	bool flip();

private:
	std::unique_ptr<wxGLContext> context;
	std::unique_ptr<wxGLCanvas> canvas;

#ifdef DEBUG
	static void GLAPIENTRY debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
#endif
};