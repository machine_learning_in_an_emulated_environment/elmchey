#include "stdafx_common.h"
#include "opengl_helper.h"

void shader_source(GLuint shader, const GLchar* shader_source)
{
	GLint shader_length = static_cast<GLint>(strlen(shader_source));
	glShaderSource(shader, 1, &shader_source, &shader_length);
}

bool compile_shader(GLuint shader)
{
	glCompileShader(shader);

	GLint result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

	if (result == GL_FALSE)
	{
#ifdef DEBUG
		GLint log_length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log_message(log_length);
		glGetShaderInfoLog(shader, log_length, &log_length, &log_message[0]);

		error("compile_shader: Failed to compile the shader:");
		log("%s\n", log_message.data());
#endif

		return false;
	}

	return true;
}

bool create_shader(GLuint* shader, GLenum shader_type, const GLchar* shader_source)
{
	*shader = glCreateShader(shader_type);
	::shader_source(*shader, shader_source);
	return compile_shader(*shader);
}

void attach_shaders(GLuint shader_program, std::vector<GLuint> shaders)
{
	for (GLuint& shader : shaders)
	{
		glAttachShader(shader_program, shader);
	}
}

bool link_shader_program(GLuint shader_program)
{
	glLinkProgram(shader_program);

	GLint result = 0;
	glGetProgramiv(shader_program, GL_LINK_STATUS, &result);

	if (result == GL_FALSE)
	{
#ifdef DEBUG
		GLint log_length = 0;
		glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log_message(log_length);
		glGetProgramInfoLog(shader_program, log_length, &log_length, &log_message[0]);

		error("link_shader_program: Failed to link the shader program:");
		log("%s\n", log_message.data());
#endif

		return false;
	}

	return true;
}

bool create_shader_program(GLuint* shader_program, std::vector<GLuint> shaders)
{
	*shader_program = glCreateProgram();
	attach_shaders(*shader_program, shaders);
	return link_shader_program(*shader_program);
}

void object_name(GLenum identifier, GLuint object, std::string name)
{
#ifdef DEBUG
	glObjectLabel(identifier, object, static_cast<GLsizei>(name.length()), name.c_str());
#endif
}