#pragma once

// Sets the shader source for the given shader program.
void shader_source(GLuint shader, const GLchar* shader_source);

// Compiles the shader.
bool compile_shader(GLuint shader);

// Creates the shader, sets the shader source and compiles the shader.
bool create_shader(GLuint* shader, GLenum shader_type, const GLchar* shader_source);

// Attaches shaders to the shader program.
void attach_shaders(GLuint shader_program, std::vector<GLuint> shaders);

// Links the shader program.
bool link_shader_program(GLuint shader_program);

// Creates the shader program, adds the shaders to it and links it.
bool create_shader_program(GLuint* shader_program, std::vector<GLuint> shaders);

// Gives the given object a name. (debug only)
void object_name(GLenum identifier, GLuint object, std::string name);