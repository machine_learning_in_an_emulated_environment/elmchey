file(GLOB COMMON_SRC "*.h")
file(GLOB LOGGER_SRC "logger/*.h" "logger/*.cpp")
file(GLOB MISC_SRC "misc/*.h" "misc/*.cpp")
file(GLOB GUI_SRC "gui/*.h" "gui/*.cpp")
file(GLOB VULKAN_SRC "vulkan/*.h" "vulkan/*.cpp")
file(GLOB OPENGL_SRC "opengl/*.h" "opengl/*.cpp")
file(GLOB NETWORKING_SRC "networking/*.h" "networking/*.cpp")

source_group("Source" FILES ${COMMON_SRC})
source_group("Source\\Logger" FILES ${LOGGER_SRC})
source_group("Source\\Misc" FILES ${MISC_SRC})
source_group("Source\\GUI" FILES ${GUI_SRC})
source_group("Source\\Vulkan" FILES ${VULKAN_SRC})
source_group("Source\\OpenGL" FILES ${OPENGL_SRC})
source_group("Source\\Networking" FILES ${NETWORKING_SRC})

add_library(common STATIC
	${COMMON_SRC}
	${LOGGER_SRC}
	${MISC_SRC}
)

# Find and setup 3rdparty libraries
if(WITH_wxWidgets)
	if(WITH_OpenGL)
		find_package(wxWidgets COMPONENTS core base adv gl REQUIRED)
	else()
		find_package(wxWidgets COMPONENTS core base adv REQUIRED)
	endif()

	target_compile_definitions(common PUBLIC "wxWidgets")
	target_include_directories(common PUBLIC ${wxWidgets_INCLUDE_DIRS})
	target_link_libraries(common PUBLIC ${wxWidgets_LIBRARIES})
	target_sources(common PRIVATE ${GUI_SRC})

	list(APPEND COMMON_COMPILE_DEFINITIONS "${wxWidgets_DEFINITIONS}")
	list(APPEND COMMON_COMPILE_DEFINITIONS "$<$<CONFIG:Debug>:${wxWidgets_DEFINITIONS_DEBUG}>")
	list(APPEND COMMON_COMPILE_DEFINITIONS "$<$<NOT:$<CONFIG:Debug>>:wxDEBUG_LEVEL=0>")
endif()

if(WITH_Vulkan)
	find_package(Vulkan REQUIRED)

	target_compile_definitions(common PUBLIC "WITH_VULKAN")
	target_sources(common PRIVATE ${VULKAN_SRC})
	target_link_libraries(common PUBLIC Vulkan::Vulkan)
endif()

if(WITH_OpenGL)
	find_package(GLEW REQUIRED)

	target_compile_definitions(common PUBLIC "WITH_OPENGL")
	target_sources(common PRIVATE ${OPENGL_SRC})
	target_link_libraries(common PUBLIC GLEW::GLEW)
endif()

if(WITH_networking)
	target_compile_definitions(common PUBLIC "WITH_NETWORKING")
	target_sources(common PRIVATE ${NETWORKING_SRC})

	if(MSVC)
		target_link_libraries(common PUBLIC "Ws2_32")
	endif()
endif()

target_include_directories(common PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

# Apply common options that are needed for every target separately.
common_target_options(common)

## Common compiler definitions and options
## These will propagate to all projects that link with Common.
# Define DEBUG for the debugging configuration
list(APPEND COMMON_COMPILE_DEFINITIONS "$<$<CONFIG:Debug>:DEBUG>")

if(MSVC)
	# Ensures that we build as Unicode
	list(APPEND COMMON_COMPILE_DEFINITIONS "UNICODE")
	list(APPEND COMMON_COMPILE_DEFINITIONS "_UNICODE")

	# Force UTF-8 for source files
	list(APPEND COMMON_COMPILE_OPTIONS "/utf-8")

	# Disable some annoying warnings that recommend stuff that only works on MSVC
	list(APPEND COMMON_COMPILE_DEFINITIONS "_CRT_SECURE_NO_WARNINGS")

	# Enable full C++ standard conformance
	list(APPEND COMMON_COMPILE_OPTIONS "/permissive-")

	# Enable parallel compiling
	list(APPEND COMMON_COMPILE_OPTIONS "/MP")

	# Disable RTTI, since we don't need it
	list(APPEND COMMON_COMPILE_OPTIONS "/GR-")

	## Options for release
	# Enable highest possible optimization.
	# For why we don't use /Ox read this: https://www.reddit.com/r/cpp/comments/5mgyf2/passing_functions_to_functions/dd9svic/
	list(APPEND COMMON_COMPILE_OPTIONS "$<$<NOT:$<OR:$<CONFIG:Debug>,$<CONFIG:MinSizeRel>>>:/O2>")

	# Enable function-level linking
	list(APPEND COMMON_COMPILE_OPTIONS "$<$<NOT:$<CONFIG:Debug>>:/Gy>")

	# Enable Whole Program Optimization for release builds.
	list(APPEND COMMON_COMPILE_OPTIONS "$<$<NOT:$<CONFIG:Debug>>:/GL>")
elseif(UNIX)
	# On Unix CMake's own C++ standard mechanism doesn't pass "-std=c++1(1/4/7)" to the compiler sometimes. 
	# This causes issues with standard libraries that need a flag to enable C++11/14/17 features, so we need to explicitly pass it to the compiler.
	# TODO: It might no longer be needed with the latest versions. Needs to be rechecked.
	#list(APPEND COMMON_COMPILE_OPTIONS "-std=c++17")
endif()

target_compile_options(common PUBLIC ${COMMON_COMPILE_OPTIONS})
target_compile_definitions(common PUBLIC ${COMMON_COMPILE_DEFINITIONS})

set_property(TARGET common PROPERTY COTIRE_CXX_PREFIX_HEADER_INIT "stdafx_common.h")
cotire(common)