#pragma once

// Creates a file with log_name in the current working directory that all log messages will be written to.
bool initialize_logging(std::string log_name);

// Logs a message. This function is thread-safe. This function cannot be used during global initialization.
void log(bool error, const std::string format, va_list args);

// Short-hands for logging.
inline void log(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	log(false, format, args);
	va_end(args);
}

inline void info(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	log(false, "I: " + format + "\n", args);
	va_end(args);
}

inline void debug(const std::string format, ...)
{
#ifdef DEBUG
	va_list args;
	va_start(args, format);
	log(false, "D: " + format + "\n", args);
	va_end(args);
#endif
}

inline void todo(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	log(false, "TODO: " + format + "\n", args);
	va_end(args);
}

inline void error(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	log(true, "E: " + format + "\n", args);
	va_end(args);
}

inline void warning(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	log(false, "W: " + format + "\n", args);
	va_end(args);
}