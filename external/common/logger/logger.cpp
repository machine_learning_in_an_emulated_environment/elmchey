#include "stdafx_common.h"
#include "logger.h"

// The instance of the log output file
std::ofstream log_file;

// Used to make sure that printing to the console and writing to the file are thread-safe, don't interleave randomly and aren't printed in the wrong order
std::mutex log_mutex;

bool initialize_logging(std::string log_name)
{
	// Ensure that the log file writes will not be buffered
	log_file.rdbuf()->pubsetbuf(nullptr, 0);

	// Open the log file and delete all the contents in it
	log_file.open(log_name, std::ios::trunc);

	if (!log_file)
	{
		error("Failed to open log file.");
		return false;
	}

	// Note that we don't need to sync with standard C++ streams, as all logging should be done through this logging library.
	// This may improve performance on some systems.
	log_file.sync_with_stdio(false);

	return true;
}

void log(bool error, const std::string format, va_list args)
{
	// Format the output
	std::vector<char> buffer(vsnprintf(nullptr, 0, format.c_str(), args) + 1);
	vsnprintf(buffer.data(), buffer.size(), format.c_str(), args);

	// We need to lock the mutex here, as writes using std::ofstream aren't thread-safe and we want to ensure that the message is printed to console and log at the same point in time
	std::lock_guard<std::mutex> lock(log_mutex);

	fprintf(error ? stderr : stdout, "%s", buffer.data());
	log_file << buffer.data();
}