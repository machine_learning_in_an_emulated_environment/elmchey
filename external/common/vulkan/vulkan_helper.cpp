#include "stdafx_common.h"
#include "vulkan_helper.h"

bool get_family_index(VkPhysicalDevice physical_device, VkSurfaceKHR surface, u32* family_index)
{
	// Get the amount of family queues
	u32 family_queue_count;
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &family_queue_count, nullptr);

	// Get list of family queues
	std::vector<VkQueueFamilyProperties> queues(family_queue_count);
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &family_queue_count, queues.data());

	VkResult result;
	VkBool32 supported;

	for (u32 i = 0; i < queues.size(); ++i)
	{
		if (!(queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
		{
			continue;
		}

		if ((result = vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, surface, &supported)) != VK_SUCCESS)
		{
			error("get_family_index: Failed to query surface support. (%d)", result);
			return false;
		}

		if (supported)
		{
			*family_index = i;
			return true;
		}
	}

	return false;
}

bool get_memory_type(VkPhysicalDeviceMemoryProperties device_memory_properties, u32 type_bits, VkFlags properties, u32* memory_type)
{
	for (u32 i = 0; i < 32; ++i)
	{
		if (type_bits & 1)
		{
			if ((device_memory_properties.memoryTypes[i].propertyFlags & properties) == properties)
			{
				*memory_type = i;
				return true;
			}
		}

		type_bits >>= 1;
	}

	return false;
}

bool create_swapchain(VkDevice device, VkSwapchainKHR& swapchain, VkSwapchainCreateInfoKHR& swapchain_info, VkImage* swapchain_images)
{
	VkResult result;

	// Wait for the device to become idle
	if ((result = vkDeviceWaitIdle(device)) != VK_SUCCESS)
	{
		error("create_swapchain: Failed to wait for the device to become idle. (%d)", result);
		return false;
	}

	// Create a new swapchain
	if ((result = vkCreateSwapchainKHR(device, &swapchain_info, nullptr, &swapchain)) != VK_SUCCESS)
	{
		error("create_swapchain: Failed to create a new swapchain. (%d)", result);
		return false;
	}

	// Obtain the number of usable swapchain images
	u32 usable_image_count;

	if ((result = vkGetSwapchainImagesKHR(device, swapchain, &usable_image_count, nullptr)) != VK_SUCCESS)
	{
		error("create_swapchain: Faild to obtain the number of usable swapchain images. (%d)", result);
		return false;
	}

	// Obtain the new swapchain images
	if ((result = vkGetSwapchainImagesKHR(device, swapchain, &swapchain_info.minImageCount, swapchain_images)) != VK_SUCCESS)
	{
		error("create_swapchain: Failed to obtain new swapchain images. (%d)", result);
		return false;
	}

	return true;
}

void pipeline_barrier(VkCommandBuffer command_buffer, VkPipelineStageFlags src_stage, VkPipelineStageFlags dst_stage, std::vector<VkMemoryBarrier> memory_barriers, std::vector<VkBufferMemoryBarrier> buffer_barriers,
                      std::vector<VkImageMemoryBarrier> image_barriers)
{
	vkCmdPipelineBarrier(command_buffer, src_stage, dst_stage, 0, static_cast<u32>(memory_barriers.size()), memory_barriers.data(), static_cast<u32>(buffer_barriers.size()), buffer_barriers.data(),
	                     static_cast<u32>(image_barriers.size()), image_barriers.data());
}