#pragma once

// Gets the index of the first graphics family that supports graphics output and surfaces.
bool get_family_index(VkPhysicalDevice physical_device, VkSurfaceKHR surface, u32* family_index);

// Gets the first memory type that supports requested properties.
bool get_memory_type(VkPhysicalDeviceMemoryProperties device_memory_properties, u32 type_bits, VkFlags properties, u32* memory_type);

// Creates a new swapchain and obtains new swapchain images.
bool create_swapchain(VkDevice device, VkSwapchainKHR& swapchain, VkSwapchainCreateInfoKHR& swapchain_info, VkImage* swapchain_images);

// Creates a pipeline barrier.
void pipeline_barrier(VkCommandBuffer command_buffer, VkPipelineStageFlags src_stage, VkPipelineStageFlags dst_stage, std::vector<VkMemoryBarrier> memory_barriers, std::vector<VkBufferMemoryBarrier> buffer_barriers,
                      std::vector<VkImageMemoryBarrier> image_barriers);

// Structure for holding basic Vulkan handles. Upon the destruction of this object, all associated handles will be destroyed.
struct VulkanHandles
{
	VkInstance instance;
	VkPhysicalDevice physical_device;
	VkSurfaceKHR surface;

	// A vector that contains all the validation layers that we want to enable. This is used for instance creation, but also required for device creation, so it needs to be accessible to whenever renderer creates a device.
	std::vector<const char*> validation_layers;

	// For debug output
#ifdef DEBUG
	PFN_vkCreateDebugReportCallbackEXT create_debug_report;
	PFN_vkDestroyDebugReportCallbackEXT destroy_debug_report;
	VkDebugReportCallbackEXT debug_callback_function;

	static VkBool32 debug_callback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT type, u64 src, u64 location, s32 msg_code, char* prefix, char* msg, void* user_data)
	{
		if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
		{
			error("[%s]: %s", prefix, msg);
		}
		else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT || flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
		{
			warning("[%s]: %s", prefix, msg);
		}

		return false;
	}
#endif

	// Destroys some basic Vulkan handles (instance, physical device, surface)
	~VulkanHandles()
	{
		if (surface)
		{
			vkDestroySurfaceKHR(instance, surface, nullptr);
		}

#ifdef DEBUG
		if (destroy_debug_report)
		{
			destroy_debug_report(instance, debug_callback_function, nullptr);
		}
#endif

		if (instance)
		{
			vkDestroyInstance(instance, nullptr);
		}
	}
};