#pragma once

// Check compiler support
#if defined(_MSC_VER) && _MSC_VER < 1911
	#error Unsupported MSVC version. Please upgrade to MSVC14.11 or newer.
#elif defined(__clang__) && __clang_major__ < 4 && __clang_minor__ < 0
	#error Unsupported Clang version. Please upgrade to Clang 4.0 or newer.
#elif defined(__GNUC__) && __GNUC__ < 7
	#error Unsupported GCC version. Please upgrade to GCC7 or newer.
#elif !defined(_MSC_VER) && !defined(__clang__) && !defined(__GNUC__)
	#error Unknown/unsupported compiler.
#endif

// STL headers
#include <algorithm>
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <memory>
#include <random>
#include <stack>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

// Makes the use of the filesystem standard library much easier
namespace fs = std::experimental::filesystem;

// So 10ms, 1s, etc can be easily used
using namespace std::chrono_literals;

// Fixed-size types
using s64 = std::int64_t;
using s32 = std::int32_t;
using s16 = std::int16_t;
using s8  = std::int8_t;
using u64 = std::uint64_t;
using u32 = std::uint32_t;
using u16 = std::uint16_t;
using u8  = std::uint8_t;

// Some defines to identify the operating system and architecture that we're compiling for
#if defined(__amd64__) || defined(_M_AMD64)
	#define X64
#else
	#error Unsupported architecture
#endif

#if defined(_WIN64)
	#define WINDOWS
#elif defined(__linux__)
	#define LINUX
#elif defined(__ANDROID__)
	#define ANDROID
#else
	#error Unsupported platform
#endif

// Disable warnings for 3rdparty libraries
#pragma warning(push, 0)
	// Only include wxWidgets if on a platform where we use it
	#if defined(wxWidgets)
		// Some defines to prevent Windows headers included by wxWidgets being too messy
		#if defined(WINDOWS)
			// Windows headers mess with std::min and std::max. This prevents them from doing so.
			#define NOMINMAX

			// Prevent Windows headers included from including unnecessary crap.
			#define WIN32_LEAN_AND_MEAN
		#endif

		// We need to define this to enable the pre-compiled header
		#define WX_PRECOMP

		// We disable both exceptions and RTTI
		#define wxNO_EXCEPTIONS
		#define wxNO_RTTI

		#include "wx/wxprec.h"
	#endif

	#if defined(WITH_OPENGL)
		// We link GLEW statically
		#define GLEW_STATIC

		#include "GL/glew.h"
	#endif

	#if defined(WITH_VULKAN)
		// Enable the appropriate Vulkan surface extension
		#if defined(WINDOWS)
			#define VK_USE_PLATFORM_WIN32_KHR
		#elif defined(LINUX)
			#define VK_USE_PLATFORM_XCB_KHR
		#endif

		#include "vulkan/vulkan.h"
	#endif
#pragma warning(pop)

// These are used by almost everything, so might as well already include them here
#include "logger/logger.h"
#include "misc/utilities.h"