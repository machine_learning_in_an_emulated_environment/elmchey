#include <algorithm>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

#include "yaml-cpp/yaml.h"

// Shorthands
namespace fs = std::experimental::filesystem;

using s64 = std::int64_t;
using s32 = std::int32_t;
using s16 = std::int16_t;
using s8  = std::int8_t;
using u64 = std::uint64_t;
using u32 = std::uint32_t;
using u16 = std::uint16_t;
using u8  = std::uint8_t;

// Gets the command-line argument
std::string get_option(s32 argc, char** begin, const std::string& option)
{
	char** itr = std::find(begin, begin + argc, option);

	if (itr != (begin + argc) && ++itr != (begin + argc))
	{
		return *itr;
	}

	return "";
}

enum ResourceType : u8
{
	BINARY,
	TEXT,
};

// Structure for keeping info about a resource
struct Resource
{
	std::string name;
	fs::path path;
	ResourceType type;
};

// Decoding thingy for the Resource structure
namespace YAML
{
	template<>
	struct convert<Resource>
	{
		static bool decode(const Node& node, Resource& resource)
		{
			resource.name = node["name"].as<std::string>();
			resource.path = fs::path(node["path"].as<std::string>());

			std::string type = node["type"].as<std::string>();

			if (type == "binary")
			{
				resource.type = ResourceType::BINARY;
			}
			else if (type == "text")
			{
				resource.type = ResourceType::TEXT;
			}
			else
			{
				std::printf("Unknown resource type. (%s)\n", type.c_str());
				return false;
			}

			return true;
		}
	};
}

s32 main(s32 argc, char** argv)
{
	// Parse command-line arguments
	fs::path output_directory(get_option(argc, argv, "-o"));
	fs::path config_file(get_option(argc, argv, "-i"));

	if (output_directory.empty())
	{
		std::printf("No output directory given, outputting to working directory.\n");
	}

	if (config_file.empty())
	{
		std::printf("No config file path given, using default.\n");
		config_file.replace_filename("resources.yaml");
	}
	else if (!config_file.has_filename())
	{
		config_file.replace_filename("resources.yaml");
	}

	// Make sure that the config file actually exists.
	if (!fs::exists(config_file))
	{
		std::printf("The config file doesn't exist. (%s)\n", config_file.u8string().c_str());
		return EXIT_FAILURE;
	}

	std::ifstream resource_config(config_file);
	YAML::Node root = YAML::Load(resource_config);

	// Parse resource nodes
	std::vector<Resource> resources;

	for (YAML::Node node : root["resources"])
	{
		// TODO: This check returns that the sequences are maps. It shouldn't do that. Maybe a yaml-cpp bug?
		/*if (!node.IsSequence())
		{
			std::printf("ERROR: Resource nodes must be sequences. (%d)\n", node.Type());
			continue;
		}*/

		if (!node["name"] || !node["path"] || !node["type"])
		{
			std::printf("ERROR: One of the following mandatory properties is missing: name/path/type\n");
			continue;
		}

		Resource resource = node.as<Resource>();

		if (!fs::exists(resource.path))
		{
			std::printf("Resource %s doesn't exist at %s\n", resource.name.c_str(), resource.path.u8string().c_str());
			continue;
		}

		resources.push_back(resource);
	}

	// Array of hex characters
	char hex_characters[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	std::ofstream output(output_directory.append("resources.h"));

#if _WIN32
	// Windows fucking sucks at detecting UTF8 encoded files, so we need to add the UTF8 BOM.
	output << "\xEF\xBB\xBF";
#endif

	output << "#pragma once" << std::endl;
	output << std::endl;
	output << "namespace resources" << std::endl;
	output << "{";

	for (Resource& resource : resources)
	{
		std::ifstream file(resource.path, std::ios::binary);
		u64 size = fs::file_size(resource.path);

		output << std::endl;

		std::vector<u8> bytes(size);
		file.read(reinterpret_cast<char*>(bytes.data()), size);

		if (resource.type == ResourceType::BINARY)
		{
			output << "\tu8 " << resource.name.c_str() << "[] =" << std::endl;
			output << "\t{";

			u16 current_byte = 16;

			for (auto& byte : bytes)
			{
				if (current_byte == 16)
				{
					current_byte = 0;

					output << std::endl;
					output << "\t\t";
				}

				output << "0x" << hex_characters[(byte & 0xF0) >> 4] << hex_characters[byte & 0xF] << ", ";

				++current_byte;
			}

			output << std::endl;
			output << "\t};" << std::endl;
			output << "\tu64 " << resource.name.c_str() << "_size = " << size << ";" << std::endl;
		}
		else if (resource.type == ResourceType::TEXT)
		{
			std::string byte_string(reinterpret_cast<char*>(bytes.data()), size);
			output << "\tstd::string " << resource.name.c_str() << " =" << std::endl;
			output << "u8R\"res(";
			output << byte_string.c_str();
			output << ")res\";" << std::endl;
		}
	}
	
	output << "};";
}