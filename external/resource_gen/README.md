# ResourceGenerator
ResourceGenerator allows you to embed resource files automatically in an executable for C++.

The following command-line options can be specified:  
-i \<path>: The path for the resource configuration file. Incase of a path to a directory, configuration file's name is assumed to be "resources.yaml". Default path is the working directory and the default file name is "resources.yaml".  
-o \<path>: The path for the output directory. Default output directory is the working directory. 

See [configuration](#configuration) for the resource file format.

## Configuration
In the root there must be a map named "resources".  
All resources must be specified in a sequence alongside with their properties as children of the above-mentioned map.

Properties:

* name (string, required)
* path (path, required)
* type (required)
  * binary
  * text

Example:
```yaml
resources:
    - name: "cool_logo"
      path: "cool_logo.png"
      type: binary
    - name: "important_licence"
      path: "important_licence.txt"
      type: text
```