#include "stdafx.h"
#include "game_boy.h"
#include "game_boy/debug.h"

GameBoy::GameBoy(Executable& executable) : Emulator(executable)
{
	instructions.fill(&GameBoy::invalid);
	instructions[gb::Instruction::NOP] = &GameBoy::nop;
	instructions[gb::Instruction::LD_BC_I16] = &GameBoy::ld_bc_i16;
	instructions[gb::Instruction::LD_ABC_A] = &GameBoy::ld_abc_a;
	instructions[gb::Instruction::INC_BC] = &GameBoy::inc_bc;
	instructions[gb::Instruction::INC_B] = &GameBoy::inc_b;
	instructions[gb::Instruction::DEC_B] = &GameBoy::dec_b;
	instructions[gb::Instruction::LD_B_I8] = &GameBoy::ld_b_i8;
	instructions[gb::Instruction::RLCA] = &GameBoy::rlca;
	instructions[gb::Instruction::LD_A16_SP] = &GameBoy::ld_a16_sp;
	instructions[gb::Instruction::ADD_HL_BC] = &GameBoy::add_hl_bc;
	instructions[gb::Instruction::LD_A_ABC] = &GameBoy::ld_a_abc;
	instructions[gb::Instruction::DEC_BC] = &GameBoy::dec_bc;
	instructions[gb::Instruction::INC_C] = &GameBoy::inc_c;
	instructions[gb::Instruction::DEC_C] = &GameBoy::dec_c;
	instructions[gb::Instruction::LD_C_I8] = &GameBoy::ld_c_i8;
	instructions[gb::Instruction::RRCA] = &GameBoy::rrca;
	instructions[gb::Instruction::LD_DE_I16] = &GameBoy::ld_de_i16;
	instructions[gb::Instruction::LD_ADE_A] = &GameBoy::ld_ade_a;
	instructions[gb::Instruction::INC_DE] = &GameBoy::inc_de;
	instructions[gb::Instruction::INC_D] = &GameBoy::inc_d;
	instructions[gb::Instruction::DEC_D] = &GameBoy::dec_d;
	instructions[gb::Instruction::LD_D_I8] = &GameBoy::ld_d_i8;
	instructions[gb::Instruction::RLA] = &GameBoy::rla;
	instructions[gb::Instruction::JR_S8] = &GameBoy::jr_s8;
	instructions[gb::Instruction::ADD_HL_DE] = &GameBoy::add_hl_de;
	instructions[gb::Instruction::LD_A_ADE] = &GameBoy::ld_a_ade;
	instructions[gb::Instruction::DEC_DE] = &GameBoy::dec_de;
	instructions[gb::Instruction::INC_E] = &GameBoy::inc_e;
	instructions[gb::Instruction::DEC_E] = &GameBoy::dec_e;
	instructions[gb::Instruction::LD_E_I8] = &GameBoy::ld_e_i8;
	instructions[gb::Instruction::RRA] = &GameBoy::rra;
	instructions[gb::Instruction::JR_NZ_S8] = &GameBoy::jr_nz_s8;
	instructions[gb::Instruction::LD_HL_I16] = &GameBoy::ld_hl_i16;
	instructions[gb::Instruction::LD_AHLI_A] = &GameBoy::ld_ahli_a;
	instructions[gb::Instruction::INC_HL] = &GameBoy::inc_hl;
	instructions[gb::Instruction::INC_H] = &GameBoy::inc_h;
	instructions[gb::Instruction::DEC_H] = &GameBoy::dec_h;
	instructions[gb::Instruction::LD_H_I8] = &GameBoy::ld_h_i8;
	instructions[gb::Instruction::DAA] = &GameBoy::daa;
	instructions[gb::Instruction::JR_Z_S8] = &GameBoy::jr_z_s8;
	instructions[gb::Instruction::ADD_HL_HL] = &GameBoy::add_hl_hl;
	instructions[gb::Instruction::LD_A_AHLI] = &GameBoy::ld_a_ahli;
	instructions[gb::Instruction::DEC_HL] = &GameBoy::dec_hl;
	instructions[gb::Instruction::INC_L] = &GameBoy::inc_l;
	instructions[gb::Instruction::DEC_L] = &GameBoy::dec_l;
	instructions[gb::Instruction::LD_L_I8] = &GameBoy::ld_l_i8;
	instructions[gb::Instruction::CPL] = &GameBoy::cpl;
	instructions[gb::Instruction::JR_NC_S8] = &GameBoy::jr_nc_s8;
	instructions[gb::Instruction::LD_SP_I16] = &GameBoy::ld_sp_i16;
	instructions[gb::Instruction::LD_AHLD_A] = &GameBoy::ld_ahld_a;
	instructions[gb::Instruction::INC_SP] = &GameBoy::inc_sp;
	instructions[gb::Instruction::INC_AHL] = &GameBoy::inc_ahl;
	instructions[gb::Instruction::DEC_AHL] = &GameBoy::dec_ahl;
	instructions[gb::Instruction::LD_AHL_I8] = &GameBoy::ld_ahl_i8;
	instructions[gb::Instruction::SCF] = &GameBoy::scf;
	instructions[gb::Instruction::JR_C_S8] = &GameBoy::jr_c_s8;
	instructions[gb::Instruction::ADD_HL_SP] = &GameBoy::add_hl_sp;
	instructions[gb::Instruction::LD_A_AHLD] = &GameBoy::ld_a_ahld;
	instructions[gb::Instruction::DEC_SP] = &GameBoy::dec_sp;
	instructions[gb::Instruction::INC_A] = &GameBoy::inc_a;
	instructions[gb::Instruction::DEC_A] = &GameBoy::dec_a;
	instructions[gb::Instruction::LD_A_I8] = &GameBoy::ld_a_i8;
	instructions[gb::Instruction::CCF] = &GameBoy::ccf;
	instructions[gb::Instruction::LD_B_B] = &GameBoy::ld_b_b;
	instructions[gb::Instruction::LD_B_C] = &GameBoy::ld_b_c;
	instructions[gb::Instruction::LD_B_D] = &GameBoy::ld_b_d;
	instructions[gb::Instruction::LD_B_E] = &GameBoy::ld_b_e;
	instructions[gb::Instruction::LD_B_H] = &GameBoy::ld_b_h;
	instructions[gb::Instruction::LD_B_L] = &GameBoy::ld_b_l;
	instructions[gb::Instruction::LD_B_AHL] = &GameBoy::ld_b_ahl;
	instructions[gb::Instruction::LD_B_A] = &GameBoy::ld_b_a;
	instructions[gb::Instruction::LD_C_B] = &GameBoy::ld_c_b;
	instructions[gb::Instruction::LD_C_C] = &GameBoy::ld_c_c;
	instructions[gb::Instruction::LD_C_D] = &GameBoy::ld_c_d;
	instructions[gb::Instruction::LD_C_E] = &GameBoy::ld_c_e;
	instructions[gb::Instruction::LD_C_H] = &GameBoy::ld_c_h;
	instructions[gb::Instruction::LD_C_L] = &GameBoy::ld_c_l;
	instructions[gb::Instruction::LD_C_AHL] = &GameBoy::ld_c_ahl;
	instructions[gb::Instruction::LD_C_A] = &GameBoy::ld_c_a;
	instructions[gb::Instruction::LD_D_B] = &GameBoy::ld_d_b;
	instructions[gb::Instruction::LD_D_C] = &GameBoy::ld_d_c;
	instructions[gb::Instruction::LD_D_D] = &GameBoy::ld_d_d;
	instructions[gb::Instruction::LD_D_E] = &GameBoy::ld_d_e;
	instructions[gb::Instruction::LD_D_H] = &GameBoy::ld_d_h;
	instructions[gb::Instruction::LD_D_L] = &GameBoy::ld_d_l;
	instructions[gb::Instruction::LD_D_AHL] = &GameBoy::ld_d_ahl;
	instructions[gb::Instruction::LD_D_A] = &GameBoy::ld_d_a;
	instructions[gb::Instruction::LD_E_B] = &GameBoy::ld_e_b;
	instructions[gb::Instruction::LD_E_C] = &GameBoy::ld_e_c;
	instructions[gb::Instruction::LD_E_D] = &GameBoy::ld_e_d;
	instructions[gb::Instruction::LD_E_E] = &GameBoy::ld_e_e;
	instructions[gb::Instruction::LD_E_H] = &GameBoy::ld_e_h;
	instructions[gb::Instruction::LD_E_L] = &GameBoy::ld_e_l;
	instructions[gb::Instruction::LD_E_AHL] = &GameBoy::ld_e_ahl;
	instructions[gb::Instruction::LD_E_A] = &GameBoy::ld_e_a;
	instructions[gb::Instruction::LD_H_B] = &GameBoy::ld_h_b;
	instructions[gb::Instruction::LD_H_C] = &GameBoy::ld_h_c;
	instructions[gb::Instruction::LD_H_D] = &GameBoy::ld_h_d;
	instructions[gb::Instruction::LD_H_E] = &GameBoy::ld_h_e;
	instructions[gb::Instruction::LD_H_H] = &GameBoy::ld_h_h;
	instructions[gb::Instruction::LD_H_L] = &GameBoy::ld_h_l;
	instructions[gb::Instruction::LD_H_AHL] = &GameBoy::ld_h_ahl;
	instructions[gb::Instruction::LD_H_A] = &GameBoy::ld_h_a;
	instructions[gb::Instruction::LD_L_B] = &GameBoy::ld_l_b;
	instructions[gb::Instruction::LD_L_C] = &GameBoy::ld_l_c;
	instructions[gb::Instruction::LD_L_D] = &GameBoy::ld_l_d;
	instructions[gb::Instruction::LD_L_E] = &GameBoy::ld_l_e;
	instructions[gb::Instruction::LD_L_H] = &GameBoy::ld_l_h;
	instructions[gb::Instruction::LD_L_L] = &GameBoy::ld_l_l;
	instructions[gb::Instruction::LD_L_AHL] = &GameBoy::ld_l_ahl;
	instructions[gb::Instruction::LD_L_A] = &GameBoy::ld_l_a;
	instructions[gb::Instruction::LD_AHL_B] = &GameBoy::ld_ahl_b;
	instructions[gb::Instruction::LD_AHL_C] = &GameBoy::ld_ahl_c;
	instructions[gb::Instruction::LD_AHL_D] = &GameBoy::ld_ahl_d;
	instructions[gb::Instruction::LD_AHL_E] = &GameBoy::ld_ahl_e;
	instructions[gb::Instruction::LD_AHL_H] = &GameBoy::ld_ahl_h;
	instructions[gb::Instruction::LD_AHL_L] = &GameBoy::ld_ahl_l;
	instructions[gb::Instruction::HALT] = &GameBoy::halt;
	instructions[gb::Instruction::LD_AHL_A] = &GameBoy::ld_ahl_a;
	instructions[gb::Instruction::LD_A_B] = &GameBoy::ld_a_b;
	instructions[gb::Instruction::LD_A_C] = &GameBoy::ld_a_c;
	instructions[gb::Instruction::LD_A_D] = &GameBoy::ld_a_d;
	instructions[gb::Instruction::LD_A_E] = &GameBoy::ld_a_e;
	instructions[gb::Instruction::LD_A_H] = &GameBoy::ld_a_h;
	instructions[gb::Instruction::LD_A_L] = &GameBoy::ld_a_l;
	instructions[gb::Instruction::LD_A_AHL] = &GameBoy::ld_a_ahl;
	instructions[gb::Instruction::LD_A_A] = &GameBoy::ld_a_a;
	instructions[gb::Instruction::ADD_B] = &GameBoy::add_b;
	instructions[gb::Instruction::ADD_C] = &GameBoy::add_c;
	instructions[gb::Instruction::ADD_D] = &GameBoy::add_d;
	instructions[gb::Instruction::ADD_E] = &GameBoy::add_e;
	instructions[gb::Instruction::ADD_H] = &GameBoy::add_h;
	instructions[gb::Instruction::ADD_L] = &GameBoy::add_l;
	instructions[gb::Instruction::ADD_AHL] = &GameBoy::add_ahl;
	instructions[gb::Instruction::ADD_A] = &GameBoy::add_a;
	instructions[gb::Instruction::ADC_B] = &GameBoy::adc_b;
	instructions[gb::Instruction::ADC_C] = &GameBoy::adc_c;
	instructions[gb::Instruction::ADC_D] = &GameBoy::adc_d;
	instructions[gb::Instruction::ADC_E] = &GameBoy::adc_e;
	instructions[gb::Instruction::ADC_H] = &GameBoy::adc_h;
	instructions[gb::Instruction::ADC_L] = &GameBoy::adc_l;
	instructions[gb::Instruction::ADC_AHL] = &GameBoy::adc_ahl;
	instructions[gb::Instruction::ADC_A] = &GameBoy::adc_a;
	instructions[gb::Instruction::SUB_B] = &GameBoy::sub_b;
	instructions[gb::Instruction::SUB_C] = &GameBoy::sub_c;
	instructions[gb::Instruction::SUB_D] = &GameBoy::sub_d;
	instructions[gb::Instruction::SUB_E] = &GameBoy::sub_e;
	instructions[gb::Instruction::SUB_H] = &GameBoy::sub_h;
	instructions[gb::Instruction::SUB_L] = &GameBoy::sub_l;
	instructions[gb::Instruction::SUB_AHL] = &GameBoy::sub_ahl;
	instructions[gb::Instruction::SUB_A] = &GameBoy::sub_a;
	instructions[gb::Instruction::SBC_B] = &GameBoy::sbc_b;
	instructions[gb::Instruction::SBC_C] = &GameBoy::sbc_c;
	instructions[gb::Instruction::SBC_D] = &GameBoy::sbc_d;
	instructions[gb::Instruction::SBC_E] = &GameBoy::sbc_e;
	instructions[gb::Instruction::SBC_H] = &GameBoy::sbc_h;
	instructions[gb::Instruction::SBC_L] = &GameBoy::sbc_l;
	instructions[gb::Instruction::SBC_AHL] = &GameBoy::sbc_ahl;
	instructions[gb::Instruction::SBC_A] = &GameBoy::sbc_a;
	instructions[gb::Instruction::AND_B] = &GameBoy::and_b;
	instructions[gb::Instruction::AND_C] = &GameBoy::and_c;
	instructions[gb::Instruction::AND_D] = &GameBoy::and_d;
	instructions[gb::Instruction::AND_E] = &GameBoy::and_e;
	instructions[gb::Instruction::AND_H] = &GameBoy::and_h;
	instructions[gb::Instruction::AND_L] = &GameBoy::and_l;
	instructions[gb::Instruction::AND_AHL] = &GameBoy::and_ahl;
	instructions[gb::Instruction::AND_A] = &GameBoy::and_a;
	instructions[gb::Instruction::XOR_B] = &GameBoy::xor_b;
	instructions[gb::Instruction::XOR_C] = &GameBoy::xor_c;
	instructions[gb::Instruction::XOR_D] = &GameBoy::xor_d;
	instructions[gb::Instruction::XOR_E] = &GameBoy::xor_e;
	instructions[gb::Instruction::XOR_H] = &GameBoy::xor_h;
	instructions[gb::Instruction::XOR_L] = &GameBoy::xor_l;
	instructions[gb::Instruction::XOR_AHL] = &GameBoy::xor_ahl;
	instructions[gb::Instruction::XOR_A] = &GameBoy::xor_a;
	instructions[gb::Instruction::OR_B] = &GameBoy::or_b;
	instructions[gb::Instruction::OR_C] = &GameBoy::or_c;
	instructions[gb::Instruction::OR_D] = &GameBoy::or_d;
	instructions[gb::Instruction::OR_E] = &GameBoy::or_e;
	instructions[gb::Instruction::OR_H] = &GameBoy::or_h;
	instructions[gb::Instruction::OR_L] = &GameBoy::or_l;
	instructions[gb::Instruction::OR_AHL] = &GameBoy::or_ahl;
	instructions[gb::Instruction::OR_A] = &GameBoy::or_a;
	instructions[gb::Instruction::CP_B] = &GameBoy::cp_b;
	instructions[gb::Instruction::CP_C] = &GameBoy::cp_c;
	instructions[gb::Instruction::CP_D] = &GameBoy::cp_d;
	instructions[gb::Instruction::CP_E] = &GameBoy::cp_e;
	instructions[gb::Instruction::CP_H] = &GameBoy::cp_h;
	instructions[gb::Instruction::CP_L] = &GameBoy::cp_l;
	instructions[gb::Instruction::CP_AHL] = &GameBoy::cp_ahl;
	instructions[gb::Instruction::CP_A] = &GameBoy::cp_a;
	instructions[gb::Instruction::RET_NZ] = &GameBoy::ret_nz;
	instructions[gb::Instruction::POP_BC] = &GameBoy::pop_bc;
	instructions[gb::Instruction::JP_NZ_A16] = &GameBoy::jp_nz_a16;
	instructions[gb::Instruction::JP_A16] = &GameBoy::jp_a16;
	instructions[gb::Instruction::CALL_NZ_A16] = &GameBoy::call_nz_a16;
	instructions[gb::Instruction::PUSH_BC] = &GameBoy::push_bc;
	instructions[gb::Instruction::ADD_I8] = &GameBoy::add_i8;
	instructions[gb::Instruction::RST_00] = &GameBoy::rst_00;
	instructions[gb::Instruction::RET_Z] = &GameBoy::ret_z;
	instructions[gb::Instruction::RET] = &GameBoy::ret;
	instructions[gb::Instruction::JP_Z_A16] = &GameBoy::jp_z_a16;
	instructions[gb::Instruction::PREFIX_CB] = &GameBoy::prefix_cb;
	instructions[gb::Instruction::CALL_Z_A16] = &GameBoy::call_z_a16;
	instructions[gb::Instruction::CALL_A16] = &GameBoy::call_a16;
	instructions[gb::Instruction::ADC_I8] = &GameBoy::adc_i8;
	instructions[gb::Instruction::RST_08] = &GameBoy::rst_08;
	instructions[gb::Instruction::RET_NC] = &GameBoy::ret_nc;
	instructions[gb::Instruction::POP_DE] = &GameBoy::pop_de;
	instructions[gb::Instruction::JP_NC_A16] = &GameBoy::jp_nc_a16;
	instructions[gb::Instruction::CALL_NC_A16] = &GameBoy::call_nc_a16;
	instructions[gb::Instruction::PUSH_DE] = &GameBoy::push_de;
	instructions[gb::Instruction::SUB_I8] = &GameBoy::sub_i8;
	instructions[gb::Instruction::RST_10] = &GameBoy::rst_10;
	instructions[gb::Instruction::RET_C] = &GameBoy::ret_c;
	instructions[gb::Instruction::RETI] = &GameBoy::reti;
	instructions[gb::Instruction::JP_C_A16] = &GameBoy::jp_c_a16;
	instructions[gb::Instruction::CALL_C_A16] = &GameBoy::call_c_a16;
	instructions[gb::Instruction::SBC_I8] = &GameBoy::sbc_i8;
	instructions[gb::Instruction::RST_18] = &GameBoy::rst_18;
	instructions[gb::Instruction::LDH_A8_A] = &GameBoy::ldh_a8_a;
	instructions[gb::Instruction::POP_HL] = &GameBoy::pop_hl;
	instructions[gb::Instruction::LD_AC_A] = &GameBoy::ld_ac_a;
	instructions[gb::Instruction::PUSH_HL] = &GameBoy::push_hl;
	instructions[gb::Instruction::AND_I8] = &GameBoy::and_i8;
	instructions[gb::Instruction::RST_20] = &GameBoy::rst_20;
	instructions[gb::Instruction::ADD_SP_S8] = &GameBoy::add_sp_s8;
	instructions[gb::Instruction::JP_HL] = &GameBoy::jp_hl;
	instructions[gb::Instruction::LD_A16_A] = &GameBoy::ld_a16_a;
	instructions[gb::Instruction::XOR_I8] = &GameBoy::xor_i8;
	instructions[gb::Instruction::RST_28] = &GameBoy::rst_28;
	instructions[gb::Instruction::LDH_A_A8] = &GameBoy::ldh_a_a8;
	instructions[gb::Instruction::POP_AF] = &GameBoy::pop_af;
	instructions[gb::Instruction::LD_A_AC] = &GameBoy::ld_a_ac;
	instructions[gb::Instruction::DI] = &GameBoy::di;
	instructions[gb::Instruction::PUSH_AF] = &GameBoy::push_af;
	instructions[gb::Instruction::OR_I8] = &GameBoy::or_i8;
	instructions[gb::Instruction::RST_30] = &GameBoy::rst_30;
	instructions[gb::Instruction::LD_HL_SP_S8] = &GameBoy::ld_hl_sp_s8;
	instructions[gb::Instruction::LD_SP_HL] = &GameBoy::ld_sp_hl;
	instructions[gb::Instruction::LD_A_A16] = &GameBoy::ld_a_a16;
	instructions[gb::Instruction::EI] = &GameBoy::ei;
	instructions[gb::Instruction::CP_I8] = &GameBoy::cp_i8;
	instructions[gb::Instruction::RST_38] = &GameBoy::rst_38;

	cb_instructions.fill(&GameBoy::cb_invalid);
	cb_instructions[gb::CBInstruction::RLC_B] = &GameBoy::rlc_b;
	cb_instructions[gb::CBInstruction::RLC_C] = &GameBoy::rlc_c;
	cb_instructions[gb::CBInstruction::RLC_D] = &GameBoy::rlc_d;
	cb_instructions[gb::CBInstruction::RLC_E] = &GameBoy::rlc_e;
	cb_instructions[gb::CBInstruction::RLC_H] = &GameBoy::rlc_h;
	cb_instructions[gb::CBInstruction::RLC_L] = &GameBoy::rlc_l;
	cb_instructions[gb::CBInstruction::RLC_AHL] = &GameBoy::rlc_ahl;
	cb_instructions[gb::CBInstruction::RLC_A] = &GameBoy::rlc_a;
	cb_instructions[gb::CBInstruction::RRC_B] = &GameBoy::rrc_b;
	cb_instructions[gb::CBInstruction::RRC_C] = &GameBoy::rrc_c;
	cb_instructions[gb::CBInstruction::RRC_D] = &GameBoy::rrc_d;
	cb_instructions[gb::CBInstruction::RRC_E] = &GameBoy::rrc_e;
	cb_instructions[gb::CBInstruction::RRC_H] = &GameBoy::rrc_h;
	cb_instructions[gb::CBInstruction::RRC_L] = &GameBoy::rrc_l;
	cb_instructions[gb::CBInstruction::RRC_AHL] = &GameBoy::rrc_ahl;
	cb_instructions[gb::CBInstruction::RRC_A] = &GameBoy::rrc_a;
	cb_instructions[gb::CBInstruction::RL_B] = &GameBoy::rl_b;
	cb_instructions[gb::CBInstruction::RL_C] = &GameBoy::rl_c;
	cb_instructions[gb::CBInstruction::RL_D] = &GameBoy::rl_d;
	cb_instructions[gb::CBInstruction::RL_E] = &GameBoy::rl_e;
	cb_instructions[gb::CBInstruction::RL_H] = &GameBoy::rl_h;
	cb_instructions[gb::CBInstruction::RL_L] = &GameBoy::rl_l;
	cb_instructions[gb::CBInstruction::RL_AHL] = &GameBoy::rl_ahl;
	cb_instructions[gb::CBInstruction::RL_A] = &GameBoy::rl_a;
	cb_instructions[gb::CBInstruction::RR_B] = &GameBoy::rr_b;
	cb_instructions[gb::CBInstruction::RR_C] = &GameBoy::rr_c;
	cb_instructions[gb::CBInstruction::RR_D] = &GameBoy::rr_d;
	cb_instructions[gb::CBInstruction::RR_E] = &GameBoy::rr_e;
	cb_instructions[gb::CBInstruction::RR_H] = &GameBoy::rr_h;
	cb_instructions[gb::CBInstruction::RR_L] = &GameBoy::rr_l;
	cb_instructions[gb::CBInstruction::RR_AHL] = &GameBoy::rr_ahl;
	cb_instructions[gb::CBInstruction::RR_A] = &GameBoy::rr_a;
	cb_instructions[gb::CBInstruction::SLA_B] = &GameBoy::sla_b;
	cb_instructions[gb::CBInstruction::SLA_C] = &GameBoy::sla_c;
	cb_instructions[gb::CBInstruction::SLA_D] = &GameBoy::sla_d;
	cb_instructions[gb::CBInstruction::SLA_E] = &GameBoy::sla_e;
	cb_instructions[gb::CBInstruction::SLA_H] = &GameBoy::sla_h;
	cb_instructions[gb::CBInstruction::SLA_L] = &GameBoy::sla_l;
	cb_instructions[gb::CBInstruction::SLA_AHL] = &GameBoy::sla_ahl;
	cb_instructions[gb::CBInstruction::SLA_A] = &GameBoy::sla_a;
	cb_instructions[gb::CBInstruction::SRA_B] = &GameBoy::sra_b;
	cb_instructions[gb::CBInstruction::SRA_C] = &GameBoy::sra_c;
	cb_instructions[gb::CBInstruction::SRA_D] = &GameBoy::sra_d;
	cb_instructions[gb::CBInstruction::SRA_E] = &GameBoy::sra_e;
	cb_instructions[gb::CBInstruction::SRA_H] = &GameBoy::sra_h;
	cb_instructions[gb::CBInstruction::SRA_L] = &GameBoy::sra_l;
	cb_instructions[gb::CBInstruction::SRA_AHL] = &GameBoy::sra_ahl;
	cb_instructions[gb::CBInstruction::SRA_A] = &GameBoy::sra_a;
	cb_instructions[gb::CBInstruction::SWAP_B] = &GameBoy::swap_b;
	cb_instructions[gb::CBInstruction::SWAP_C] = &GameBoy::swap_c;
	cb_instructions[gb::CBInstruction::SWAP_D] = &GameBoy::swap_d;
	cb_instructions[gb::CBInstruction::SWAP_E] = &GameBoy::swap_e;
	cb_instructions[gb::CBInstruction::SWAP_H] = &GameBoy::swap_h;
	cb_instructions[gb::CBInstruction::SWAP_L] = &GameBoy::swap_l;
	cb_instructions[gb::CBInstruction::SWAP_AHL] = &GameBoy::swap_ahl;
	cb_instructions[gb::CBInstruction::SWAP_A] = &GameBoy::swap_a;
	cb_instructions[gb::CBInstruction::SRL_B] = &GameBoy::srl_b;
	cb_instructions[gb::CBInstruction::SRL_C] = &GameBoy::srl_c;
	cb_instructions[gb::CBInstruction::SRL_D] = &GameBoy::srl_d;
	cb_instructions[gb::CBInstruction::SRL_E] = &GameBoy::srl_e;
	cb_instructions[gb::CBInstruction::SRL_H] = &GameBoy::srl_h;
	cb_instructions[gb::CBInstruction::SRL_L] = &GameBoy::srl_l;
	cb_instructions[gb::CBInstruction::SRL_AHL] = &GameBoy::srl_ahl;
	cb_instructions[gb::CBInstruction::SRL_A] = &GameBoy::srl_a;
	cb_instructions[gb::CBInstruction::BIT_0_B] = &GameBoy::bit_0_b;
	cb_instructions[gb::CBInstruction::BIT_0_C] = &GameBoy::bit_0_c;
	cb_instructions[gb::CBInstruction::BIT_0_D] = &GameBoy::bit_0_d;
	cb_instructions[gb::CBInstruction::BIT_0_E] = &GameBoy::bit_0_e;
	cb_instructions[gb::CBInstruction::BIT_0_H] = &GameBoy::bit_0_h;
	cb_instructions[gb::CBInstruction::BIT_0_L] = &GameBoy::bit_0_l;
	cb_instructions[gb::CBInstruction::BIT_0_AHL] = &GameBoy::bit_0_ahl;
	cb_instructions[gb::CBInstruction::BIT_0_A] = &GameBoy::bit_0_a;
	cb_instructions[gb::CBInstruction::BIT_1_B] = &GameBoy::bit_1_b;
	cb_instructions[gb::CBInstruction::BIT_1_C] = &GameBoy::bit_1_c;
	cb_instructions[gb::CBInstruction::BIT_1_D] = &GameBoy::bit_1_d;
	cb_instructions[gb::CBInstruction::BIT_1_E] = &GameBoy::bit_1_e;
	cb_instructions[gb::CBInstruction::BIT_1_H] = &GameBoy::bit_1_h;
	cb_instructions[gb::CBInstruction::BIT_1_L] = &GameBoy::bit_1_l;
	cb_instructions[gb::CBInstruction::BIT_1_AHL] = &GameBoy::bit_1_ahl;
	cb_instructions[gb::CBInstruction::BIT_1_A] = &GameBoy::bit_1_a;
	cb_instructions[gb::CBInstruction::BIT_2_B] = &GameBoy::bit_2_b;
	cb_instructions[gb::CBInstruction::BIT_2_C] = &GameBoy::bit_2_c;
	cb_instructions[gb::CBInstruction::BIT_2_D] = &GameBoy::bit_2_d;
	cb_instructions[gb::CBInstruction::BIT_2_E] = &GameBoy::bit_2_e;
	cb_instructions[gb::CBInstruction::BIT_2_H] = &GameBoy::bit_2_h;
	cb_instructions[gb::CBInstruction::BIT_2_L] = &GameBoy::bit_2_l;
	cb_instructions[gb::CBInstruction::BIT_2_AHL] = &GameBoy::bit_2_ahl;
	cb_instructions[gb::CBInstruction::BIT_2_A] = &GameBoy::bit_2_a;
	cb_instructions[gb::CBInstruction::BIT_3_B] = &GameBoy::bit_3_b;
	cb_instructions[gb::CBInstruction::BIT_3_C] = &GameBoy::bit_3_c;
	cb_instructions[gb::CBInstruction::BIT_3_D] = &GameBoy::bit_3_d;
	cb_instructions[gb::CBInstruction::BIT_3_E] = &GameBoy::bit_3_e;
	cb_instructions[gb::CBInstruction::BIT_3_H] = &GameBoy::bit_3_h;
	cb_instructions[gb::CBInstruction::BIT_3_L] = &GameBoy::bit_3_l;
	cb_instructions[gb::CBInstruction::BIT_3_AHL] = &GameBoy::bit_3_ahl;
	cb_instructions[gb::CBInstruction::BIT_3_A] = &GameBoy::bit_3_a;
	cb_instructions[gb::CBInstruction::BIT_4_B] = &GameBoy::bit_4_b;
	cb_instructions[gb::CBInstruction::BIT_4_C] = &GameBoy::bit_4_c;
	cb_instructions[gb::CBInstruction::BIT_4_D] = &GameBoy::bit_4_d;
	cb_instructions[gb::CBInstruction::BIT_4_E] = &GameBoy::bit_4_e;
	cb_instructions[gb::CBInstruction::BIT_4_H] = &GameBoy::bit_4_h;
	cb_instructions[gb::CBInstruction::BIT_4_L] = &GameBoy::bit_4_l;
	cb_instructions[gb::CBInstruction::BIT_4_AHL] = &GameBoy::bit_4_ahl;
	cb_instructions[gb::CBInstruction::BIT_4_A] = &GameBoy::bit_4_a;
	cb_instructions[gb::CBInstruction::BIT_5_B] = &GameBoy::bit_5_b;
	cb_instructions[gb::CBInstruction::BIT_5_C] = &GameBoy::bit_5_c;
	cb_instructions[gb::CBInstruction::BIT_5_D] = &GameBoy::bit_5_d;
	cb_instructions[gb::CBInstruction::BIT_5_E] = &GameBoy::bit_5_e;
	cb_instructions[gb::CBInstruction::BIT_5_H] = &GameBoy::bit_5_h;
	cb_instructions[gb::CBInstruction::BIT_5_L] = &GameBoy::bit_5_l;
	cb_instructions[gb::CBInstruction::BIT_5_AHL] = &GameBoy::bit_5_ahl;
	cb_instructions[gb::CBInstruction::BIT_5_A] = &GameBoy::bit_5_a;
	cb_instructions[gb::CBInstruction::BIT_6_B] = &GameBoy::bit_6_b;
	cb_instructions[gb::CBInstruction::BIT_6_C] = &GameBoy::bit_6_c;
	cb_instructions[gb::CBInstruction::BIT_6_D] = &GameBoy::bit_6_d;
	cb_instructions[gb::CBInstruction::BIT_6_E] = &GameBoy::bit_6_e;
	cb_instructions[gb::CBInstruction::BIT_6_H] = &GameBoy::bit_6_h;
	cb_instructions[gb::CBInstruction::BIT_6_L] = &GameBoy::bit_6_l;
	cb_instructions[gb::CBInstruction::BIT_6_AHL] = &GameBoy::bit_6_ahl;
	cb_instructions[gb::CBInstruction::BIT_6_A] = &GameBoy::bit_6_a;
	cb_instructions[gb::CBInstruction::BIT_7_B] = &GameBoy::bit_7_b;
	cb_instructions[gb::CBInstruction::BIT_7_C] = &GameBoy::bit_7_c;
	cb_instructions[gb::CBInstruction::BIT_7_D] = &GameBoy::bit_7_d;
	cb_instructions[gb::CBInstruction::BIT_7_E] = &GameBoy::bit_7_e;
	cb_instructions[gb::CBInstruction::BIT_7_H] = &GameBoy::bit_7_h;
	cb_instructions[gb::CBInstruction::BIT_7_L] = &GameBoy::bit_7_l;
	cb_instructions[gb::CBInstruction::BIT_7_AHL] = &GameBoy::bit_7_ahl;
	cb_instructions[gb::CBInstruction::BIT_7_A] = &GameBoy::bit_7_a;
	cb_instructions[gb::CBInstruction::RES_0_B] = &GameBoy::res_0_b;
	cb_instructions[gb::CBInstruction::RES_0_C] = &GameBoy::res_0_c;
	cb_instructions[gb::CBInstruction::RES_0_D] = &GameBoy::res_0_d;
	cb_instructions[gb::CBInstruction::RES_0_E] = &GameBoy::res_0_e;
	cb_instructions[gb::CBInstruction::RES_0_H] = &GameBoy::res_0_h;
	cb_instructions[gb::CBInstruction::RES_0_L] = &GameBoy::res_0_l;
	cb_instructions[gb::CBInstruction::RES_0_AHL] = &GameBoy::res_0_ahl;
	cb_instructions[gb::CBInstruction::RES_0_A] = &GameBoy::res_0_a;
	cb_instructions[gb::CBInstruction::RES_1_B] = &GameBoy::res_1_b;
	cb_instructions[gb::CBInstruction::RES_1_C] = &GameBoy::res_1_c;
	cb_instructions[gb::CBInstruction::RES_1_D] = &GameBoy::res_1_d;
	cb_instructions[gb::CBInstruction::RES_1_E] = &GameBoy::res_1_e;
	cb_instructions[gb::CBInstruction::RES_1_H] = &GameBoy::res_1_h;
	cb_instructions[gb::CBInstruction::RES_1_L] = &GameBoy::res_1_l;
	cb_instructions[gb::CBInstruction::RES_1_AHL] = &GameBoy::res_1_ahl;
	cb_instructions[gb::CBInstruction::RES_1_A] = &GameBoy::res_1_a;
	cb_instructions[gb::CBInstruction::RES_2_B] = &GameBoy::res_2_b;
	cb_instructions[gb::CBInstruction::RES_2_C] = &GameBoy::res_2_c;
	cb_instructions[gb::CBInstruction::RES_2_D] = &GameBoy::res_2_d;
	cb_instructions[gb::CBInstruction::RES_2_E] = &GameBoy::res_2_e;
	cb_instructions[gb::CBInstruction::RES_2_H] = &GameBoy::res_2_h;
	cb_instructions[gb::CBInstruction::RES_2_L] = &GameBoy::res_2_l;
	cb_instructions[gb::CBInstruction::RES_2_AHL] = &GameBoy::res_2_ahl;
	cb_instructions[gb::CBInstruction::RES_2_A] = &GameBoy::res_2_a;
	cb_instructions[gb::CBInstruction::RES_3_B] = &GameBoy::res_3_b;
	cb_instructions[gb::CBInstruction::RES_3_C] = &GameBoy::res_3_c;
	cb_instructions[gb::CBInstruction::RES_3_D] = &GameBoy::res_3_d;
	cb_instructions[gb::CBInstruction::RES_3_E] = &GameBoy::res_3_e;
	cb_instructions[gb::CBInstruction::RES_3_H] = &GameBoy::res_3_h;
	cb_instructions[gb::CBInstruction::RES_3_L] = &GameBoy::res_3_l;
	cb_instructions[gb::CBInstruction::RES_3_AHL] = &GameBoy::res_3_ahl;
	cb_instructions[gb::CBInstruction::RES_3_A] = &GameBoy::res_3_a;
	cb_instructions[gb::CBInstruction::RES_4_B] = &GameBoy::res_4_b;
	cb_instructions[gb::CBInstruction::RES_4_C] = &GameBoy::res_4_c;
	cb_instructions[gb::CBInstruction::RES_4_D] = &GameBoy::res_4_d;
	cb_instructions[gb::CBInstruction::RES_4_E] = &GameBoy::res_4_e;
	cb_instructions[gb::CBInstruction::RES_4_H] = &GameBoy::res_4_h;
	cb_instructions[gb::CBInstruction::RES_4_L] = &GameBoy::res_4_l;
	cb_instructions[gb::CBInstruction::RES_4_AHL] = &GameBoy::res_4_ahl;
	cb_instructions[gb::CBInstruction::RES_4_A] = &GameBoy::res_4_a;
	cb_instructions[gb::CBInstruction::RES_5_B] = &GameBoy::res_5_b;
	cb_instructions[gb::CBInstruction::RES_5_C] = &GameBoy::res_5_c;
	cb_instructions[gb::CBInstruction::RES_5_D] = &GameBoy::res_5_d;
	cb_instructions[gb::CBInstruction::RES_5_E] = &GameBoy::res_5_e;
	cb_instructions[gb::CBInstruction::RES_5_H] = &GameBoy::res_5_h;
	cb_instructions[gb::CBInstruction::RES_5_L] = &GameBoy::res_5_l;
	cb_instructions[gb::CBInstruction::RES_5_AHL] = &GameBoy::res_5_ahl;
	cb_instructions[gb::CBInstruction::RES_5_A] = &GameBoy::res_5_a;
	cb_instructions[gb::CBInstruction::RES_6_B] = &GameBoy::res_6_b;
	cb_instructions[gb::CBInstruction::RES_6_C] = &GameBoy::res_6_c;
	cb_instructions[gb::CBInstruction::RES_6_D] = &GameBoy::res_6_d;
	cb_instructions[gb::CBInstruction::RES_6_E] = &GameBoy::res_6_e;
	cb_instructions[gb::CBInstruction::RES_6_H] = &GameBoy::res_6_h;
	cb_instructions[gb::CBInstruction::RES_6_L] = &GameBoy::res_6_l;
	cb_instructions[gb::CBInstruction::RES_6_AHL] = &GameBoy::res_6_ahl;
	cb_instructions[gb::CBInstruction::RES_6_A] = &GameBoy::res_6_a;
	cb_instructions[gb::CBInstruction::RES_7_B] = &GameBoy::res_7_b;
	cb_instructions[gb::CBInstruction::RES_7_C] = &GameBoy::res_7_c;
	cb_instructions[gb::CBInstruction::RES_7_D] = &GameBoy::res_7_d;
	cb_instructions[gb::CBInstruction::RES_7_E] = &GameBoy::res_7_e;
	cb_instructions[gb::CBInstruction::RES_7_H] = &GameBoy::res_7_h;
	cb_instructions[gb::CBInstruction::RES_7_L] = &GameBoy::res_7_l;
	cb_instructions[gb::CBInstruction::RES_7_AHL] = &GameBoy::res_7_ahl;
	cb_instructions[gb::CBInstruction::RES_7_A] = &GameBoy::res_7_a;
	cb_instructions[gb::CBInstruction::SET_0_B] = &GameBoy::set_0_b;
	cb_instructions[gb::CBInstruction::SET_0_C] = &GameBoy::set_0_c;
	cb_instructions[gb::CBInstruction::SET_0_D] = &GameBoy::set_0_d;
	cb_instructions[gb::CBInstruction::SET_0_E] = &GameBoy::set_0_e;
	cb_instructions[gb::CBInstruction::SET_0_H] = &GameBoy::set_0_h;
	cb_instructions[gb::CBInstruction::SET_0_L] = &GameBoy::set_0_l;
	cb_instructions[gb::CBInstruction::SET_0_AHL] = &GameBoy::set_0_ahl;
	cb_instructions[gb::CBInstruction::SET_0_A] = &GameBoy::set_0_a;
	cb_instructions[gb::CBInstruction::SET_1_B] = &GameBoy::set_1_b;
	cb_instructions[gb::CBInstruction::SET_1_C] = &GameBoy::set_1_c;
	cb_instructions[gb::CBInstruction::SET_1_D] = &GameBoy::set_1_d;
	cb_instructions[gb::CBInstruction::SET_1_E] = &GameBoy::set_1_e;
	cb_instructions[gb::CBInstruction::SET_1_H] = &GameBoy::set_1_h;
	cb_instructions[gb::CBInstruction::SET_1_L] = &GameBoy::set_1_l;
	cb_instructions[gb::CBInstruction::SET_1_AHL] = &GameBoy::set_1_ahl;
	cb_instructions[gb::CBInstruction::SET_1_A] = &GameBoy::set_1_a;
	cb_instructions[gb::CBInstruction::SET_2_B] = &GameBoy::set_2_b;
	cb_instructions[gb::CBInstruction::SET_2_C] = &GameBoy::set_2_c;
	cb_instructions[gb::CBInstruction::SET_2_D] = &GameBoy::set_2_d;
	cb_instructions[gb::CBInstruction::SET_2_E] = &GameBoy::set_2_e;
	cb_instructions[gb::CBInstruction::SET_2_H] = &GameBoy::set_2_h;
	cb_instructions[gb::CBInstruction::SET_2_L] = &GameBoy::set_2_l;
	cb_instructions[gb::CBInstruction::SET_2_AHL] = &GameBoy::set_2_ahl;
	cb_instructions[gb::CBInstruction::SET_2_A] = &GameBoy::set_2_a;
	cb_instructions[gb::CBInstruction::SET_3_B] = &GameBoy::set_3_b;
	cb_instructions[gb::CBInstruction::SET_3_C] = &GameBoy::set_3_c;
	cb_instructions[gb::CBInstruction::SET_3_D] = &GameBoy::set_3_d;
	cb_instructions[gb::CBInstruction::SET_3_E] = &GameBoy::set_3_e;
	cb_instructions[gb::CBInstruction::SET_3_H] = &GameBoy::set_3_h;
	cb_instructions[gb::CBInstruction::SET_3_L] = &GameBoy::set_3_l;
	cb_instructions[gb::CBInstruction::SET_3_AHL] = &GameBoy::set_3_ahl;
	cb_instructions[gb::CBInstruction::SET_3_A] = &GameBoy::set_3_a;
	cb_instructions[gb::CBInstruction::SET_4_B] = &GameBoy::set_4_b;
	cb_instructions[gb::CBInstruction::SET_4_C] = &GameBoy::set_4_c;
	cb_instructions[gb::CBInstruction::SET_4_D] = &GameBoy::set_4_d;
	cb_instructions[gb::CBInstruction::SET_4_E] = &GameBoy::set_4_e;
	cb_instructions[gb::CBInstruction::SET_4_H] = &GameBoy::set_4_h;
	cb_instructions[gb::CBInstruction::SET_4_L] = &GameBoy::set_4_l;
	cb_instructions[gb::CBInstruction::SET_4_AHL] = &GameBoy::set_4_ahl;
	cb_instructions[gb::CBInstruction::SET_4_A] = &GameBoy::set_4_a;
	cb_instructions[gb::CBInstruction::SET_5_B] = &GameBoy::set_5_b;
	cb_instructions[gb::CBInstruction::SET_5_C] = &GameBoy::set_5_c;
	cb_instructions[gb::CBInstruction::SET_5_D] = &GameBoy::set_5_d;
	cb_instructions[gb::CBInstruction::SET_5_E] = &GameBoy::set_5_e;
	cb_instructions[gb::CBInstruction::SET_5_H] = &GameBoy::set_5_h;
	cb_instructions[gb::CBInstruction::SET_5_L] = &GameBoy::set_5_l;
	cb_instructions[gb::CBInstruction::SET_5_AHL] = &GameBoy::set_5_ahl;
	cb_instructions[gb::CBInstruction::SET_5_A] = &GameBoy::set_5_a;
	cb_instructions[gb::CBInstruction::SET_6_B] = &GameBoy::set_6_b;
	cb_instructions[gb::CBInstruction::SET_6_C] = &GameBoy::set_6_c;
	cb_instructions[gb::CBInstruction::SET_6_D] = &GameBoy::set_6_d;
	cb_instructions[gb::CBInstruction::SET_6_E] = &GameBoy::set_6_e;
	cb_instructions[gb::CBInstruction::SET_6_H] = &GameBoy::set_6_h;
	cb_instructions[gb::CBInstruction::SET_6_L] = &GameBoy::set_6_l;
	cb_instructions[gb::CBInstruction::SET_6_AHL] = &GameBoy::set_6_ahl;
	cb_instructions[gb::CBInstruction::SET_6_A] = &GameBoy::set_6_a;
	cb_instructions[gb::CBInstruction::SET_7_B] = &GameBoy::set_7_b;
	cb_instructions[gb::CBInstruction::SET_7_C] = &GameBoy::set_7_c;
	cb_instructions[gb::CBInstruction::SET_7_D] = &GameBoy::set_7_d;
	cb_instructions[gb::CBInstruction::SET_7_E] = &GameBoy::set_7_e;
	cb_instructions[gb::CBInstruction::SET_7_H] = &GameBoy::set_7_h;
	cb_instructions[gb::CBInstruction::SET_7_L] = &GameBoy::set_7_l;
	cb_instructions[gb::CBInstruction::SET_7_AHL] = &GameBoy::set_7_ahl;
	cb_instructions[gb::CBInstruction::SET_7_A] = &GameBoy::set_7_a;
}

GameBoy::~GameBoy()
{
	emulating = false;

	debugging = false;
	pause = false;
	SetEvent(data_event);
}

bool GameBoy::load_rom(std::ifstream& rom, u16 address, u16 size)
{
	rom.read(reinterpret_cast<char*>(memory + address), size);

	return true;
}

bool GameBoy::load_rom(fs::path path, u16 address, u16 size)
{
	std::ifstream rom(path, std::ios::binary);

	if (!rom)
	{
		error("Failed to open ROM file: %s", std::strerror(errno));
		return false;
	}

	// Get the size of the file
	u64 length = fs::file_size(path);

	if (length < size)
	{
		error("The ROM is too small. (0x%X of 0x%X)", length, size);
		return false;
	}

	return load_rom(rom, address, size);
}

bool GameBoy::initialize()
{
	std::ifstream rom(executable.path, std::ios::binary);

	if (!rom)
	{
		error("Failed to open ROM file: %s", std::strerror(errno));
		return false;
	}

	rom.read(reinterpret_cast<char*>(memory), 0x8000);

	if (memory[gb::ROM_MBC] == 0x0)
	{
		rom_type = ROMType::ROM;
	}
	else if (memory[gb::ROM_MBC] == 0x1 || memory[gb::ROM_MBC] == 0x2)
	{
		rom_type = ROMType::MBC1;
		rom_banks.resize(4);

		for (u8 i = 0; i < 4; ++i)
		{
			rom_banks[i].resize(0x2000);
			rom.read(reinterpret_cast<char*>(rom_banks[i].data()), 0x2000);
		}

		if (memory[gb::ROM_MBC] == 0x2)
		{
			ram = true;
		}
	}
	else
	{
		error("Unsupported MBC type. (0x%X)", memory[0x147]);
		return false;
	}

	// Since the executable ROM must be in the memory during boot ROM execution,
	// we load it beforehand and load the boot ROM over it
	constexpr bool load_boot_rom = false;

	if (load_boot_rom)
	{
		if (!load_rom(fs::current_path() / "Game Boy" / "boot_rom.gb", 0x0, 0x100))
		{
			return false;
		}
	}

	// Prepare rendering
	if (!cfg::game_boy.headless)
	{
		u32 width = 160;
		u32 height = 144;
		u8 scaling = 3;
		renderer.reset(PixelRenderer::create(cfg::game_boy.renderer, executable.name, width, height, scaling, scaling, true));

		if (!renderer)
		{
			error("Failed to create a pixel renderer instance.");
			return false;
		}

		pixels.resize(width * height * 3);
		std::memset(pixels.data(), 0, pixels.size());

		renderer->set_pixels(pixels.data());
		renderer->frame->bind_key_down(&GameBoy::key_down, this);
		renderer->frame->bind_key_up(&GameBoy::key_up, this);
		renderer->frame->bind_close(&GameBoy::exit, reinterpret_cast<Emulator*>(this));
	}

	// Fill the memory with random values
	std::random_device seed;
	std::mt19937 rng(seed());
	std::uniform_int_distribution<> gen(0, 0xFF);

	for (u64 i = fs::file_size(executable.path); i < 0xFFFF; ++i)
	{
		memory[i] = static_cast<u8>(gen(rng));
	}

	// Initialize I/O registers to defaults
	memory[gb::JOYPAD] = 0xCF;
	memory[gb::SC] = 0x7E;
	memory[gb::TIMA] = 0;
	memory[gb::TMA] = 0;
	memory[gb::TAC] = 0xF8;
	memory[gb::IF] = 0xE1;
	memory[gb::SCY] = 0;
	memory[gb::SCX] = 0;
	memory[gb::LY] = 0;
	memory[gb::LYC] = 0;
	memory[gb::WY] = 0;
	memory[gb::WX] = 0;
	memory[gb::IE] = 0;

	if (load_boot_rom)
	{
		memory[gb::DIV] = 0;
		memory[gb::TAC] = 0;
		memory[gb::LCDC] = 0;
		memory[gb::STAT] = 0x84;
	}
	else
	{
		real_cycle = 0x20D4;

		regs.AF = 0x01B0;
		regs.BC = 0x0013;
		regs.DE = 0x00D8;
		regs.HL = 0x014D;
		regs.SP = 0xFFFE;
		regs.PC = 0x100;

		memory[gb::DIV] = 0xAB;
		memory[gb::LCDC] = 0x91;
		memory[gb::STAT] = 0x85;

		std::memset(&memory[0x8000], 0, 0x1FFF);
	}

	return true;
}

void GameBoy::key_down(wxKeyEvent& event)
{
	switch (event.GetKeyCode())
	{
		case wxKeyCode::WXK_RIGHT:
		{
			p14.right = false;
			break;
		}

		case wxKeyCode::WXK_LEFT:
		{
			p14.left = false;
			break;
		}

		case wxKeyCode::WXK_UP:
		{
			p14.up = false;
			break;
		}

		case wxKeyCode::WXK_DOWN:
		{
			p14.down = false;
			break;
		}

		case 'A':
		{
			p15.a = false;
			break;
		}

		case 'S':
		{
			p15.b = false;
			break;
		}

		case wxKeyCode::WXK_SPACE:
		{
			p15.select = false;
			break;
		}

		case wxKeyCode::WXK_RETURN:
		{
			p15.start = false;
			//memory[gb::DIV] = 0x40;
			//debug("DIV: 0x%02X", memory[gb::DIV]);
			break;
		}
	}
}

void GameBoy::key_up(wxKeyEvent& event)
{
	switch (event.GetKeyCode())
	{
		case wxKeyCode::WXK_RIGHT:
		{
			p14.right = true;
			break;
		}

		case wxKeyCode::WXK_LEFT:
		{
			p14.left = true;
			break;
		}

		case wxKeyCode::WXK_UP:
		{
			p14.up = true;
			break;
		}

		case wxKeyCode::WXK_DOWN:
		{
			p14.down = true;
			break;
		}

		case 'A':
		{
			p15.a = true;
			break;
		}

		case 'S':
		{
			p15.b = true;
			break;
		}

		case wxKeyCode::WXK_SPACE:
		{
			p15.select = true;
			break;
		}

		case wxKeyCode::WXK_RETURN:
		{
			p15.start = true;
			break;
		}
	}
}

void GameBoy::handle_interrupts()
{
	if (interrupts)
	{
		gb::INTRegister ie_reg;
		gb::INTRegister if_reg;
		ie_reg.value = memory[gb::IE];
		if_reg.value = memory[gb::IF];

		u16 new_pc;

		if (ie_reg.vblank && if_reg.vblank)
		{
			if_reg.vblank = false;
			new_pc = 0x40;
		}
		else if (ie_reg.lcd_stat && if_reg.lcd_stat)
		{
			if_reg.lcd_stat = false;
			new_pc = 0x48;
		}
		else if (ie_reg.timer && if_reg.timer)
		{
			if_reg.timer = false;
			new_pc = 0x50;
		}
		else if (ie_reg.serial_io && if_reg.serial_io)
		{
			if_reg.serial_io = false;
			new_pc = 0x58;
		}
		else if (ie_reg.joypad && if_reg.joypad)
		{
			if_reg.joypad = false;
			new_pc = 0x60;
		}
		else
		{
			return;
		}

		halted = false;
		interrupts = false;
		memory[gb::IF] = if_reg.value;

		push(regs.PC);
		regs.PC = new_pc;
		cycle += 5;
	}
	else if (interrupt_delay)
	{
		interrupt_delay = false;
		interrupts = true;
	}
	else if ((memory[gb::IF] & memory[gb::IE] & 0x1F) && !halted_skip)
	{
		halted = false;
	}
}

static constexpr u8 colour_table[][3] =
{
	{ 224, 248, 208 },
	{ 136, 192, 112 },
	{ 52, 104, 86 },
	{ 8, 24, 32 },
};

void GameBoy::draw_pixel(u8 x, u8 y, u8 colour)
{
	u32 pixel_index = (y * 160 * 3) + x * 3;
	pixels[pixel_index] = colour_table[colour][0];
	pixels[pixel_index + 1] = colour_table[colour][1];
	pixels[pixel_index + 2] = colour_table[colour][2];
}

void GameBoy::draw_tile(u16 tile_addr, u8 x, u8 y)
{
	gb::Palette bgp;
	bgp.value = memory[gb::BGP];

	u16 tile = (memory[tile_addr + 1] << 8) | memory[tile_addr];

	for (u8 pix = 0; pix < 8; ++pix)
	{
		u8 j = 7 - pix;
		u8 colour = (tile >> (7 + j)) & 0x2 | (tile >> j) & 0x1;

		draw_pixel(x + pix, y, bgp[colour]);
	}
}

void GameBoy::process_lcd()
{
	if (lcd_enable)
	{
		lcd_clock += cycle;

		gb::INTRegister if_reg;
		gb::STATRegister stat;
		if_reg.value = memory[gb::IF];
		stat.value = memory[gb::STAT];

		u8 ly = memory[gb::LY];

		// non-VBlank period
		if (lcd_clock < 16416)
		{
			// OAM read
			if ((lcd_clock % 114) < 20)
			{
				if (stat.mode != 2)
				{
					if (stat.mode != 1)
					{
						++ly;
					}
				}

				stat.mode = 2;

				// TODO: OAM STAT interrupt
			}
			// VRAM read
			else if ((lcd_clock % 114) < 63)
			{
				if (stat.mode != 3)
				{
					stat.mode = 3;
				}
			}
			// HBlank
			else
			{
				if (stat.mode != 0)
				{
					stat.mode = 0;

					gb::LCDCRegister lcdc;
					lcdc.value = memory[gb::LCDC];

					if (renderer && lcdc.lcd_enable)
					{
						u8 scroll_y = memory[gb::SCY];

						if (lcdc.bg_enable && ((ly >= scroll_y && ly < std::min(144, scroll_y + 144)) || (scroll_y >= 112 && ly < static_cast<u8>(scroll_y + 144))))
						{
							u16 tile_data = lcdc.bg_char_area ? 0x8000 : 0x9000;
							u16 tile_map = lcdc.bg_code_area ? 0x9C00 : 0x9800;

							for (u8 tile_x = 0; tile_x < 20; ++tile_x)
							{
								u16 tile_number = memory[tile_map + (ly / 8) * 32 + tile_x];
								u16 tile_addr = tile_data;

								if (!lcdc.bg_char_area)
								{
									tile_addr += static_cast<s8>(tile_number) * 0x10;
								}
								else
								{
									tile_addr += tile_number * 0x10;
								}

								u16 tile_part_addr = tile_addr + 2 * (ly % 8);
								draw_tile(tile_part_addr, tile_x * 8, ly - scroll_y);
							}
						}

						// Render sprites
						if (lcdc.obj_enable)
						{
							gb::Palette obp_0;
							gb::Palette obp_1;
							obp_0.value = memory[gb::OBP0];
							obp_1.value = memory[gb::OBP1];

							gb::Palette bgp;
							bgp.value = memory[gb::BGP];

							for (u8 sprite_num = 0; sprite_num < 40; ++sprite_num)
							{
								u16 sprite_data = 0xFE00 + sprite_num * 4;
								u8 y = memory[sprite_data] - 16;
								u8 x = memory[sprite_data + 1] - 8;
								u8 pattern = memory[sprite_data + 2];
								gb::SpriteFlags flags;
								flags.value = memory[sprite_data + 3];

								gb::Palette& obj_palette = flags.palette ? obp_1 : obp_0;

								if ((y >= 144 && y <= 248) || (x >= 160 && x <= 248))
								{
									continue;
								}

								for (u8 y_pix = 0; y_pix < 8; ++y_pix)
								{
									u16 tile_addr = 0x8000 + pattern * 0x10 + y_pix * 2;
									u16 tile = (memory[tile_addr + 1] << 8) | memory[tile_addr];

									for (u8 x_pix = 0; x_pix < 8; ++x_pix)
									{
										u8 j = 7 - x_pix;
										u8 colour = (tile >> (7 + j)) & 0x2 | (tile >> j) & 0x1;

										// Sprites don't display the colour 00
										if (colour == 0)
										{
											continue;
										}

										colour = obj_palette[colour];

										u8 actual_x = flags.x_flip ? x + 7 - x_pix : x + x_pix;
										u8 actual_y = flags.y_flip ? y + 7 - y_pix : y + y_pix;

										if (flags.priority)
										{
											if (pixels[(actual_y * 160 * 3) + actual_x * 3] == colour_table[0][0])
											{
												draw_pixel(actual_x, actual_y, colour);
											}
										}
										else
										{
											draw_pixel(actual_x, actual_y, colour);
										}
									}
								}
							}
						}
					}

					// TODO: Horizontal DMA
					// TODO: HBlank interrupt
				}
			}
		}
		// VBlank
		else
		{
			static u8 vblank_clock{};
			vblank_clock += cycle;

			// About to enter VBlank
			if (stat.mode != 1)
			{
				stat.mode = 1;
				ly++;

				// TODO: VBlank STAT interrupt

				// VBlank interrupt
				if_reg.vblank = true;

				if (renderer && !renderer->draw())
				{
					error("Game Boy failed to output a frame.");
					return;
				}

				// Handle VBlank event for debugger, if necessary
				if (vblank_subscribed)
				{
					vblank_occurred = true;

					std::memcpy(last_pixels.data(), pixels.data(), pixels.size());

					// Pause the debugger
					ResetEvent(data_event);
				}

				// Clear the old buffer
				std::memset(pixels.data(), 0, pixels.size());
			}
			// Ongoing VBlank
			else
			{
				if (vblank_clock >= 114)
				{
					vblank_clock -= 114;
					++ly;

					if (ly == 153)
					{
						ly = 0;
						lcd_clock = 0;
					}
					else if (ly == 1)
					{
						error("WTF! ly = 1");
					}
					else
					{
						// TODO: LCY
					}
				}
			}
		}

		memory[gb::STAT] = stat.value;
		memory[gb::LY] = ly;
		memory[gb::IF] = if_reg.value;
	}
}

u8 GameBoy::emulate()
{
	cycle = 0;

	handle_interrupts();

	gb::TACRegister tac;
	tac.value = memory[gb::TAC];

	if (halted)
	{
		if (interrupts || !halted_skip)
		{
			cycle = 1;
		}
		// HALT bug
		else if (halted_skip)
		{
			halted = false;
			halted_skip = false;

			gb::Instruction instruction = gb::decode(memory[regs.PC]);

			// Execute the instruction, but keep the PC same.
			u16 prev_PC = regs.PC;
			(this->*instructions[instruction])();
			regs.PC = prev_PC;
		}
	}
	else
	{
		gb::Instruction instruction = gb::decode(memory[regs.PC]);
		(this->*instructions[instruction])();
	}

	process_lcd();

	static u8 div_counter{};
	div_counter += cycle;

	if (div_counter >= 64)
	{
		div_counter -= 64;
		memory[gb::DIV]++;
	}

	if (tac.enable)
	{
		static u16 tima_counter{};

		tac.value = memory[gb::TAC];
		tima_counter += cycle;

		u16 tima_speed;

		switch (tac.clock)
		{
			case 0b00:
			{
				tima_speed = 256;
				break;
			}

			case 0b01:
			{
				tima_speed = 4;
				break;
			}

			case 0b10:
			{
				tima_speed = 16;
				break;
			}

			case 0b11:
			{
				tima_speed = 64;
				break;
			}
		}

		if (tima_counter >= tima_speed)
		{
		decrement_tima:
			tima_counter -= tima_speed;
			memory[gb::TIMA]++;

			if (memory[gb::TIMA] == 0)
			{
				gb::INTRegister if_reg;
				if_reg.value = memory[gb::IF];
				if_reg.timer = true;
				memory[gb::IF] = if_reg.value;

				memory[gb::TIMA] = memory[gb::TMA];
			}

			// In some cases the TIMA counter may need to get decremented multiple times.
			if (tima_counter >= tima_speed)
			{
				goto decrement_tima;
			}
		}

		//u16 to_interrupt = (0x100 - memory[gb::TIMA]) * 2 * tima_speed - tima_counter * 2;
		//u8 i = 0;
	}

	return cycle;
}

void GameBoy::run()
{
	if (!renderer->initialize())
	{
		error("Failed to initialize the pixel renderer for Game Boy.");
		return;
	}

	while (emulating)
	{
		//auto before = std::chrono::steady_clock::now();

		while (real_cycle < 17555)
		{
			real_cycle += emulate();
		}

		real_cycle = 0;

		//auto after = std::chrono::steady_clock::now();
		//std::this_thread::sleep_for(std::chrono::duration_cast<std::chrono::milliseconds>(16.742ms - (after - before)));
	}
}

void GameBoy::run_debugger()
{
	if (renderer)
	{
		if (!renderer->initialize())
		{
			error("Failed to initialize the pixel renderer for Game Boy.");
			return;
		}

		last_pixels.resize(160 * 144 * 3);
	}

	data_event = CreateEventW(nullptr, true, false, nullptr);

	if (data_event == nullptr)
	{
		error("Failed to create GB debugger event. (%d)", GetLastError());
		exit();
		return;
	}

	// We start paused
	WaitForSingleObject(data_event, INFINITE);

	while (debugging)
	{
		// Normal emulation loop. Runs for a frame.
		while (real_cycle < 17555)
		{
			real_cycle += emulate();
		}

		real_cycle = 0;

		// We only send VBlank signal here to ensure we are actually already paused
		if (vblank_subscribed && vblank_occurred)
		{
			vblank_occurred = false;

			// Send the event notification
			ByteBuffer notification;
			notification.push8(gb::dbg::Message::EVENT);
			notification.push8(gb::dbg::Event::VBLANK);

			dbg::send(notification);
		}

		// Pause for handling debugger stuff, if necessary
		WaitForSingleObject(data_event, INFINITE);
	}

	exit();
}

void GameBoy::handle_message(u8& message, ByteBuffer& buffer)
{
	gb::dbg::Command& command = reinterpret_cast<gb::dbg::Command&>(message);

	switch (command)
	{
		case gb::dbg::Command::RUN:
		{
			SetEvent(data_event);
			break;
		}

		case gb::dbg::Command::PAUSE:
		{
			ResetEvent(data_event);
			break;
		}

		case gb::dbg::Command::SUBSCRIBE:
		{
			gb::dbg::SubscribeCommand command = ByteBuffer::get_structure(buffer);

			switch (command.event)
			{
				case gb::dbg::Event::VBLANK:
				{
					vblank_subscribed = command.subscribe;
					break;
				}
					
				default:
				{
					dbg::send_error(gb::dbg::Error::INVALID_EVENT, "Invalid event received in subscribe command.");
				}
			}

			break;
		}

		case gb::dbg::Command::READ_MEMORY:
		{
			gb::dbg::ReadMemoryCommand command = ByteBuffer::get_structure(buffer);

			if (command.address + command.size > 0xFFFF)
			{
				dbg::send_error(gb::dbg::Error::OUT_OF_BOUNDS_READ, "The requested memory area is out of bounds.");
				break;
			}

			ByteBuffer message;
			message.push8(gb::dbg::Message::MEMORY);
			message.push16(command.size);
			message.resize(message.size() + command.size);
			std::memcpy(message.data() + 3, memory + command.address, command.size);

			dbg::send(message);

			break;
		}

		case gb::dbg::Command::WRITE_MEMORY:
		{
			gb::dbg::WriteMemoryCommand command = ByteBuffer::get_structure(buffer);
			memory[command.address] = command.value;
			break;
		}

		case gb::dbg::Command::SEND_SCREEN_BUFFER:
		{
			ByteBuffer screen_buffer;
			screen_buffer.push8(gb::dbg::Message::SCREEN_BUFFER);
			screen_buffer.resize(screen_buffer.size() + last_pixels.size());
			std::memcpy(screen_buffer.data() + 1, last_pixels.data(), last_pixels.size());

			dbg::send(screen_buffer);

			break;
		}

		case gb::dbg::Command::BUTTON:
		{
			u8& button = buffer.get8();
			bool status = !static_cast<bool>(buffer.get8());

			switch (button)
			{
				case 0:
				{
					p14.right = status;
					break;
				}

				case 1:
				{
					p14.left = status;
					break;
				}

				case 2:
				{
					p14.up = status;
					break;
				}

				case 3:
				{
					p14.down = status;
					break;
				}

				case 4:
				{
					p15.a = status;
					break;
				}

				case 5:
				{
					p15.b = status;
					break;
				}

				case 6:
				{
					p15.select = status;
					break;
				}

				case 7:
				{
					p15.start = status;
					break;
				}

				default:
				{
					dbg::send_error(gb::dbg::Error::INVALID_BUTTON, "Invalid GameBoy button received.");
				}
			}

			break;
		}

		default:
		{
			dbg::send_error(gb::dbg::Error::INVALID_COMMAND, "Invalid debugger command received.");
		}
	}
}

// Common CPU emulation functions
void GameBoy::write_memory(u16 address, u8 value)
{
	if (rom_type != ROMType::ROM)
	{
		if (ram && address <= 0x1FFF)
		{
			if ((value & 0xF) == 0xA)
			{
				ram_banking_enabled = true;
			}
			else
			{
				ram_banking_enabled = false;
			}
		}
		else if (address >= 0x2000 && address <= 0x3FFF)
		{
			rom_bank = value & 0x1F;
		}
		else if (address >= 0x4000 && address <= 0x5FFF)
		{
			bank_bits = value & 0x3;
		}
		else if (address >= 0x6000 && address <= 0x7FFF)
		{
			bank_mode = value & 0x1;
		}
		else if (ram && ram_banking_enabled && address >= 0xA000 && address <= 0xBFFF)
		{
			if (bank_mode == 0)
			{
				ram_banks[0][address - 0xA000] = value;
			}
			else
			{
				ram_banks[bank_bits][address - 0xA000] = value;
			}

			todo("RAM banking probably doesn't work for reads yet.");

			return;
		}

		if (address >= 0x2000 && address <= 0x7FFF)
		{
			u8 ext_rom_bank = (bank_bits << 5) | rom_bank;

			if (ext_rom_bank == 0x20 || ext_rom_bank == 0x40 || ext_rom_bank == 0x60)
			{
				ext_rom_bank++;
			}

			if (bank_mode == 1 || rom_banks.size() < 0x5)
			{
				ext_rom_bank &= 0x1F;
			}

			if (ext_rom_bank >= 2)
			{
				std::memcpy(&memory[0x4000], &rom_banks[ext_rom_bank - 2][0], 0x2000);
			}
		}
	}

	if (address >= 0xE000 && address <= 0xFDFF)
	{
		memory[address - 0x2000] = value;
	}
	else if (address >= 0xC000 && address <= 0xDDFF)
	{
		memory[address + 0x2000] = value;
	}
	else if (address == gb::DEACTIVATE_BOOT_ROM)
	{
		// Load the full executable ROM, which'll replace the boot ROM portion too.
		load_rom(executable.path, 0x0, 0x8000);
		return;
	}
	else if (address == gb::LCDC)
	{
		gb::STATRegister stat;
		stat.value = memory[gb::STAT];

		if (!lcd_enable)
		{
			lcd_enable = value & 0x80;

			if (lcd_enable)
			{
				stat.mode = 2;
			}
		}
		else
		{
			lcd_enable = value & 0x80;

			if (!lcd_enable)
			{
				memory[gb::LY] = 0;
				lcd_clock = 0;
				stat.mode = 0;
			}
		}

		memory[gb::STAT] = stat.value;
	}
	else if (address == gb::IF)
	{
		value |= 0xE0;
	}
	else if (address == gb::JOYPAD)
	{
		value &= 0x30;

		if (value == 0x30)
		{
			value = 0xFF;
		}
		else if (value == 0x20)
		{
			value = p14.value;
		}
		else
		{
			value = p15.value;
		}
	}
	else if (address == gb::DMA)
	{
		u16 dma_src = value << 8;

		for (u16 dma_dest = 0xFE00; dma_dest < 0xFEA0; ++dma_src, ++dma_dest)
		{
			memory[dma_dest] = memory[dma_src];
		}
	}
	else if (address == gb::TIMA)
	{
		//debug("hi");
	}
	else if (address == gb::TAC)
	{
		value |= 0xF8;
	}
	else if (address == gb::SVBK)
	{
		error("RAM banking usage. Unsupported.");
	}
	else if (address <= 0x7FFF)
	{
		// You can't write to the ROM
		return;
	}

	memory[address] = value;
}

void GameBoy::bitwise_xor(u8& value)
{
	regs.A  ^= value;
	regs.F.C = false;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::bitwise_or(u8& value)
{
	regs.A  |= value;
	regs.F.C = false;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::bitwise_and(u8& value)
{
	regs.A  &= value;
	regs.F.C = false;
	regs.F.H = true;
	regs.F.N = false;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::inc(u8& reg)
{
	regs.F.H = (++reg & 0xF) == 0;
	regs.F.N = false;
	regs.F.Z = reg == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::inc(u16& reg)
{
	++reg;
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::dec(u8& reg)
{
	regs.F.H = (reg & 0xF) == 0;
	regs.F.N = true;
	regs.F.Z = --reg == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::dec(u16& reg)
{
	--reg;
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::add(u8& reg)
{
	regs.F.C = static_cast<u16>(regs.A + reg) > 0xFF;
	regs.F.H = (regs.A & 0xF) + (reg & 0xF) > 0xF;
	regs.F.N = false;
	regs.A += reg;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

u16 GameBoy::add(u16 value, u8& op)
{
	regs.F.C = (value & 0xFF) + op > 0xFF;
	regs.F.H = (value & 0xF) + (op & 0xF) > 0xF;
	regs.F.N = false;
	regs.F.Z = false;
	regs.PC += 1;
	cycle += 3;
	return value + static_cast<s8>(op);
}

void GameBoy::add(u16& reg, u16& op)
{
	regs.F.C = static_cast<u32>(reg + op) > 0xFFFF;
	regs.F.H = (reg & 0xFFF) + (op & 0xFFF) > 0xFFF;
	regs.F.N = false;

	reg += op;
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::adc(u8 value)
{
	u8 carry = regs.F.C ? 1 : 0;

	// Set some flags to false initially
	regs.F.C = false;
	regs.F.H = false;

	if (regs.A + carry > 0xFF)
	{
		regs.F.C = true;
	}

	if ((regs.A & 0xF) + carry > 0xF)
	{
		regs.F.H = true;
	}

	regs.A += carry;

	if (regs.A + value > 0xFF)
	{
		regs.F.C = true;
	}

	if ((regs.A & 0xF) + (value & 0xF) > 0xF)
	{
		regs.F.H = true;
	}

	regs.A += value;
	regs.F.N = false;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::sub_flags(u8& value)
{
	regs.F.C = regs.A < value;
	regs.F.H = (regs.A & 0xF) < (value & 0xF);
	regs.F.N = true;
	regs.F.Z = (regs.A - value) == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::sub(u8& value)
{
	sub_flags(value);
	regs.A -= value;
}

void GameBoy::sbc(u8 value)
{
	u8 carry = regs.F.C ? 1 : 0;

	// Set some flags to false initially
	regs.F.C = false;
	regs.F.H = false;

	if (regs.A < carry)
	{
		regs.F.C = true;
	}

	if ((regs.A & 0xF) < carry)
	{
		regs.F.H = true;
	}

	regs.A -= carry;

	if (regs.A < value)
	{
		regs.F.C = true;
	}

	if ((regs.A & 0xF) < (value & 0xF))
	{
		regs.F.H = true;
	}

	regs.A -= value;
	regs.F.N = true;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::load(u8& dest, u8& src)
{
	dest = src;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::load_i8(u8& reg)
{
	reg = memory[++regs.PC];
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::load_i16(u16& reg)
{
	reg  = memory[++regs.PC];
	reg |= memory[++regs.PC] << 8;
	regs.PC += 1;
	cycle += 3;
}

void GameBoy::load_a16(u8& reg)
{
	u16 addr;
	addr  = memory[++regs.PC];
	addr |= memory[++regs.PC] << 8;
	reg   = memory[addr];
	regs.PC += 1;
	cycle += 4;
}

void GameBoy::load_addr(u8& reg, u16 address)
{
	reg = memory[address];
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::write_addr(u16 address, u8& reg)
{
	write_memory(address, reg);
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::compare(u8& value)
{
	sub_flags(value);
}

void GameBoy::jr_s8(bool cond)
{
	regs.PC += 1;
	cycle += 2;

	if (cond)
	{
		regs.PC += static_cast<s8>(memory[regs.PC]);
		cycle += 1;
	}

	regs.PC += 1;
}

void GameBoy::push(u16 value)
{
	memory[--regs.SP] = value >> 8;
	memory[--regs.SP] = value & 0xFF;

	regs.PC += 1;
	cycle += 4;
}

u16 GameBoy::pop()
{
	regs.PC += 1;
	cycle += 3;

	u16 popped_value;
	popped_value  = memory[regs.SP++];
	popped_value |= memory[regs.SP++] << 8;
	return popped_value;
}

void GameBoy::call(bool cond)
{
	cycle += 2;

	if (cond)
	{
		u16 new_pc;
		new_pc  = memory[++regs.PC];
		new_pc |= memory[++regs.PC] << 8;

		push(++regs.PC);
		regs.PC = new_pc;
	}
	else
	{
		regs.PC += 3;
		cycle += 1;
	}
}

void GameBoy::ret(bool cond)
{
	regs.PC += 1;
	cycle += 2;

	if (cond)
	{
		regs.PC = pop();
	}
}

void GameBoy::jump(bool cond)
{
	if (cond)
	{
		u16 new_pc;
		new_pc  = memory[++regs.PC];
		new_pc |= memory[++regs.PC] << 8;
		regs.PC = new_pc;
		cycle += 4;
	}
	else
	{
		regs.PC += 3;
		cycle += 3;
	}
}

void GameBoy::rst(u16 address)
{
	push(++regs.PC);
	regs.PC = address;
}

void GameBoy::invalid()
{
	error("Invalid instruction 0x%X at 0x%X", memory[regs.PC], regs.PC);
	emulating = false;
}

void GameBoy::cb_invalid()
{
	error("Invalid CB instruction 0x%X at 0x%X", memory[regs.PC], regs.PC);
	emulating = false;
}

// CPU instructions
void GameBoy::nop()
{
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::ld_bc_i16()
{
	load_i16(regs.BC);
}

void GameBoy::ld_abc_a()
{
	write_addr(regs.BC, regs.A);
}

void GameBoy::inc_bc()
{
	inc(regs.BC);
}

void GameBoy::inc_b()
{
	inc(regs.B);
}

void GameBoy::dec_b()
{
	dec(regs.B);
}

void GameBoy::ld_b_i8()
{
	load_i8(regs.B);
}

void GameBoy::rlca()
{
	regs.F.C = regs.A & 0x80;
	regs.A <<= 1;
	regs.A |= regs.F.C ? 1 : 0;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::ld_a16_sp()
{
	u16 address;
	address  = memory[++regs.PC];
	address |= memory[++regs.PC] << 8;
	write_memory(address++, regs.SP & 0xFF);
	write_memory(address, regs.SP >> 8);
	regs.PC += 1;
	cycle += 5;
}

void GameBoy::add_hl_bc()
{
	add(regs.HL, regs.BC);
}

void GameBoy::ld_a_abc()
{
	load_addr(regs.A, regs.BC);
}

void GameBoy::dec_bc()
{
	dec(regs.BC);
}

void GameBoy::inc_c()
{
	inc(regs.C);
}

void GameBoy::dec_c()
{
	dec(regs.C);
}

void GameBoy::ld_c_i8()
{
	load_i8(regs.C);
}

void GameBoy::rrca()
{
	regs.F.C = regs.A & 0x1;
	regs.A >>= 1;
	regs.A |= (regs.F.C ? 1 : 0) << 7;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::ld_de_i16()
{
	load_i16(regs.DE);
}

void GameBoy::ld_ade_a()
{
	write_addr(regs.DE, regs.A);
}

void GameBoy::inc_de()
{
	inc(regs.DE);
}

void GameBoy::inc_d()
{
	inc(regs.D);
}

void GameBoy::dec_d()
{
	dec(regs.D);
}

void GameBoy::ld_d_i8()
{
	load_i8(regs.D);
}

void GameBoy::rla()
{
	u8 new_carry = regs.A & 0x80;
	regs.A <<= 1;
	regs.A |= regs.F.C ? 1 : 0;
	regs.F.C = new_carry;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::jr_s8()
{
	jr_s8(true);
}

void GameBoy::add_hl_de()
{
	add(regs.HL, regs.DE);
}

void GameBoy::ld_a_ade()
{
	load(regs.A, memory[regs.DE]);
}

void GameBoy::dec_de()
{
	dec(regs.DE);
}

void GameBoy::inc_e()
{
	inc(regs.E);
}

void GameBoy::dec_e()
{
	dec(regs.E);
}

void GameBoy::ld_e_i8()
{
	load_i8(regs.E);
}

void GameBoy::rra()
{
	u8 new_carry = regs.A & 0x1;
	regs.A >>= 1;
	regs.A |= (regs.F.C ? 1 : 0) << 7;
	regs.F.C = new_carry;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::jr_nz_s8()
{
	jr_s8(!regs.F.Z);
}

void GameBoy::ld_hl_i16()
{
	load_i16(regs.HL);
}

void GameBoy::ld_ahli_a()
{
	write_addr(regs.HL++, regs.A);
}

void GameBoy::inc_hl()
{
	inc(regs.HL);
}

void GameBoy::inc_h()
{
	inc(regs.H);
}

void GameBoy::dec_h()
{
	dec(regs.H);
}

void GameBoy::ld_h_i8()
{
	load_i8(regs.H);
}

void GameBoy::daa()
{
	u16 temp_a = static_cast<u16>(regs.A);

	if (regs.F.N)
	{
		if (regs.F.H)
		{
			temp_a -= 0x6;
			temp_a &= 0xFF;
		}
		
		if (regs.F.C)
		{
			temp_a -= 0x60;
		}
	}
	else
	{
		if (regs.F.H || (temp_a & 0xF) > 0x9)
		{
			temp_a += 0x6;
		}

		if (regs.F.C || temp_a > 0x9F)
		{
			temp_a += 0x60;
		}
	}

	if (temp_a & 0x100)
	{
		regs.F.C = true;
	}

	regs.A = static_cast<u8>(temp_a);
	regs.F.H = false;
	regs.F.Z = regs.A == 0;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::jr_z_s8()
{
	jr_s8(regs.F.Z);
}

void GameBoy::add_hl_hl()
{
	add(regs.HL, regs.HL);
}

void GameBoy::ld_a_ahli()
{
	load_addr(regs.A, regs.HL++);
}

void GameBoy::dec_hl()
{
	dec(regs.HL);
}

void GameBoy::inc_l()
{
	inc(regs.L);
}

void GameBoy::dec_l()
{
	dec(regs.L);
}

void GameBoy::ld_l_i8()
{
	load_i8(regs.L);
}

void GameBoy::cpl()
{
	regs.A = ~regs.A;
	regs.F.H = true;
	regs.F.N = true;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::jr_nc_s8()
{
	jr_s8(!regs.F.C);
}

void GameBoy::ld_sp_i16()
{
	load_i16(regs.SP);
}

void GameBoy::ld_ahld_a()
{
	write_addr(regs.HL--, regs.A);
}

void GameBoy::inc_sp()
{
	inc(regs.SP);
}

void GameBoy::inc_ahl()
{
	inc(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::dec_ahl()
{
	dec(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::ld_ahl_i8()
{
	write_addr(regs.HL, memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::scf()
{
	regs.F.C = true;
	regs.F.H = false;
	regs.F.N = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::jr_c_s8()
{
	jr_s8(regs.F.C);
}

void GameBoy::add_hl_sp()
{
	add(regs.HL, regs.SP);
}

void GameBoy::ld_a_ahld()
{
	load_addr(regs.A, regs.HL--);
}

void GameBoy::dec_sp()
{
	dec(regs.SP);
}

void GameBoy::inc_a()
{
	inc(regs.A);
}

void GameBoy::dec_a()
{
	dec(regs.A);
}

void GameBoy::ld_a_i8()
{
	load_i8(regs.A);
}

void GameBoy::ccf()
{
	regs.F.C = regs.F.C ? false : true;
	regs.F.H = false;
	regs.F.N = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::ld_b_b()
{
	load(regs.B, regs.B);
}

void GameBoy::ld_b_c()
{
	load(regs.B, regs.C);
}

void GameBoy::ld_b_d()
{
	load(regs.B, regs.D);
}

void GameBoy::ld_b_e()
{
	load(regs.B, regs.E);
}

void GameBoy::ld_b_h()
{
	load(regs.B, regs.H);
}

void GameBoy::ld_b_l()
{
	load(regs.B, regs.L);
}

void GameBoy::ld_b_ahl()
{
	load_addr(regs.B, regs.HL);
}

void GameBoy::ld_b_a()
{
	load(regs.B, regs.A);
}

void GameBoy::ld_c_b()
{
	load(regs.C, regs.B);
}

void GameBoy::ld_c_c()
{
	load(regs.C, regs.C);
}

void GameBoy::ld_c_d()
{
	load(regs.C, regs.D);
}

void GameBoy::ld_c_e()
{
	load(regs.C, regs.E);
}

void GameBoy::ld_c_h()
{
	load(regs.C, regs.H);
}

void GameBoy::ld_c_l()
{
	load(regs.C, regs.L);
}

void GameBoy::ld_c_ahl()
{
	load_addr(regs.C, regs.HL);
}

void GameBoy::ld_c_a()
{
	load(regs.C, regs.A);
}

void GameBoy::ld_d_b()
{
	load(regs.D, regs.B);
}

void GameBoy::ld_d_c()
{
	load(regs.D, regs.C);
}

void GameBoy::ld_d_d()
{
	load(regs.D, regs.D);
}

void GameBoy::ld_d_e()
{
	load(regs.D, regs.E);
}

void GameBoy::ld_d_h()
{
	load(regs.D, regs.H);
}

void GameBoy::ld_d_l()
{
	load(regs.D, regs.L);
}

void GameBoy::ld_d_ahl()
{
	load_addr(regs.D, regs.HL);
}

void GameBoy::ld_d_a()
{
	load(regs.D, regs.A);
}

void GameBoy::ld_e_b()
{
	load(regs.E, regs.B);
}

void GameBoy::ld_e_c()
{
	load(regs.E, regs.C);
}

void GameBoy::ld_e_d()
{
	load(regs.E, regs.D);
}

void GameBoy::ld_e_e()
{
	load(regs.E, regs.E);
}

void GameBoy::ld_e_h()
{
	load(regs.E, regs.H);
}

void GameBoy::ld_e_l()
{
	load(regs.E, regs.L);
}

void GameBoy::ld_e_ahl()
{
	load_addr(regs.E, regs.HL);
}

void GameBoy::ld_e_a()
{
	load(regs.E, regs.A);
}

void GameBoy::ld_h_b()
{
	load(regs.H, regs.B);
}

void GameBoy::ld_h_c()
{
	load(regs.H, regs.C);
}

void GameBoy::ld_h_d()
{
	load(regs.H, regs.D);
}

void GameBoy::ld_h_e()
{
	load(regs.H, regs.E);
}

void GameBoy::ld_h_h()
{
	load(regs.H, regs.H);
}

void GameBoy::ld_h_l()
{
	load(regs.H, regs.L);
}

void GameBoy::ld_h_ahl()
{
	load_addr(regs.H, regs.HL);
}

void GameBoy::ld_h_a()
{
	load(regs.H, regs.A);
}

void GameBoy::ld_l_b()
{
	load(regs.L, regs.B);
}

void GameBoy::ld_l_c()
{
	load(regs.L, regs.C);
}

void GameBoy::ld_l_d()
{
	load(regs.L, regs.D);
}

void GameBoy::ld_l_e()
{
	load(regs.L, regs.E);
}

void GameBoy::ld_l_h()
{
	load(regs.L, regs.H);
}

void GameBoy::ld_l_l()
{
	load(regs.L, regs.L);
}

void GameBoy::ld_l_ahl()
{
	load_addr(regs.L, regs.HL);
}

void GameBoy::ld_l_a()
{
	load(regs.L, regs.A);
}

void GameBoy::ld_ahl_b()
{
	write_addr(regs.HL, regs.B);
}

void GameBoy::ld_ahl_c()
{
	write_addr(regs.HL, regs.C);
}

void GameBoy::ld_ahl_d()
{
	write_addr(regs.HL, regs.D);
}

void GameBoy::ld_ahl_e()
{
	write_addr(regs.HL, regs.E);
}

void GameBoy::ld_ahl_h()
{
	write_addr(regs.HL, regs.H);
}

void GameBoy::ld_ahl_l()
{
	write_addr(regs.HL, regs.L);
}

void GameBoy::halt()
{
	halted = true;
	halted_skip = ((memory[gb::IE] & memory[gb::IF] & 0x1F) && !interrupts) ? true : false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::ld_ahl_a()
{
	write_addr(regs.HL, regs.A);
}

void GameBoy::ld_a_b()
{
	load(regs.A, regs.B);
}

void GameBoy::ld_a_c()
{
	load(regs.A, regs.C);
}

void GameBoy::ld_a_d()
{
	load(regs.A, regs.D);
}

void GameBoy::ld_a_e()
{
	load(regs.A, regs.E);
}

void GameBoy::ld_a_h()
{
	load(regs.A, regs.H);
}

void GameBoy::ld_a_l()
{
	load(regs.A, regs.L);
}

void GameBoy::ld_a_ahl()
{
	load_addr(regs.A, regs.HL);
}

void GameBoy::ld_a_a()
{
	load(regs.A, regs.A);
}

void GameBoy::add_b()
{
	add(regs.B);
}

void GameBoy::add_c()
{
	add(regs.C);
}

void GameBoy::add_d()
{
	add(regs.D);
}

void GameBoy::add_e()
{
	add(regs.E);
}

void GameBoy::add_h()
{
	add(regs.H);
}

void GameBoy::add_l()
{
	add(regs.L);
}

void GameBoy::add_ahl()
{
	add(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::add_a()
{
	add(regs.A);
}

void GameBoy::adc_b()
{
	adc(regs.B);
}

void GameBoy::adc_c()
{
	adc(regs.C);
}

void GameBoy::adc_d()
{
	adc(regs.D);
}

void GameBoy::adc_e()
{
	adc(regs.E);
}

void GameBoy::adc_h()
{
	adc(regs.H);
}

void GameBoy::adc_l()
{
	adc(regs.L);
}

void GameBoy::adc_ahl()
{
	adc(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::adc_a()
{
	adc(regs.A);
}

void GameBoy::sub_b()
{
	sub(regs.B);
}

void GameBoy::sub_c()
{
	sub(regs.C);
}

void GameBoy::sub_d()
{
	sub(regs.D);
}

void GameBoy::sub_e()
{
	sub(regs.E);
}

void GameBoy::sub_h()
{
	sub(regs.H);
}

void GameBoy::sub_l()
{
	sub(regs.L);
}

void GameBoy::sub_ahl()
{
	sub(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::sub_a()
{
	sub(regs.A);
}

void GameBoy::sbc_b()
{
	sbc(regs.B);
}

void GameBoy::sbc_c()
{
	sbc(regs.C);
}

void GameBoy::sbc_d()
{
	sbc(regs.D);
}

void GameBoy::sbc_e()
{
	sbc(regs.E);
}

void GameBoy::sbc_h()
{
	sbc(regs.H);
}

void GameBoy::sbc_l()
{
	sbc(regs.L);
}

void GameBoy::sbc_ahl()
{
	sbc(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::sbc_a()
{
	sbc(regs.A);
}

void GameBoy::and_b()
{
	bitwise_and(regs.B);
}

void GameBoy::and_c()
{
	bitwise_and(regs.C);
}

void GameBoy::and_d()
{
	bitwise_and(regs.D);
}

void GameBoy::and_e()
{
	bitwise_and(regs.E);
}

void GameBoy::and_h()
{
	bitwise_and(regs.H);
}

void GameBoy::and_l()
{
	bitwise_and(regs.L);
}

void GameBoy::and_ahl()
{
	bitwise_and(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::and_a()
{
	bitwise_and(regs.A);
}

void GameBoy::xor_b()
{
	bitwise_xor(regs.B);
}

void GameBoy::xor_c()
{
	bitwise_xor(regs.C);
}

void GameBoy::xor_d()
{
	bitwise_xor(regs.D);
}

void GameBoy::xor_e()
{
	bitwise_xor(regs.E);
}

void GameBoy::xor_h()
{
	bitwise_xor(regs.H);
}

void GameBoy::xor_l()
{
	bitwise_xor(regs.L);
}

void GameBoy::xor_ahl()
{
	bitwise_xor(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::xor_a()
{
	bitwise_xor(regs.A);
}

void GameBoy::or_b()
{
	bitwise_or(regs.B);
}

void GameBoy::or_c()
{
	bitwise_or(regs.C);
}

void GameBoy::or_d()
{
	bitwise_or(regs.D);
}

void GameBoy::or_e()
{
	bitwise_or(regs.E);
}

void GameBoy::or_h()
{
	bitwise_or(regs.H);
}

void GameBoy::or_l()
{
	bitwise_or(regs.L);
}

void GameBoy::or_ahl()
{
	bitwise_or(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::or_a()
{
	bitwise_or(regs.A);
}

void GameBoy::cp_b()
{
	compare(regs.B);
}

void GameBoy::cp_c()
{
	compare(regs.C);
}

void GameBoy::cp_d()
{
	compare(regs.D);
}

void GameBoy::cp_e()
{
	compare(regs.E);
}

void GameBoy::cp_h()
{
	compare(regs.H);
}

void GameBoy::cp_l()
{
	compare(regs.L);
}

void GameBoy::cp_ahl()
{
	compare(memory[regs.HL]);
	cycle += 1;
}

void GameBoy::cp_a()
{
	compare(regs.A);
}

void GameBoy::ret_nz()
{
	ret(!regs.F.Z);
}

void GameBoy::pop_bc()
{
	regs.BC = pop();
}

void GameBoy::jp_nz_a16()
{
	jump(!regs.F.Z);
}

void GameBoy::jp_a16()
{
	jump(true);
}

void GameBoy::call_nz_a16()
{
	call(!regs.F.Z);
}

void GameBoy::push_bc()
{
	push(regs.BC);
}

void GameBoy::add_i8()
{
	add(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_00()
{
	rst(0x0);
}

void GameBoy::ret_z()
{
	ret(regs.F.Z);
}

void GameBoy::ret()
{
	ret(true);
	cycle -= 1;
}

void GameBoy::jp_z_a16()
{
	jump(regs.F.Z);
}

void GameBoy::prefix_cb()
{
	// Since all CB instructions are 2 bytes long, we can increment the PC once before
	// executing the instruction and once after (this helps break dependency chains).
	gb::CBInstruction cb_instruction = gb::cb_decode(memory[++regs.PC]);
	(this->*cb_instructions[cb_instruction])();

	//debug("CB instruction: 0x%X", cb_instruction);

	regs.PC += 1;
}

void GameBoy::call_z_a16()
{
	call(regs.F.Z);
}

void GameBoy::call_a16()
{
	call(true);
}

void GameBoy::adc_i8()
{
	adc(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_08()
{
	rst(0x8);
}

void GameBoy::ret_nc()
{
	ret(!regs.F.C);
}

void GameBoy::pop_de()
{
	regs.DE = pop();
}

void GameBoy::jp_nc_a16()
{
	jump(!regs.F.C);
}

void GameBoy::call_nc_a16()
{
	call(!regs.F.C);
}

void GameBoy::push_de()
{
	push(regs.DE);
}

void GameBoy::sub_i8()
{
	sub(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_10()
{
	rst(0x10);
}

void GameBoy::ret_c()
{
	ret(regs.F.C);
}

void GameBoy::reti()
{
	ret();
	interrupts = true;
}

void GameBoy::jp_c_a16()
{
	jump(regs.F.C);
}

void GameBoy::call_c_a16()
{
	call(regs.F.C);
}

void GameBoy::sbc_i8()
{
	sbc(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_18()
{
	rst(0x18);
}

void GameBoy::ldh_a8_a()
{
	write_addr(0xFF00 + memory[++regs.PC], regs.A);
	cycle += 1;
}

void GameBoy::pop_hl()
{
	regs.HL = pop();
}

void GameBoy::ld_ac_a()
{
	write_addr(0xFF00 + regs.C, regs.A);
}

void GameBoy::push_hl()
{
	push(regs.HL);
}

void GameBoy::and_i8()
{
	bitwise_and(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_20()
{
	rst(0x20);
}

void GameBoy::add_sp_s8()
{
	regs.SP = add(regs.SP, memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::jp_hl()
{
	regs.PC = regs.HL;
	cycle += 1;
}

void GameBoy::ld_a16_a()
{
	u16 address;
	address  = memory[++regs.PC];
	address |= memory[++regs.PC] << 8;
	write_memory(address, regs.A);

	regs.PC += 1;
	cycle += 4;
}

void GameBoy::xor_i8()
{
	bitwise_xor(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_28()
{
	rst(0x28);
}

void GameBoy::ldh_a_a8()
{
	load_addr(regs.A, 0xFF00 + memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::pop_af()
{
	// The unused bits of F always remain 0.
	regs.AF = pop() & 0xFFF0;
}

void GameBoy::ld_a_ac()
{
	load_addr(regs.A, 0xFF00 + regs.C);
}

void GameBoy::di()
{
	interrupts = false;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::push_af()
{
	push(regs.AF);
}

void GameBoy::or_i8()
{
	bitwise_or(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_30()
{
	rst(0x30);
}

void GameBoy::ld_hl_sp_s8()
{
	regs.HL = add(regs.SP, memory[++regs.PC]);
}

void GameBoy::ld_sp_hl()
{
	regs.SP = regs.HL;
	regs.PC += 1;
	cycle += 2;
}

void GameBoy::ld_a_a16()
{
	load_a16(regs.A);
}

void GameBoy::ei()
{
	interrupt_delay = true;
	regs.PC += 1;
	cycle += 1;
}

void GameBoy::cp_i8()
{
	compare(memory[++regs.PC]);
	cycle += 1;
}

void GameBoy::rst_38()
{
	rst(0x38);
}

// CB prefix instructions
void GameBoy::rlc(u8& reg)
{
	regs.F.C = reg & (1 << 7);

	reg <<= 1;
	reg |= regs.F.C ? 1 : 0;

	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
}

void GameBoy::rrc(u8& reg)
{
	regs.F.C = reg & 0x1;

	reg >>= 1;
	reg |= (regs.F.C ? 1 : 0) << 7;

	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
}

void GameBoy::rl(u8& reg)
{
	u8 new_carry = reg & (1 << 7);

	reg <<= 1;
	reg |= regs.F.C ? 1 : 0;

	regs.F.C = new_carry;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
	cycle += 2;
}

void GameBoy::rr(u8& reg)
{
	u8 new_carry = reg & 0x1;

	reg >>= 1;
	reg |= (regs.F.C ? 1 : 0) << 7;

	regs.F.C = new_carry;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
	cycle += 2;
}

void GameBoy::sla(u8& reg)
{
	regs.F.C = reg & (1 << 7);

	reg <<= 1;

	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
}

void GameBoy::sra(u8& reg)
{
	regs.F.C = reg & (1 << 0);

	reg >>= 1;
	reg |= (reg & (1 << 6)) << 1;

	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
}

void GameBoy::swap(u8& value)
{
	u8 top = value & 0xF0;
	u8 bottom = value & 0x0F;

	value = top >> 4 | bottom << 4;

	regs.F.C = false;
	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = value == 0;
	cycle += 2;
}

void GameBoy::srl(u8& reg)
{
	regs.F.C = reg & 0x1;

	reg >>= 1;

	regs.F.H = false;
	regs.F.N = false;
	regs.F.Z = reg == 0;
	cycle += 2;
}

void GameBoy::bit(u8 bit, u8& reg)
{
	regs.F.H = true;
	regs.F.N = false;
	regs.F.Z = !(reg & (1 << bit));
	cycle += 2;
}

void GameBoy::res(u8 bit, u8& reg)
{
	reg &= ~(1 << bit);
	cycle += 2;
}

void GameBoy::set(u8 bit, u8& reg)
{
	reg |= 1 << bit;
	cycle += 2;
}

void GameBoy::rlc_b()
{
	rlc(regs.B);
}

void GameBoy::rlc_c()
{
	rlc(regs.C);
}

void GameBoy::rlc_d()
{
	rlc(regs.D);
}

void GameBoy::rlc_e()
{
	rlc(regs.E);
}

void GameBoy::rlc_h()
{
	rlc(regs.H);
}

void GameBoy::rlc_l()
{
	rlc(regs.L);
}

void GameBoy::rlc_ahl()
{
	rlc(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::rlc_a()
{
	rlc(regs.A);
}

void GameBoy::rrc_b()
{
	rrc(regs.B);
}

void GameBoy::rrc_c()
{
	rrc(regs.C);
}

void GameBoy::rrc_d()
{
	rrc(regs.D);
}

void GameBoy::rrc_e()
{
	rrc(regs.E);
}

void GameBoy::rrc_h()
{
	rrc(regs.H);
}

void GameBoy::rrc_l()
{
	rrc(regs.L);
}

void GameBoy::rrc_ahl()
{
	rrc(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::rrc_a()
{
	rrc(regs.A);
}

void GameBoy::rl_b()
{
	rl(regs.B);
}

void GameBoy::rl_c()
{
	rl(regs.C);
}

void GameBoy::rl_d()
{
	rl(regs.D);
}

void GameBoy::rl_e()
{
	rl(regs.E);
}

void GameBoy::rl_h()
{
	rl(regs.H);
}

void GameBoy::rl_l()
{
	rl(regs.L);
}

void GameBoy::rl_ahl()
{
	rl(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::rl_a()
{
	rl(regs.A);
}

void GameBoy::rr_b()
{
	rr(regs.B);
}

void GameBoy::rr_c()
{
	rr(regs.C);
}

void GameBoy::rr_d()
{
	rr(regs.D);
}

void GameBoy::rr_e()
{
	rr(regs.E);
}

void GameBoy::rr_h()
{
	rr(regs.H);
}

void GameBoy::rr_l()
{
	rr(regs.L);
}

void GameBoy::rr_ahl()
{
	rr(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::rr_a()
{
	rr(regs.A);
}

void GameBoy::sla_b()
{
	sla(regs.B);
}

void GameBoy::sla_c()
{
	sla(regs.C);
}

void GameBoy::sla_d()
{
	sla(regs.D);
}

void GameBoy::sla_e()
{
	sla(regs.E);
}

void GameBoy::sla_h()
{
	sla(regs.H);
}

void GameBoy::sla_l()
{
	sla(regs.L);
}

void GameBoy::sla_ahl()
{
	sla(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::sla_a()
{
	sla(regs.A);
}

void GameBoy::sra_b()
{
	sra(regs.B);
}

void GameBoy::sra_c()
{
	sra(regs.C);
}

void GameBoy::sra_d()
{
	sra(regs.D);
}

void GameBoy::sra_e()
{
	sra(regs.E);
}

void GameBoy::sra_h()
{
	sra(regs.H);
}

void GameBoy::sra_l()
{
	sra(regs.L);
}

void GameBoy::sra_ahl()
{
	sra(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::sra_a()
{
	sra(regs.A);
}

void GameBoy::swap_b()
{
	swap(regs.B);
}

void GameBoy::swap_c()
{
	swap(regs.C);
}

void GameBoy::swap_d()
{
	swap(regs.D);
}

void GameBoy::swap_e()
{
	swap(regs.E);
}

void GameBoy::swap_h()
{
	swap(regs.H);
}

void GameBoy::swap_l()
{
	swap(regs.L);
}

void GameBoy::swap_ahl()
{
	swap(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::swap_a()
{
	swap(regs.A);
}

void GameBoy::srl_b()
{
	srl(regs.B);
}

void GameBoy::srl_c()
{
	srl(regs.C);
}

void GameBoy::srl_d()
{
	srl(regs.D);
}

void GameBoy::srl_e()
{
	srl(regs.E);
}

void GameBoy::srl_h()
{
	srl(regs.H);
}

void GameBoy::srl_l()
{
	srl(regs.L);
}

void GameBoy::srl_ahl()
{
	srl(memory[regs.HL]);
	cycle += 2;
}

void GameBoy::srl_a()
{
	srl(regs.A);
}

void GameBoy::bit_0_b()
{
	bit(0, regs.B);
}

void GameBoy::bit_0_c()
{
	bit(0, regs.C);
}

void GameBoy::bit_0_d()
{
	bit(0, regs.D);
}

void GameBoy::bit_0_e()
{
	bit(0, regs.E);
}

void GameBoy::bit_0_h()
{
	bit(0, regs.H);
}

void GameBoy::bit_0_l()
{
	bit(0, regs.L);
}

void GameBoy::bit_0_ahl()
{
	bit(0, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_0_a()
{
	bit(0, regs.A);
}

void GameBoy::bit_1_b()
{
	bit(1, regs.B);
}

void GameBoy::bit_1_c()
{
	bit(1, regs.C);
}

void GameBoy::bit_1_d()
{
	bit(1, regs.D);
}

void GameBoy::bit_1_e()
{
	bit(1, regs.E);
}

void GameBoy::bit_1_h()
{
	bit(1, regs.H);
}

void GameBoy::bit_1_l()
{
	bit(1, regs.L);
}

void GameBoy::bit_1_ahl()
{
	bit(1, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_1_a()
{
	bit(1, regs.A);
}

void GameBoy::bit_2_b()
{
	bit(2, regs.B);
}

void GameBoy::bit_2_c()
{
	bit(2, regs.C);
}

void GameBoy::bit_2_d()
{
	bit(2, regs.D);
}

void GameBoy::bit_2_e()
{
	bit(2, regs.E);
}

void GameBoy::bit_2_h()
{
	bit(2, regs.H);
}

void GameBoy::bit_2_l()
{
	bit(2, regs.L);
}

void GameBoy::bit_2_ahl()
{
	bit(2, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_2_a()
{
	bit(2, regs.A);
}

void GameBoy::bit_3_b()
{
	bit(3, regs.B);
}

void GameBoy::bit_3_c()
{
	bit(3, regs.C);
}

void GameBoy::bit_3_d()
{
	bit(3, regs.D);
}

void GameBoy::bit_3_e()
{
	bit(3, regs.E);
}

void GameBoy::bit_3_h()
{
	bit(3, regs.H);
}

void GameBoy::bit_3_l()
{
	bit(3, regs.L);
}

void GameBoy::bit_3_ahl()
{
	bit(3, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_3_a()
{
	bit(3, regs.A);
}

void GameBoy::bit_4_b()
{
	bit(4, regs.B);
}

void GameBoy::bit_4_c()
{
	bit(4, regs.C);
}

void GameBoy::bit_4_d()
{
	bit(4, regs.D);
}

void GameBoy::bit_4_e()
{
	bit(4, regs.E);
}

void GameBoy::bit_4_h()
{
	bit(4, regs.H);
}

void GameBoy::bit_4_l()
{
	bit(4, regs.L);
}

void GameBoy::bit_4_ahl()
{
	bit(4, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_4_a()
{
	bit(4, regs.A);
}

void GameBoy::bit_5_b()
{
	bit(5, regs.B);
}

void GameBoy::bit_5_c()
{
	bit(5, regs.C);
}

void GameBoy::bit_5_d()
{
	bit(5, regs.D);
}

void GameBoy::bit_5_e()
{
	bit(5, regs.E);
}

void GameBoy::bit_5_h()
{
	bit(5, regs.H);
}

void GameBoy::bit_5_l()
{
	bit(5, regs.L);
}

void GameBoy::bit_5_ahl()
{
	bit(5, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_5_a()
{
	bit(5, regs.A);
}

void GameBoy::bit_6_b()
{
	bit(6, regs.B);
}

void GameBoy::bit_6_c()
{
	bit(6, regs.C);
}

void GameBoy::bit_6_d()
{
	bit(6, regs.D);
}

void GameBoy::bit_6_e()
{
	bit(6, regs.E);
}

void GameBoy::bit_6_h()
{
	bit(6, regs.H);
}

void GameBoy::bit_6_l()
{
	bit(6, regs.L);
}

void GameBoy::bit_6_ahl()
{
	bit(6, memory[regs.HL]);
}

void GameBoy::bit_6_a()
{
	bit(6, regs.A);
}

void GameBoy::bit_7_b()
{
	bit(7, regs.B);
}

void GameBoy::bit_7_c()
{
	bit(7, regs.C);
}

void GameBoy::bit_7_d()
{
	bit(7, regs.D);
}

void GameBoy::bit_7_e()
{
	bit(7, regs.E);
}

void GameBoy::bit_7_h()
{
	bit(7, regs.H);
}

void GameBoy::bit_7_l()
{
	bit(7, regs.L);
}

void GameBoy::bit_7_ahl()
{
	bit(7, memory[regs.HL]);
	cycle += 1;
}

void GameBoy::bit_7_a()
{
	bit(7, regs.A);
}

void GameBoy::res_0_b()
{
	res(0, regs.B);
}

void GameBoy::res_0_c()
{
	res(0, regs.C);
}

void GameBoy::res_0_d()
{
	res(0, regs.D);
}

void GameBoy::res_0_e()
{
	res(0, regs.E);
}

void GameBoy::res_0_h()
{
	res(0, regs.H);
}

void GameBoy::res_0_l()
{
	res(0, regs.L);
}

void GameBoy::res_0_ahl()
{
	res(0, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_0_a()
{
	res(0, regs.A);
}

void GameBoy::res_1_b()
{
	res(1, regs.B);
}

void GameBoy::res_1_c()
{
	res(1, regs.C);
}

void GameBoy::res_1_d()
{
	res(1, regs.D);
}

void GameBoy::res_1_e()
{
	res(1, regs.E);
}

void GameBoy::res_1_h()
{
	res(1, regs.H);
}

void GameBoy::res_1_l()
{
	res(1, regs.L);
}

void GameBoy::res_1_ahl()
{
	res(1, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_1_a()
{
	res(1, regs.A);
}

void GameBoy::res_2_b()
{
	res(2, regs.B);
}

void GameBoy::res_2_c()
{
	res(2, regs.C);
}

void GameBoy::res_2_d()
{
	res(2, regs.D);
}

void GameBoy::res_2_e()
{
	res(2, regs.E);
}

void GameBoy::res_2_h()
{
	res(2, regs.H);
}

void GameBoy::res_2_l()
{
	res(2, regs.L);
}

void GameBoy::res_2_ahl()
{
	res(2, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_2_a()
{
	res(2, regs.A);
}

void GameBoy::res_3_b()
{
	res(3, regs.B);
}

void GameBoy::res_3_c()
{
	res(3, regs.C);
}

void GameBoy::res_3_d()
{
	res(3, regs.D);
}

void GameBoy::res_3_e()
{
	res(3, regs.E);
}

void GameBoy::res_3_h()
{
	res(3, regs.H);
}

void GameBoy::res_3_l()
{
	res(3, regs.L);
}

void GameBoy::res_3_ahl()
{
	res(3, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_3_a()
{
	res(3, regs.A);
}

void GameBoy::res_4_b()
{
	res(4, regs.B);
}

void GameBoy::res_4_c()
{
	res(4, regs.C);
}

void GameBoy::res_4_d()
{
	res(4, regs.D);
}

void GameBoy::res_4_e()
{
	res(4, regs.E);
}

void GameBoy::res_4_h()
{
	res(4, regs.H);
}

void GameBoy::res_4_l()
{
	res(4, regs.L);
}

void GameBoy::res_4_ahl()
{
	res(4, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_4_a()
{
	res(4, regs.A);
}

void GameBoy::res_5_b()
{
	res(5, regs.B);
}

void GameBoy::res_5_c()
{
	res(5, regs.C);
}

void GameBoy::res_5_d()
{
	res(5, regs.D);
}

void GameBoy::res_5_e()
{
	res(5, regs.E);
}

void GameBoy::res_5_h()
{
	res(5, regs.H);
}

void GameBoy::res_5_l()
{
	res(5, regs.L);
}

void GameBoy::res_5_ahl()
{
	res(5, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_5_a()
{
	res(5, regs.A);
}

void GameBoy::res_6_b()
{
	res(6, regs.B);
}

void GameBoy::res_6_c()
{
	res(6, regs.C);
}

void GameBoy::res_6_d()
{
	res(6, regs.D);
}

void GameBoy::res_6_e()
{
	res(6, regs.E);
}

void GameBoy::res_6_h()
{
	res(6, regs.H);
}

void GameBoy::res_6_l()
{
	res(6, regs.L);
}

void GameBoy::res_6_ahl()
{
	res(6, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_6_a()
{
	res(6, regs.A);
}

void GameBoy::res_7_b()
{
	res(7, regs.B);
}

void GameBoy::res_7_c()
{
	res(7, regs.C);
}

void GameBoy::res_7_d()
{
	res(7, regs.D);
}

void GameBoy::res_7_e()
{
	res(7, regs.E);
}

void GameBoy::res_7_h()
{
	res(7, regs.H);
}

void GameBoy::res_7_l()
{
	res(7, regs.L);
}

void GameBoy::res_7_ahl()
{
	res(7, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::res_7_a()
{
	res(7, regs.A);
}

void GameBoy::set_0_b()
{
	set(0, regs.B);
}

void GameBoy::set_0_c()
{
	set(0, regs.C);
}

void GameBoy::set_0_d()
{
	set(0, regs.D);
}

void GameBoy::set_0_e()
{
	set(0, regs.E);
}

void GameBoy::set_0_h()
{
	set(0, regs.H);
}

void GameBoy::set_0_l()
{
	set(0, regs.L);
}

void GameBoy::set_0_ahl()
{
	set(0, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_0_a()
{
	set(0, regs.A);
}

void GameBoy::set_1_b()
{
	set(1, regs.B);
}

void GameBoy::set_1_c()
{
	set(1, regs.C);
}

void GameBoy::set_1_d()
{
	set(1, regs.D);
}

void GameBoy::set_1_e()
{
	set(1, regs.E);
}

void GameBoy::set_1_h()
{
	set(1, regs.H);
}

void GameBoy::set_1_l()
{
	set(1, regs.L);
}

void GameBoy::set_1_ahl()
{
	set(1, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_1_a()
{
	set(1, regs.A);
}

void GameBoy::set_2_b()
{
	set(2, regs.B);
}

void GameBoy::set_2_c()
{
	set(2, regs.C);
}

void GameBoy::set_2_d()
{
	set(2, regs.D);
}

void GameBoy::set_2_e()
{
	set(2, regs.E);
}

void GameBoy::set_2_h()
{
	set(2, regs.H);
}

void GameBoy::set_2_l()
{
	set(2, regs.L);
}

void GameBoy::set_2_ahl()
{
	set(2, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_2_a()
{
	set(2, regs.A);
}

void GameBoy::set_3_b()
{
	set(3, regs.B);
}

void GameBoy::set_3_c()
{
	set(3, regs.C);
}

void GameBoy::set_3_d()
{
	set(3, regs.D);
}

void GameBoy::set_3_e()
{
	set(3, regs.E);
}

void GameBoy::set_3_h()
{
	set(3, regs.H);
}

void GameBoy::set_3_l()
{
	set(3, regs.L);
}

void GameBoy::set_3_ahl()
{
	set(3, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_3_a()
{
	set(3, regs.A);
}

void GameBoy::set_4_b()
{
	set(4, regs.B);
}

void GameBoy::set_4_c()
{
	set(4, regs.C);
}

void GameBoy::set_4_d()
{
	set(4, regs.D);
}

void GameBoy::set_4_e()
{
	set(4, regs.E);
}

void GameBoy::set_4_h()
{
	set(4, regs.H);
}

void GameBoy::set_4_l()
{
	set(4, regs.L);
}

void GameBoy::set_4_ahl()
{
	set(4, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_4_a()
{
	set(4, regs.A);
}

void GameBoy::set_5_b()
{
	set(5, regs.B);
}

void GameBoy::set_5_c()
{
	set(5, regs.C);
}

void GameBoy::set_5_d()
{
	set(5, regs.D);
}

void GameBoy::set_5_e()
{
	set(5, regs.E);
}

void GameBoy::set_5_h()
{
	set(5, regs.H);
}

void GameBoy::set_5_l()
{
	set(5, regs.L);
}

void GameBoy::set_5_ahl()
{
	set(5, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_5_a()
{
	set(5, regs.A);
}

void GameBoy::set_6_b()
{
	set(6, regs.B);
}

void GameBoy::set_6_c()
{
	set(6, regs.C);
}

void GameBoy::set_6_d()
{
	set(6, regs.D);
}

void GameBoy::set_6_e()
{
	set(6, regs.E);
}

void GameBoy::set_6_h()
{
	set(6, regs.H);
}

void GameBoy::set_6_l()
{
	set(6, regs.L);
}

void GameBoy::set_6_ahl()
{
	set(6, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_6_a()
{
	set(6, regs.A);
}

void GameBoy::set_7_b()
{
	set(7, regs.B);
}

void GameBoy::set_7_c()
{
	set(7, regs.C);
}

void GameBoy::set_7_d()
{
	set(7, regs.D);
}

void GameBoy::set_7_e()
{
	set(7, regs.E);
}

void GameBoy::set_7_h()
{
	set(7, regs.H);
}

void GameBoy::set_7_l()
{
	set(7, regs.L);
}

void GameBoy::set_7_ahl()
{
	set(7, memory[regs.HL]);
	cycle += 2;
}

void GameBoy::set_7_a()
{
	set(7, regs.A);
}