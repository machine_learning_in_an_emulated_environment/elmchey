#include "stdafx.h"
#include "elmchey.h"

#include "debug/debug.h"
#include "gui/emu_frame.h"

#include "chip8.h"
#include "game_boy.h"

// Definitions for globals
std::unique_ptr<Emulator> emu;
Elmchey elmchey;

bool Elmchey::initialize()
{
#if defined(DEBUG) && defined(_MSC_VER)
	// wxWidgets allocates some global and static objects by default on the heap.
	// Thus we simply disable the heap leak detection to prevent console spam.
	_CrtSetDbgFlag(0);
#endif

	// Initialize logging
	if (!initialize_logging("Elmchey.log"))
	{
		error("Failed to initialize logging! (Ironically we use the logging system to output this)");
		return false;
	}
	
	// Load the global configuration
	if (!cfg::load_global())
	{
		// TODO: Probably needs a dialog box or something to notify the user of this.
		error("Failed to load the configuration. The configuration may be loaded partially. Defaults will be used for values that were unable to be loaded.\n"
		      "Please check the log file for more information.");
	}

	if (cfg::developer.enable_debugging)
	{
		if (!dbg::initialize())
		{
			error("Failed to initialize debugging.");
			return false;
		}
	}
	else
	{
		// Create the main window
		frame = new EmuFrame();
	}

//#ifdef DEBUG
	// This allows quick development and testing of games (the game is run right after startup)
	Executable executable;
	executable.path = fs::current_path() / "Game Boy" / "dr_mario.gb";
	executable.name = "Dr. Mario";
	executable.system = System::GAME_BOY;
	run(executable);
//#endif

	return true;
}

s32 Elmchey::on_exit()
{
	if (!cfg::save())
	{
		error("Failed to save the config.\n"
		      "Please check the log file for more information.");
	}

	return 0;
}

bool Elmchey::run(Executable& executable)
{
	// Pust the current configuration onto the stack, so we can re-load it later.
	cfg::push();

	// Load the custom configuration, if available
	bool using_custom_config = cfg::load_custom(executable);

	// Create the instance for the correct emulator core
	switch (executable.system)
	{
		case System::CHIP8:
		{
			emu = std::make_unique<CHIP8>(executable);
			break;
		}

		case System::GAME_BOY:
		{
			emu = std::make_unique<GameBoy>(executable);
			break;
		}

		default:
		{
			error("Unknown/unavailable emulator core. (%d)", executable.system);
			return false;
		}
	}

	if (!emu->initialize())
	{
		error("Failed to initialize the emulator core for %s.", system_to_string(executable.system));

		// Make sure the emulator instance is destroyed
		emu.reset();

		return false;
	}

	// No need to manipulate the GUI in debug mode
	if (!cfg::developer.enable_debugging)
	{
		if (using_custom_config)
		{
			// Disable the global settings menu
			// Note that context menu will only be available for the currently executing executable.
			frame->enable_settings(false);
		}
		else
		{
			// The global settings menu will remain available, but the context menus for custom settings will be disabled.
			frame->enable_context_menus(false);
		}
	}

	emu->start();

	return true;
}

void Elmchey::emulator_exited()
{
	// Re-enable the settings menu and the context menus (except in the debug mode)
	if (!cfg::developer.enable_debugging)
	{
		frame->enable_settings();
		frame->enable_context_menus();
	}
}

// This is here so we can access the Elmchey instance.
Emulator::~Emulator()
{
	if (emulation_thread.joinable())
	{
		emulation_thread.join();
	}

	// Go back to the last used configuration
	cfg::pop();

	// Notify Elmchey that the current emulator instance has shut down
	elmchey.emulator_exited();
}

// The platform-specific initialization implementation classes
#ifdef wxWidgets
	class wxElmchey : public wxApp
	{
	public:
		bool OnInit()
		{
			// wxApp::OnInit handles some default wxWidgets parameters
			if (!wxApp::OnInit())
			{
				return false;
			}

			return elmchey.initialize();
		}

		s32 OnExit()
		{
			return elmchey.on_exit();
		}
	};

	// Implement our wxWidgets app so it can be ran.
	// For debug mode we implement it as a console app, to allow easier debugging through the console. For release only the GUI will be visible.
	#ifdef DEBUG
		wxIMPLEMENT_APP_CONSOLE(wxElmchey);
	#else
		wxIMPLEMENT_APP(wxElmchey);
	#endif

	void Emulator::exit()
	{
		if (cfg::developer.enable_debugging)
		{
			todo("Rewrite to WinAPI, so we can close after debugging ends.");
		}
		else
		{
			emu.reset();
		}
	}
#else
	#error Unsupported platform
#endif