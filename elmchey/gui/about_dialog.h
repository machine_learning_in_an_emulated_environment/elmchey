﻿#pragma once

enum AboutIDs
{
	BUTTON_LICENCES = 10,
};

class AboutDialog : private wxDialog
{
public:
	AboutDialog(wxWindow* parent);
	
private:
	void close(wxCommandEvent& event);
	void licences(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class LicenceDialog : private wxDialog
{
public:
	LicenceDialog(wxWindow* parent);

private:
	void close(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class LicencePage : public wxPanel
{
public:
	LicencePage(wxWindow* parent, std::string& text, s32 wrap_width) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(wrap_width, -1))
	{
		new wxTextCtrl(this, wxID_ANY, wxString::FromUTF8(text), wxDefaultPosition, wxSize(wrap_width, -1), wxTE_MULTILINE | wxTE_READONLY | wxTE_RICH2);
	}
};