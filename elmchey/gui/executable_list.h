#pragma once

#include <wx/listctrl.h>

enum : s32
{
	EXECUTABLE_LIST = 20,
	CUSTOM_SETTINGS = 21,
};

class ExecutableList : private wxListCtrl
{
public:
	ExecutableList(wxWindow* parent);
	void update();

	// The id of the executable for which the context menu should be shown.
	// -1 if all context menus should be shown.
	// -2 if no context menus should be shown.
	s32 current_executable_id;

private:
	void insert_executable(Executable& executable);
	void run_executable(wxListEvent& event);

	// Creates the context menu when an item is right clicked.
	void context_menu(wxListEvent& event);

	// Called when the settings context menu item is clicked on an executable.
	void custom_settings(wxCommandEvent& event);

	std::vector<Executable> executables;

	// General executable loaders
	void CHIP8_loader(fs::path& path);
	void GameBoy_loader(fs::path& path);

	wxDECLARE_EVENT_TABLE();
};