#include "stdafx.h"
#include "emu_frame.h"

wxBEGIN_EVENT_TABLE(EmuFrame, wxFrame)
	EVT_MENU(RUN, EmuFrame::run)
	EVT_MENU(INSTALL, EmuFrame::install)
	EVT_MENU(SETTINGS, EmuFrame::settings)
	EVT_MENU(ABOUT, EmuFrame::about)
	EVT_CLOSE(EmuFrame::close)
wxEND_EVENT_TABLE()

EmuFrame::EmuFrame() : wxFrame(nullptr, wxID_ANY, "Elmchey")
{
	wxMenu* menu_file = new wxMenu();
	menu_file->Append(RUN, "&Run");
	menu_file->Append(INSTALL, "&Install");

	wxMenu* menu_options = new wxMenu();
	menu_options->Append(SETTINGS, "&Settings");

	wxMenu* menu_help = new wxMenu();
	menu_help->Append(ABOUT, "&About");

	menu_bar = new wxMenuBar();
	menu_bar->Append(menu_file, "&File");
	menu_bar->Append(menu_options, "&Options");
	menu_bar->Append(menu_help, "&Help");
	SetMenuBar(menu_bar);

	executable_list = new ExecutableList(this);
	executable_list->update();

	// Set the icon
	// TODO: This doesn't work for some reason.
	/*wxBitmap icon_bitmap(&elmchey_icon, wxBITMAP_TYPE_PNG, 16, 16, 32);
	wxIcon actual_icon;
	actual_icon.CopyFromBitmap(icon_bitmap);
	SetIcon(actual_icon);*/

	// Make it slightly bigger by default
	SetClientSize(wxSize(400, 450));

	// Display the window
	Centre();
	Show();

	// FOR TESTING
	//SettingsDialog* settings_dialog = new SettingsDialog(this);
}

void EmuFrame::enable_settings(bool enable)
{
	menu_bar->Enable(SETTINGS, enable);
}

void EmuFrame::enable_context_menus(bool enable)
{
	if (enable)
	{
		executable_list->current_executable_id = -1;
	}
	else
	{
		executable_list->current_executable_id = -2;
	}
}

void EmuFrame::close(wxCloseEvent& event)
{
	emu.reset();
	event.Skip();
}

void EmuFrame::run(wxCommandEvent& event)
{
	wxFileDialog run_dialog(this, "Select an executable", wxGetCwd(), wxEmptyString, wxFileSelectorDefaultWildcardStr, wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	run_dialog.Centre();

	if (run_dialog.ShowModal() != wxID_CANCEL)
	{
		todo("Booting an executable without previously knowing the system type.");
	}
}

void EmuFrame::install(wxCommandEvent& event)
{
	todo("Implement installing");
}

void EmuFrame::settings(wxCommandEvent& event)
{
	SettingsDialog* settings_dialog = new SettingsDialog(this);
}

void EmuFrame::about(wxCommandEvent& event)
{
	AboutDialog* about_dialog = new AboutDialog(this);
}