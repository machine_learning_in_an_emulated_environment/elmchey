#include "stdafx.h"

#include "settings.h"

SettingsAll::SettingsAll(wxWindow* parent) : SettingsBook(parent)
{
	add_page(new SettingsGeneral(*this));
	add_page(new SettingsCHIP8(*this));
	add_page(new SettingsGameBoy(*this));
	add_page(new SettingsDeveloper(*this));
}

SettingsGeneral::SettingsGeneral(wxWindow* parent) : SettingsPage(parent, "General")
{
}

SettingsCHIP8::SettingsCHIP8(wxWindow* parent) : SettingsPage(parent, "CHIP8")
{
	add_pixel_renderer_setting(cfg::chip8.renderer);
}

SettingsGameBoy::SettingsGameBoy(wxWindow* parent) : SettingsPage(parent, "Game Boy")
{
	add_pixel_renderer_setting(cfg::game_boy.renderer);
}

SettingsDeveloper::SettingsDeveloper(wxWindow* parent) : SettingsPage(parent, "Developer")
{
	add_boolean(cfg::developer.enable_debugging, "Enable debugging");
}