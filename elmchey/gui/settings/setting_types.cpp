#include "stdafx.h"
#include "setting_types.h"

SettingBoolean::SettingBoolean(wxWindow* parent, cfg::Boolean* config_item, std::string label) : config_item(config_item)
{
	check_box = new wxCheckBox(parent, wxID_ANY, label);
	check_box->SetValue(config_item->value);
}

void SettingBoolean::save()
{
	config_item->value = check_box->GetValue();
}