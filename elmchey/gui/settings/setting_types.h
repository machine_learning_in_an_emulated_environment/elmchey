#pragma once

class SettingItem
{
public:
	virtual void save() = 0;
};

class SettingBoolean : public SettingItem
{
	friend class SettingsPage;

public:
	SettingBoolean(wxWindow* parent, cfg::Boolean* config_item, std::string label);
	void save();

private:
	wxCheckBox* check_box;
	cfg::Boolean* config_item;
};

template<typename Enumerator>
class SettingEnum : public SettingItem
{
protected:
	SettingEnum(cfg::Enum<Enumerator>* config_item) : config_item(config_item)
	{
	}

	// Returns a C-string vector of options and the index of the option that should be selected. Also prepares the options mapping for index -> Enumerator.
	void prepare_choices(std::vector<std::pair<Enumerator, std::string>>& option_map, std::vector<const char*>& option_strings, u8& selection)
	{
		for (u8 i = 0; i < option_map.size(); ++i)
		{
			option_strings.push_back(option_map[i].second.c_str());
			this->options.push_back(option_map[i].first);

			if (option_map[i].first == config_item->value)
			{
				selection = i;
			}
		}
	}

	cfg::Enum<Enumerator>* config_item;
	std::vector<Enumerator> options;
};

template<typename Enumerator>
class SettingEnumCombo : public SettingEnum<Enumerator>
{
	friend class SettingsPage;

public:
	SettingEnumCombo(wxWindow* parent, cfg::Enum<Enumerator>* config_item, std::string label, std::vector<std::pair<Enumerator, std::string>>& option_map)
	                : SettingEnum<Enumerator>(config_item)
	{
		std::vector<const char*> option_strings;
		u8 selection;

		this->prepare_choices(option_map, option_strings, selection);

		static_box = new wxStaticBoxSizer(wxVERTICAL, parent, label);

		wxChoice* combo_box = new wxChoice(static_box->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxArrayString(option_strings.size(), option_strings.data()));
		combo_box->SetSelection(selection);

		static_box->Add(combo_box, wxSizerFlags().Border());
	}

	void save()
	{
		this->config_item->value = this->options[combo_box->GetSelection()];
	}

private:
	wxStaticBoxSizer* static_box;
	wxChoice* combo_box;
};

template<typename Enumerator>
class SettingEnumRadio : public SettingEnum<Enumerator>
{
	friend class SettingsPage;

public:
	SettingEnumRadio(wxWindow* parent, cfg::Enum<Enumerator>* config_item, std::string label, std::vector<std::pair<Enumerator, std::string>>& option_map)
	                : SettingEnum<Enumerator>(config_item)
	{
		std::vector<const char*> option_strings;
		u8 selection;

		this->prepare_choices(option_map, option_strings, selection);

		radio_box = new wxRadioBox(parent, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxArrayString(option_strings.size(), option_strings.data()), 1);
		radio_box->SetSelection(selection);
	}

	void save()
	{
		this->config_item->value = this->options[radio_box->GetSelection()];
	}

private:
	wxRadioBox* radio_box;
};