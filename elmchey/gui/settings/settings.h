#pragma once

#include "setting_containers.h"

class SettingsAll : public SettingsBook
{
public:
	SettingsAll(wxWindow* parent);
};

class SettingsGeneral : public SettingsPage
{
public:
	SettingsGeneral(wxWindow* parent);
};

class SettingsCHIP8 : public SettingsPage
{
public:
	SettingsCHIP8(wxWindow* parent);
};

class SettingsGameBoy : public SettingsPage
{
public:
	SettingsGameBoy(wxWindow* parent);
};

class SettingsDeveloper : public SettingsPage
{
public:
	SettingsDeveloper(wxWindow* parent);
};