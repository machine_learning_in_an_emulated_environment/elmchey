#pragma once

#include "setting_types.h"

#include <wx/notebook.h>

class SettingContainer
{
public:
	virtual void save() = 0;
	virtual operator wxWindow*() = 0;
};

// A container that holds different settings (checkboxes, radioboxes, comboboxes, etc)
// Can be either created standalone or added to a SettingBook.
class SettingsPage : public SettingContainer
{
	friend class SettingsDialog;
	friend class SettingsBook;

public:
	void save();

	std::string name;

	operator wxWindow*()
	{
		return panel;
	}

protected:
	SettingsPage(wxWindow* parent, std::string name);

	// Creates a checkbox.
	void add_boolean(cfg::Boolean& config_item, std::string label);

	// Creates a combobox with for choosing values.
	// option_map should be a vector of pairs that contains option enum values and their correpsonding string value/description.
	template<typename Enumerator>
	void add_enum_combo(cfg::Enum<Enumerator>& config_item, std::string label, std::vector<std::pair<Enumerator, std::string>> option_map)
	{
		std::unique_ptr<SettingEnumCombo<Enumerator>> enum_combo(new SettingEnumCombo<Enumerator>(panel, &config_item, label, option_map));
		page_sizer->Add(enum_combo->static_box, wxSizerFlags().Border());

		settings.push_back(std::move(enum_combo));
	}

	// Similar to add_enum_combo(), but uses a radiobox.
	template<typename Enumerator>
	void add_enum_radio(cfg::Enum<Enumerator>& config_item, std::string label, std::vector<std::pair<Enumerator, std::string>> option_map)
	{
		std::unique_ptr<SettingEnumRadio<Enumerator>> enum_radio(new SettingEnumRadio<Enumerator>(panel, &config_item, label, option_map));
		page_sizer->Add(enum_radio->radio_box, wxSizerFlags().Border());

		settings.push_back(std::move(enum_radio));
	}

	// Short version for the PixelRendererType settings
	void add_pixel_renderer_setting(cfg::Enum<PixelRendererType>& renderer_type)
	{
		add_enum_radio(renderer_type, "Renderer",
		{
			{ PixelRendererType::VULKAN,   "Vulkan" },
			{ PixelRendererType::DX12,     "DX12" },
			{ PixelRendererType::OPENGL45, "OpenGL 4.5" },
			{ PixelRendererType::OPENGL11, "OpenGL 1.1" },
		});
	}

private:
	wxPanel* panel;
	wxBoxSizer* page_sizer;
	std::vector<std::unique_ptr<SettingItem>> settings;
};

// A container which holds setting pages.
class SettingsBook : public SettingContainer
{
public:
	void save();

	operator wxWindow*()
	{
		return notebook;
	}

protected:
	SettingsBook(wxWindow* parent);
	void add_page(SettingsPage* page);

private:
	wxNotebook* notebook;
	std::vector<SettingsPage*> pages;
};