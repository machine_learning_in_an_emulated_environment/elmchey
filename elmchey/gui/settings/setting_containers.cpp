#include "stdafx.h"
#include "setting_containers.h"

SettingsPage::SettingsPage(wxWindow* parent, std::string name) : name(name)
{
	panel = new wxPanel(parent);
	page_sizer = new wxBoxSizer(wxVERTICAL);
	panel->SetSizer(page_sizer);
}

void SettingsPage::save()
{
	for (std::unique_ptr<SettingItem>& setting : settings)
	{
		setting->save();
	}
}

void SettingsPage::add_boolean(cfg::Boolean& config_item, std::string label)
{
	std::unique_ptr<SettingBoolean> boolean(new SettingBoolean(panel, &config_item, label));
	page_sizer->Add(boolean->check_box, wxSizerFlags().Border());

	settings.push_back(std::move(boolean));
}

SettingsBook::SettingsBook(wxWindow* parent)
{
	notebook = new wxNotebook(parent, wxID_ANY);
}

void SettingsBook::save()
{
	for (SettingsPage* page : pages)
	{
		page->save();
	}
}

void SettingsBook::add_page(SettingsPage* page)
{
	pages.push_back(page);
	notebook->AddPage(page->panel, page->name);
}