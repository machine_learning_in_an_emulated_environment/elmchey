#include "stdafx.h"
#include "settings_dialog.h"

wxBEGIN_EVENT_TABLE(SettingsDialog, wxDialog)
	EVT_BUTTON(wxID_OK, SettingsDialog::save)
wxEND_EVENT_TABLE()

SettingsDialog::SettingsDialog(wxWindow* parent) : SettingsDialog(parent, "Settings")
{
	settings = std::make_unique<SettingsAll>(this);
	initialize();
}

SettingsDialog::SettingsDialog(wxWindow* parent, Executable& executable) : SettingsDialog(parent, "Settings for \"" + executable.name + "\"")
{
	// Try to load the game-specific configuration
	cfg::load_custom(executable);

	switch (executable.system)
	{
		case System::CHIP8:
		{
			settings = std::make_unique<SettingsCHIP8>(this);
			break;
		}

		case System::GAME_BOY:
		{
			settings = std::make_unique<SettingsGameBoy>(this);
			break;
		}

		default:
		{
			debug("%s is unhandled in SettingsDialog for custom settings!", system_to_string(executable.system));
			return;
		}
	}

	initialize();
}

SettingsDialog::SettingsDialog(wxWindow* parent, std::string title) : wxDialog(parent, wxID_ANY, title)
{
	// Make sure that we don't disturb the currently in-use configuration
	cfg::push();
}

void SettingsDialog::initialize()
{
	// Close buttons
	wxButton* cancel_button = new wxButton(this, wxID_CANCEL, "Cancel");
	wxButton* save_button = new wxButton(this, wxID_OK, "Save");
	save_button->SetDefault();

	wxBoxSizer* button_sizer = new wxBoxSizer(wxHORIZONTAL);
	button_sizer->Add(cancel_button, wxSizerFlags().Border(wxRIGHT));
	button_sizer->Add(save_button, wxSizerFlags());

	// Layout
	wxBoxSizer* dialog_sizer = new wxBoxSizer(wxVERTICAL);
	dialog_sizer->Add(*settings.get(), wxSizerFlags().Border().Expand());
	dialog_sizer->Add(button_sizer, wxSizerFlags().Border().Right());

	SetSizerAndFit(dialog_sizer);

	Centre();
	ShowModal();

	// Switch back to the previous configuration
	cfg::pop();
}

void SettingsDialog::save(wxCommandEvent& event)
{
	// Save the changes
	settings->save();

	if (!cfg::save())
	{
		// TODO: An error dialog
		error("Failed to save the configuration changes to disk.");
	}

	EndModal(0);
}