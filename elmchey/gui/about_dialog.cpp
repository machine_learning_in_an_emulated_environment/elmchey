﻿#include "stdafx.h"
#include "about_dialog.h"
#include "resources.h"

#include <wx/hyperlink.h>
#include <wx/notebook.h>

wxBEGIN_EVENT_TABLE(AboutDialog, wxDialog)
	EVT_BUTTON(wxID_CLOSE, AboutDialog::close)
	EVT_BUTTON(BUTTON_LICENCES, AboutDialog::licences)
wxEND_EVENT_TABLE()

wxBEGIN_EVENT_TABLE(LicenceDialog, wxDialog)
	EVT_BUTTON(wxID_CLOSE, LicenceDialog::close)
wxEND_EVENT_TABLE()

AboutDialog::AboutDialog(wxWindow* parent) : wxDialog(parent, wxID_ABOUT, "About Elmchey")
{
	constexpr s32 text_width = 320;

	// Title
	wxStaticText* text_title = new wxStaticText(this, wxID_ANY, "Elmchey");
	text_title->SetFont(wxFont(18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

	// Version
	wxStaticText* text_version = new wxStaticText(this, wxID_ANY, "VERSION");
	text_version->SetForegroundColour(wxColour(50, 50, 50));

	// Website link
	wxHyperlinkCtrl* website_hyperlink = new wxHyperlinkCtrl(this, wxID_ANY, "Website", "https://www.elmchey.com");

	// About section
	wxStaticText* about_text = new wxStaticText(this, wxID_ANY, "Elmchey is a multi-system emulator that offers accurate emulation, online"
	                                            " play support and a wide range of development tools.\n\n© Raul Tambre 2016-2017");
	about_text->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	about_text->Wrap(text_width);

	// Buttons
	wxButton* close_button = new wxButton(this, wxID_CLOSE, "Close");
	close_button->SetDefault();

	wxButton* licences_button = new wxButton(this, BUTTON_LICENCES, "Licences");

	wxBoxSizer* button_sizer = new wxBoxSizer(wxHORIZONTAL);
	button_sizer->Add(close_button, wxSizerFlags().Border());
	button_sizer->Add(licences_button, wxSizerFlags().Border());

	// Layout
	wxBoxSizer* dialog_sizer = new wxBoxSizer(wxVERTICAL);
	dialog_sizer->Add(text_title, wxSizerFlags().Border(wxALL ^ wxBOTTOM));
	dialog_sizer->Add(text_version, wxSizerFlags().Border(wxALL ^ wxTOP));
	dialog_sizer->Add(website_hyperlink, wxSizerFlags().Border(wxALL ^ wxTOP));
	dialog_sizer->Add(about_text, wxSizerFlags().Border(wxALL ^ wxTOP));
	dialog_sizer->Add(button_sizer, wxSizerFlags().Centre());

	SetSizerAndFit(dialog_sizer);

	Centre();
	ShowModal();
}

void AboutDialog::close(wxCommandEvent& event)
{
	EndModal(0);
}

void AboutDialog::licences(wxCommandEvent& event)
{
	new LicenceDialog(this);
}

LicenceDialog::LicenceDialog(wxWindow* parent) : wxDialog(parent, wxID_ANY, "Licences")
{
	constexpr s32 text_width = 512;

	// Notebook to hold different licence tabs
	wxNotebook* licence_notebook = new wxNotebook(this, wxID_ANY);
	licence_notebook->AddPage(new LicencePage(licence_notebook, resources::glew_licence, text_width), "GLEW licence");
	licence_notebook->AddPage(new LicencePage(licence_notebook, resources::yaml_cpp_licence, text_width), "yaml-cpp licence");
	licence_notebook->AddPage(new LicencePage(licence_notebook, resources::xbyak_copyright, text_width), "Xbyak licence");

	// Close button
	wxButton* close_button = new wxButton(this, wxID_CLOSE, "Close");
	close_button->SetDefault();
	
	// Layout
	wxBoxSizer* dialog_sizer = new wxBoxSizer(wxVERTICAL);
	dialog_sizer->Add(licence_notebook, wxSizerFlags());
	dialog_sizer->Add(close_button, wxSizerFlags().Border().Centre());

	SetSizerAndFit(dialog_sizer);

	Centre();
	ShowModal();
}

void LicenceDialog::close(wxCommandEvent& event)
{
	EndModal(0);
}