#pragma once

#include "executable_list.h"
#include "about_dialog.h"
#include "settings_dialog.h"

// Event IDs
enum : s32
{
	RUN = 1,
	INSTALL = 2,
	SETTINGS = 3,
	ABOUT = wxID_ABOUT,
};

class EmuFrame : private wxFrame
{
public:
	EmuFrame();
	void enable_settings(bool enable = true);
	void enable_context_menus(bool enable = true);

private:
	ExecutableList* executable_list;
	wxMenuBar* menu_bar;

	void close(wxCloseEvent& event);
	void run(wxCommandEvent& event);
	void install(wxCommandEvent& event);
	void settings(wxCommandEvent& event);
	void about(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};