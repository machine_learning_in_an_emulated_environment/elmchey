#include "stdafx.h"
#include "executable_list.h"
#include "settings_dialog.h"

wxBEGIN_EVENT_TABLE(ExecutableList, wxListCtrl)
	EVT_LIST_ITEM_ACTIVATED(EXECUTABLE_LIST, ExecutableList::run_executable)
	EVT_LIST_ITEM_RIGHT_CLICK(EXECUTABLE_LIST, ExecutableList::context_menu)
wxEND_EVENT_TABLE()

ExecutableList::ExecutableList(wxWindow* parent) : wxListCtrl(parent, EXECUTABLE_LIST, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_AUTOARRANGE),
                                                   current_executable_id(-1)
{
	wxListItem name;
	name.SetId(0);
	name.SetText("Name");
	name.SetWidth(300);
	InsertColumn(0, name);

	wxListItem system;
	system.SetId(1);
	system.SetText("System");
	system.SetWidth(100);
	InsertColumn(1, system);
}

void ExecutableList::insert_executable(Executable& executable)
{
	wxListItem entry;
	entry.SetId(GetItemCount());
	InsertItem(entry);

	SetItem(entry.GetId(), 0, executable.name);
	SetItem(entry.GetId(), 1, system_to_string(executable.system));

	executables.push_back(executable);
}

void ExecutableList::run_executable(wxListEvent& event)
{
	// By default we make the context menu available only for the executable that's going to be run.
	current_executable_id = event.GetIndex();

	if (!elmchey.run(executables[event.GetIndex()]))
	{
		// Make sure the context menu is available once again, if the executable fails to be ran.
		current_executable_id = -1;
	}
}

void ExecutableList::context_menu(wxListEvent& event)
{
	s32 selection = event.GetIndex();

	// While an emulator is executing, only show the context menu for the executable that's executing
	if (current_executable_id != -2 && (current_executable_id == -1 || current_executable_id == selection))
	{
		wxMenu context_menu;
		context_menu.SetClientData(&selection);
		context_menu.Append(CUSTOM_SETTINGS, "Custom settings");
		context_menu.Bind(wxEVT_COMMAND_MENU_SELECTED, &ExecutableList::custom_settings, this, CUSTOM_SETTINGS, CUSTOM_SETTINGS);

		PopupMenu(&context_menu);
	}
}

void ExecutableList::custom_settings(wxCommandEvent& event)
{
	s32 selection = *reinterpret_cast<s32*>(static_cast<wxMenu*>(event.GetEventObject())->GetClientData());

	SettingsDialog* settings = new SettingsDialog(this, executables[selection]);
}

void ExecutableList::update()
{
	// Generic loaders that load from the <string> directory.
	std::unordered_map<std::string, void(ExecutableList::*)(fs::path&)> generic_loaders =
	{
		{ "CHIP8", &ExecutableList::CHIP8_loader },
		{ "Game Boy", &ExecutableList::GameBoy_loader },
	};

	// TODO: Allow addition of extra loading locations
	for (auto& loader : generic_loaders)
	{
		(this->*loader.second)(fs::current_path().append(loader.first));
	}
}

void ExecutableList::CHIP8_loader(fs::path& path)
{
	for (auto& entry : fs::directory_iterator(path))
	{
		fs::path path = entry.path();

		if (!fs::is_regular_file(entry.status()) || path.extension().string() != ".ch8")
		{
			continue;
		}

		std::string filename = path.filename().u8string();

		Executable executable;
		executable.system = System::CHIP8;
		executable.name = filename.replace(filename.find_last_of('.'), path.extension().string().length(), "");
		executable.path = path;

		insert_executable(executable);
	}
}

void ExecutableList::GameBoy_loader(fs::path& path)
{
	for (auto& entry : fs::directory_iterator(path))
	{
		fs::path path = entry.path();

		if (!fs::is_regular_file(entry.status()) || path.extension().string() != ".gb")
		{
			continue;
		}

		std::string filename = path.filename().u8string();

		Executable executable;
		executable.system = System::GAME_BOY;
		executable.name = filename.replace(filename.find_last_of('.'), path.extension().string().length(), "");
		executable.path = path;

		insert_executable(executable);
	}
}