#pragma once

#include "settings/settings.h"

class SettingsDialog : public wxDialog
{
public:
	SettingsDialog(wxWindow* parent);
	SettingsDialog(wxWindow* parent, Executable& executable);

protected:
	SettingsDialog(wxWindow* parent, std::string title);
	void initialize();

private:
	std::unique_ptr<SettingContainer> settings;
	void save(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};