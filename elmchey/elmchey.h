#pragma once

// Represents the global Elmchey state
class Elmchey
{
	friend class wxElmchey;
	friend class Emulator;

public:
	bool run(Executable& executable);

private:
	// For internal usage
	class EmuFrame* frame;

	// These methods are only meant to be called by the platform-specific implementation class.
	bool initialize();
	s32 on_exit();

	// Called when an Emulator instance has exited or been destroyed
	void emulator_exited();
};

// The Elmchey instance
extern Elmchey elmchey;